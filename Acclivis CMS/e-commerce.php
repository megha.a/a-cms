

            <div class="card mb-5">
              <div class="card-body">
                <div class="row g-5">
                      <!-- Widget Type 2-->
                      <div class="col-md-6 col-xl-3">
                        <h6 class="subtitle fw-normal text-muted">Earnings</h6>
                        <h5 class="m-b-25">$10,500<span class="ms-3 float-end text-red">+20.3%</span></h5>
                        <div class="progress">
                          <div class="progress-bar bg-red" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width:85%"></div>
                        </div>
                      </div>
                      <!-- /Widget Type 2-->
                      <!-- Widget Type 2-->
                      <div class="col-md-6 col-xl-3">
                        <h6 class="subtitle fw-normal text-muted">Readers</h6>
                        <h5 class="m-b-25">584<span class="ms-3 float-end text-blue">+3.3%</span></h5>
                        <div class="progress">
                          <div class="progress-bar bg-blue" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width:75%"></div>
                        </div>
                      </div>
                      <!-- /Widget Type 2-->
                      <!-- Widget Type 2-->
                      <div class="col-md-6 col-xl-3">
                        <h6 class="subtitle fw-normal text-muted">Bookmarks</h6>
                        <h5 class="m-b-25">876<span class="ms-3 float-end text-primary">+10.5%</span></h5>
                        <div class="progress">
                          <div class="progress-bar bg-primary" role="progressbar" aria-valuenow="67" aria-valuemin="0" aria-valuemax="100" style="width:67%"></div>
                        </div>
                      </div>
                      <!-- /Widget Type 2-->
                      <!-- Widget Type 2-->
                      <div class="col-md-6 col-xl-3">
                        <h6 class="subtitle fw-normal text-muted">Visitors</h6>
                        <h5 class="m-b-25">3,500<span class="ms-3 float-end text-green">-5.8%</span></h5>
                        <div class="progress">
                          <div class="progress-bar bg-green" role="progressbar" aria-valuenow="93" aria-valuemin="0" aria-valuemax="100" style="width:93%"></div>
                        </div>
                      </div>
                      <!-- /Widget Type 2-->
                </div>
              </div>
            </div>
            <!-- <Sales>-->
              <div class="card mb-5">
                <div class="card-header bg-inverse">
                  <div class="row align-items-center">
                    <div class="col"> 
                      <h5 class="card-heading">Sales by channel</h5>
                    </div>
                    <div class="col-auto">
                      <div class="btn-group"><a class="btn btn-sm btn-outline-light active" href="#!" aria-current="page">This week</a><a class="btn btn-sm btn-outline-light" href="#!">Last week</a></div>
                    </div>
                  </div>
                </div>
                <div class="card-body">
                  <div class="row align-items-center mb-5">
                    <div class="col">
                      <h3 class="subtitle text-gray-500">Total Revenue</h3>
                      <div class="h1 text-primary">$19,200</div>
                      <p class="mb-0"><span class="text-muted me-3">+$2,032 </span><span class="badge badge-success-light"><i class="fas fa-arrow-up me-2"></i>19.5%</span></p>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-xl ms-2 bg-primary-light">
                            <svg class="svg-icon text-primary">
                              <use xlink:href="icons/orion-svg-sprite.71e9f5f2.svg#pay-1"> </use>
                            </svg>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-9 col-xl-10"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                      <canvas id="barChart" width="1182" height="472" style="display: block; height: 378px; width: 946px;" class="chartjs-render-monitor"></canvas>
                      <ul class="mt-4 text-gray-500 list-inline card-text text-center">
                        <li class="list-inline-item"> <span class="indicator bg-primary"></span>Organic Search </li>
                        <li class="list-inline-item"><span class="indicator" style="background: #d0d2f3"> </span>Facebook Ads </li>
                      </ul>
                    </div>
                    <div class="col-lg-3 col-xl-2 text-end border-start d-flex flex-column justify-content-between py-lg-3 py-xxl-5">
                      <div>
                        <h3 class="subtitle text-gray-500 fw-normal">Organic Search Revenue</h3>
                        <div class="h4 fw-normal h1 text-dark">$19,200</div>
                        <p class="mb-0"><span class="text-muted me-2">+$2,123 </span><span class="badge badge-success-light"><i class="fas fa-arrow-up me-2"></i>21.3%</span></p>
                      </div>
                      <hr class="bg-gray-500">
                      <div>
                        <h3 class="subtitle text-gray-500 fw-normal">Facebook Ads Revenue</h3>
                        <div class="h4 fw-normal h1 text-dark">$19,200</div>
                        <p class="mb-0"><span class="text-muted me-2">-$233 </span><span class="badge badge-danger-light"><i class="fas fa-arrow-down me-2"></i>-2.1%                   </span></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <!-- </Sales>-->
            <div class="row">
              <!-- <Latest activity>-->
              <div class="col-lg-4">
                <div class="card-adjust-height-lg">
                  <div class="card mb-4 mb-lg-0">
                    <div class="card-header">
                      <h5 class="card-heading">Latest activity</h5>
                          <div class="card-header-more">
                            <button class="btn-header-more" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></button>
                            <div class="dropdown-menu dropdown-menu-end text-sm"><a class="dropdown-item" href="#!"><i class="fas fa-expand-arrows-alt opacity-5 me-2"></i>Expand</a><a class="dropdown-item" href="#!"><i class="far fa-window-minimize opacity-5 me-2"></i>Minimize</a><a class="dropdown-item" href="#!"><i class="fas fa-redo opacity-5 me-2"></i> Reload</a><a class="dropdown-item" href="#!"><i class="far fa-trash-alt opacity-5 me-2"></i> Remove</a></div>
                          </div>
                    </div>
                    <div class="card-body">
                      <div class="list-group list-group-flush list-group-timeline">
                        <div class="list-group-item px-0">
                          <div class="row">
                            <div class="col-auto"><img class="avatar p-1 me-2" src="imgs/avatar-0.jpg" alt="Nielsen Cobb"></div>
                            <div class="col ms-n3 pt-2 text-sm text-gray-800"><strong class="text-dark">Nielsen Cobb </strong> subscribed to your newsletter.
                              <div class="text-gray-500 small">3m ago</div>
                            </div>
                          </div>
                        </div>
                        <!-- /.list-group-item-->
                        <div class="list-group-item px-0">
                          <div class="row">
                            <div class="col-auto"><img class="avatar p-1 me-2" src="imgs/avatar-1.jpg" alt="Margret Cote"></div>
                            <div class="col ms-n3 pt-2 text-sm text-gray-800"><strong class="text-dark">Margret Cote </strong> liked your post 🎉
                              <div class="text-gray-500 small">4m ago</div>
                            </div>
                          </div>
                        </div>
                        <!-- /.list-group-item-->
                        <div class="list-group-item px-0">
                          <div class="row">
                            <div class="col-auto"><img class="avatar p-1 me-2" src="imgs/avatar-2.jpg" alt="Rachel Vinson"></div>
                            <div class="col ms-n3 pt-2 text-sm text-gray-800"><strong class="text-dark">Rachel Vinson </strong> placed an order.
                              <div class="text-gray-500 small">5m ago</div>
                            </div>
                          </div>
                        </div>
                        <!-- /.list-group-item-->
                        <div class="list-group-item px-0">
                          <div class="row">
                            <div class="col-auto"><img class="avatar p-1 me-2" src="imgs/avatar-3.jpg" alt="Gabrielle Aguirre"></div>
                            <div class="col ms-n3 pt-2 text-sm text-gray-800"><strong class="text-dark">Gabrielle Aguirre </strong>commented on &quot;How to season your new grill.&quot;
                              <div class="text-gray-500 small">6m ago</div>
                            </div>
                          </div>
                        </div>
                        <!-- /.list-group-item-->
                        <div class="list-group-item px-0">
                          <div class="row">
                            <div class="col-auto"><img class="avatar p-1 me-2" src="imgs/avatar-4.jpg" alt="Spears Collier"></div>
                            <div class="col ms-n3 pt-2 text-sm text-gray-800"><strong class="text-dark">Spears Collier </strong> subscribed to your newsletter.
                              <div class="text-gray-500 small">7m ago</div>
                            </div>
                          </div>
                        </div>
                        <!-- /.list-group-item-->
                        <div class="list-group-item px-0">
                          <div class="row">
                            <div class="col-auto"><img class="avatar p-1 me-2" src="imgs/avatar-5.jpg" alt="Keisha Thomas"></div>
                            <div class="col ms-n3 pt-2 text-sm text-gray-800"><strong class="text-dark">Keisha Thomas </strong> liked your post 🎉
                              <div class="text-gray-500 small">8m ago</div>
                            </div>
                          </div>
                        </div>
                        <!-- /.list-group-item-->
                        <div class="list-group-item px-0">
                          <div class="row">
                            <div class="col-auto"><img class="avatar p-1 me-2" src="imgs/avatar-6.jpg" alt="Elisabeth Key"></div>
                            <div class="col ms-n3 pt-2 text-sm text-gray-800"><strong class="text-dark">Elisabeth Key </strong> placed an order.
                              <div class="text-gray-500 small">9m ago</div>
                            </div>
                          </div>
                        </div>
                        <!-- /.list-group-item-->
                        <div class="list-group-item px-0">
                          <div class="row">
                            <div class="col-auto"><img class="avatar p-1 me-2" src="imgs/avatar-7.jpg" alt="Patel Mack"></div>
                            <div class="col ms-n3 pt-2 text-sm text-gray-800"><strong class="text-dark">Patel Mack </strong>commented on &quot;How to season your new grill.&quot;
                              <div class="text-gray-500 small">10m ago</div>
                            </div>
                          </div>
                        </div>
                        <!-- /.list-group-item-->
                        <div class="list-group-item px-0">
                          <div class="row">
                            <div class="col-auto"><img class="avatar p-1 me-2" src="imgs/avatar-8.jpg" alt="Erika Whitaker"></div>
                            <div class="col ms-n3 pt-2 text-sm text-gray-800"><strong class="text-dark">Erika Whitaker </strong> subscribed to your newsletter.
                              <div class="text-gray-500 small">11m ago</div>
                            </div>
                          </div>
                        </div>
                        <!-- /.list-group-item-->
                        <div class="list-group-item px-0">
                          <div class="row">
                            <div class="col-auto"><img class="avatar p-1 me-2" src="imgs/avatar-9.jpg" alt="Meyers Swanson"></div>
                            <div class="col ms-n3 pt-2 text-sm text-gray-800"><strong class="text-dark">Meyers Swanson </strong> liked your post 🎉
                              <div class="text-gray-500 small">12m ago</div>
                            </div>
                          </div>
                        </div>
                        <!-- /.list-group-item-->
                        <div class="list-group-item px-0">
                          <div class="row">
                            <div class="col-auto"><img class="avatar p-1 me-2" src="imgs/avatar-10.jpg" alt="Townsend Sloan"></div>
                            <div class="col ms-n3 pt-2 text-sm text-gray-800"><strong class="text-dark">Townsend Sloan </strong> placed an order.
                              <div class="text-gray-500 small">13m ago</div>
                            </div>
                          </div>
                        </div>
                        <!-- /.list-group-item-->
                        <div class="list-group-item px-0">
                          <div class="row">
                            <div class="col-auto"><img class="avatar p-1 me-2" src="imgs/avatar-11.jpg" alt="Millicent Henry"></div>
                            <div class="col ms-n3 pt-2 text-sm text-gray-800"><strong class="text-dark">Millicent Henry </strong>commented on &quot;How to season your new grill.&quot;
                              <div class="text-gray-500 small">14m ago</div>
                            </div>
                          </div>
                        </div>
                        <!-- /.list-group-item-->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- </Latest activity>-->
              <!-- <Products>-->
              <div class="col-lg-8">
                <div class="card card-table mb-4"> 
                  <div class="card-header">
                    <h5 class="card-heading">Bestsellers this month</h5>
                        <div class="card-header-more">
                          <button class="btn-header-more" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></button>
                          <div class="dropdown-menu dropdown-menu-end text-sm"><a class="dropdown-item" href="#!"><i class="fas fa-expand-arrows-alt opacity-5 me-2"></i>Expand</a><a class="dropdown-item" href="#!"><i class="far fa-window-minimize opacity-5 me-2"></i>Minimize</a><a class="dropdown-item" href="#!"><i class="fas fa-redo opacity-5 me-2"></i> Reload</a><a class="dropdown-item" href="#!"><i class="far fa-trash-alt opacity-5 me-2"></i> Remove        </a></div>
                        </div>
                  </div>
                  <div class="preload-wrapper opacity-10">
                    <div class="dataTable-wrapper dataTable-loading no-header no-footer searchable fixed-columns"><div class="dataTable-top d-none"></div><div class="dataTable-container border-0"><table class="table table-hover table-striped table-borderless text-sm align-middle mb-0 dataTable-table" id="productsDatatable">
                      
                      <tbody><tr class="align-middle"><td class="d-flex align-items-center"><img class="card-table-img img-fluid rounded me-3" src="https://d19m59y37dris4.cloudfront.net/bubbly/1-3/img/product/product-1.jpg" alt="" width="30"><a class="text-reset text-decoration-none" href="#!"><strong>Round grey hanging decor</strong></a></td><td>$22.00</td><td class="text-muted">2021/10/15</td><td>
                            <label class="badge badge-primary-light">New Arrival</label>
                          </td></tr><tr class="align-middle"><td class="d-flex align-items-center"><img class="card-table-img img-fluid rounded me-3" src="https://d19m59y37dris4.cloudfront.net/bubbly/1-3/img/product/product-2.jpg" alt="" width="30"><a class="text-reset text-decoration-none" href="#!"><strong>silver black round Ipod</strong></a></td><td>$22.00</td><td class="text-muted">2021/10/01</td><td>
                          </td></tr><tr class="align-middle"><td class="d-flex align-items-center"><img class="card-table-img img-fluid rounded me-3" src="https://d19m59y37dris4.cloudfront.net/bubbly/1-3/img/product/product-3.jpg" alt="" width="30"><a class="text-reset text-decoration-none" href="#!"><strong>White USB cable</strong></a></td><td>$22.00</td><td class="text-muted">2021/06/27</td><td>
                            <label class="badge badge-info-light">Trending</label>
                          </td></tr><tr class="align-middle"><td class="d-flex align-items-center"><img class="card-table-img img-fluid rounded me-3" src="https://d19m59y37dris4.cloudfront.net/bubbly/1-3/img/product/product-4.jpg" alt="" width="30"><a class="text-reset text-decoration-none" href="#!"><strong>Sony PS 4 game controller</strong></a></td><td>$22.00</td><td class="text-muted">2021/08/30</td><td>
                          </td></tr><tr class="align-middle"><td class="d-flex align-items-center"><img class="card-table-img img-fluid rounded me-3" src="https://d19m59y37dris4.cloudfront.net/bubbly/1-3/img/product/product-5.jpg" alt="" width="30"><a class="text-reset text-decoration-none" href="#!"><strong>Kui Ye Chen’s AirPods</strong></a></td><td>$22.00</td><td class="text-muted">2021/12/17</td><td>
                          </td></tr></tbody>
                    </table></div><div class="dataTable-bottom"><div class="dataTable-info">Showing 1 to 5 of 10 entries</div><nav class="dataTable-pagination"><ul class="dataTable-pagination-list"><li class="active"><a href="#" data-page="1">1</a></li><li class=""><a href="#" data-page="2">2</a></li><li class="pager"><a href="#" data-page="2">›</a></li></ul></nav></div></div>
                  </div>
                </div>
              </div>
              <!-- </Products>-->
            </div>
            <!-- <Orders>-->
              <div class="card card-table mb-4">
                <div class="card-header">
                  <h5 class="card-heading">Latest orders</h5>
                      <div class="card-header-more">
                        <button class="btn-header-more" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></button>
                        <div class="dropdown-menu dropdown-menu-end text-sm"><a class="dropdown-item" href="#!"><i class="fas fa-expand-arrows-alt opacity-5 me-2"></i>Expand</a><a class="dropdown-item" href="#!"><i class="far fa-window-minimize opacity-5 me-2"></i>Minimize</a><a class="dropdown-item" href="#!"><i class="fas fa-redo opacity-5 me-2"></i> Reload</a><a class="dropdown-item" href="#!"><i class="far fa-trash-alt opacity-5 me-2"></i> Remove        </a></div>
                      </div>
                </div>
                <div class="card-body">
                  <div class="preload-wrapper opacity-10">
                    <div class="table-responsive">
                      <div class="dataTable-wrapper dataTable-loading no-footer sortable searchable fixed-columns"><div class="dataTable-top"><div class="dataTable-dropdown"><span class="me-2" id="categoryBulkActionOrders">
                      <select class="form-select form-select-sm d-inline w-auto" name="categoryBulkAction">
                        <option>Bulk Actions</option>
                        <option>Delete</option>
                      </select>
                      <button class="btn btn-sm btn-outline-primary align-top">Apply</button></span><label><select class="dataTable-selector form-select form-select-sm"><option value="5">5</option><option value="10" selected="">10</option><option value="15">15</option><option value="20">20</option><option value="25">25</option></select> entries per page</label></div><div class="dataTable-search"><input class="dataTable-input form-control form-control-sm" placeholder="Search..." type="text"></div></div><div class="dataTable-container"><table class="table table-hover mb-0 dataTable-table" id="ordersDatatable">
                        <thead>
                          <tr><th data-sortable="" style="width: 15.6832%;"><a href="#" class="dataTable-sorter">Order Id</a></th><th data-sortable="" style="width: 28.628%;"><a href="#" class="dataTable-sorter">Name</a></th><th data-sortable="" style="width: 11.7831%;"><a href="#" class="dataTable-sorter">Date</a></th><th data-sortable="" style="width: 16.0981%;"><a href="#" class="dataTable-sorter">Total Price</a></th><th data-sortable="" style="width: 12.364%;"><a href="#" class="dataTable-sorter">Status</a></th><th data-sortable="" style="width: 15.4342%;"><a href="#" class="dataTable-sorter">Review</a></th></tr>
                        </thead>
                        <tbody><tr class="align-middle"><td> <span class="form-check">
                                <input class="form-check-input" type="checkbox" id="check0">
                                <label class="form-check-label" for="check0">#2458</label></span></td><td> <strong>Nielsen Cobb</strong><br><span class="text-muted text-sm">nielsencobb@memora.com</span></td><td>2021/01/05</td><td>$160.29</td><td><span class="badge badge-success-light"> <span class="indicator"></span>Open</span></td><td class="text-end" style="min-width: 125px;"><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-gray-500"></i></a>
                            </td></tr><tr class="align-middle"><td> <span class="form-check">
                                <input class="form-check-input" type="checkbox" id="check1">
                                <label class="form-check-label" for="check1">#2459</label></span></td><td> <strong>Margret Cote</strong><br><span class="text-muted text-sm">margretcote@zilidium.com</span></td><td>2021/05/31</td><td>$670.63</td><td><span class="badge badge-danger-light"> <span class="indicator"></span>Closed</span></td><td class="text-end" style="min-width: 125px;"><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-gray-500"></i></a>
                            </td></tr><tr class="align-middle"><td> <span class="form-check">
                                <input class="form-check-input" type="checkbox" id="check2">
                                <label class="form-check-label" for="check2">#2460</label></span></td><td> <strong>Rachel Vinson</strong><br><span class="text-muted text-sm">rachelvinson@chorizon.com</span></td><td>2021/12/26</td><td>$490.29</td><td><span class="badge badge-warning-light"> <span class="indicator"></span>On Hold</span></td><td class="text-end" style="min-width: 125px;"><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-gray-500"></i></a>
                            </td></tr><tr class="align-middle"><td> <span class="form-check">
                                <input class="form-check-input" type="checkbox" id="check3">
                                <label class="form-check-label" for="check3">#2461</label></span></td><td> <strong>Gabrielle Aguirre</strong><br><span class="text-muted text-sm">gabrielleaguirre@comverges.com</span></td><td>2021/11/06</td><td>$690.19</td><td><span class="badge badge-info-light"> <span class="indicator"></span>In Progress</span></td><td class="text-end" style="min-width: 125px;"><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-gray-500"></i></a>
                            </td></tr><tr class="align-middle"><td> <span class="form-check">
                                <input class="form-check-input" type="checkbox" id="check4">
                                <label class="form-check-label" for="check4">#2462</label></span></td><td> <strong>Spears Collier</strong><br><span class="text-muted text-sm">spearscollier@remold.com</span></td><td>2021/01/23</td><td>$520.46</td><td><span class="badge badge-success-light"> <span class="indicator"></span>Open</span></td><td class="text-end" style="min-width: 125px;"><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-gray-500"></i></a>
                            </td></tr><tr class="align-middle"><td> <span class="form-check">
                                <input class="form-check-input" type="checkbox" id="check5">
                                <label class="form-check-label" for="check5">#2463</label></span></td><td> <strong>Keisha Thomas</strong><br><span class="text-muted text-sm">keishathomas@euron.com</span></td><td>2021/06/07</td><td>$190.76</td><td><span class="badge badge-danger-light"> <span class="indicator"></span>Closed</span></td><td class="text-end" style="min-width: 125px;"><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-gray-500"></i></a>
                            </td></tr><tr class="align-middle"><td> <span class="form-check">
                                <input class="form-check-input" type="checkbox" id="check6">
                                <label class="form-check-label" for="check6">#2464</label></span></td><td> <strong>Elisabeth Key</strong><br><span class="text-muted text-sm">elisabethkey@netagy.com</span></td><td>2021/02/17</td><td>$390.07</td><td><span class="badge badge-warning-light"> <span class="indicator"></span>On Hold</span></td><td class="text-end" style="min-width: 125px;"><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-gray-500"></i></a>
                            </td></tr><tr class="align-middle"><td> <span class="form-check">
                                <input class="form-check-input" type="checkbox" id="check7">
                                <label class="form-check-label" for="check7">#2465</label></span></td><td> <strong>Patel Mack</strong><br><span class="text-muted text-sm">patelmack@zedalis.com</span></td><td>2021/02/23</td><td>$220.61</td><td><span class="badge badge-info-light"> <span class="indicator"></span>In Progress</span></td><td class="text-end" style="min-width: 125px;"><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-gray-500"></i></a>
                            </td></tr><tr class="align-middle"><td> <span class="form-check">
                                <input class="form-check-input" type="checkbox" id="check8">
                                <label class="form-check-label" for="check8">#2466</label></span></td><td> <strong>Erika Whitaker</strong><br><span class="text-muted text-sm">erikawhitaker@uniworld.com</span></td><td>2021/06/21</td><td>$570.02</td><td><span class="badge badge-success-light"> <span class="indicator"></span>Open</span></td><td class="text-end" style="min-width: 125px;"><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-gray-500"></i></a>
                            </td></tr><tr class="align-middle"><td> <span class="form-check">
                                <input class="form-check-input" type="checkbox" id="check9">
                                <label class="form-check-label" for="check9">#2467</label></span></td><td> <strong>Meyers Swanson</strong><br><span class="text-muted text-sm">meyersswanson@candecor.com</span></td><td>2021/04/02</td><td>$730.99</td><td><span class="badge badge-danger-light"> <span class="indicator"></span>Closed</span></td><td class="text-end" style="min-width: 125px;"><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-warning"></i></a><a href="#!"><i class="fa fa-star text-gray-500"></i></a>
                            </td></tr></tbody>
                      </table></div><div class="dataTable-bottom"><div class="dataTable-info">Showing 1 to 10 of 20 entries</div><nav class="dataTable-pagination"><ul class="dataTable-pagination-list"><li class="active"><a href="#" data-page="1">1</a></li><li class=""><a href="#" data-page="2">2</a></li><li class="pager"><a href="#" data-page="2">›</a></li></ul></nav></div></div>
                    </div>
                  </div>
                </div>
              </div>
            <!-- </Orders>-->
          