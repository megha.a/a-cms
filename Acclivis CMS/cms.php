

            <div class="row mb-3">
                  <!-- Widget Type 1-->
                  <div class="mb-4 col-sm-6 col-lg-3 mb-4">
                    <div class="card h-100">
                      <div class="card-body">
                        <div class="d-flex align-items-center justify-content-between">
                          <div>
                            <h4 class="fw-normal text-red">500</h4>
                            <p class="subtitle text-sm text-muted mb-0">Contact us request</p>
                          </div>
                          <div class="flex-shrink-0 ms-3">
                                <svg class="svg-icon text-red">
                                  <use xlink:href="https://demo.bootstrapious.com/bubbly/1-3/icons/orion-svg-sprite.71e9f5f2.svg#speed-1"> </use>
                                </svg>
                          </div>
                        </div>
                      </div>
                      <div class="card-footer py-3 bg-red-light">
                        <div class="row align-items-center text-red">
                          <div class="col-10">
                            <p class="mb-0">20% increase</p>
                          </div>
                          <div class="col-2 text-end"><i class="fas fa-caret-up"></i>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- /Widget Type 1-->
                  <!-- Widget Type 1-->
                  <div class="mb-4 col-sm-6 col-lg-3 mb-4">
                    <div class="card h-100">
                      <div class="card-body">
                        <div class="d-flex align-items-center justify-content-between">
                          <div>
                            <h4 class="fw-normal text-blue">584</h4>
                            <p class="subtitle text-sm text-muted mb-0">Live Chat</p>
                          </div>
                          <div class="flex-shrink-0 ms-3">
                                <svg class="svg-icon text-blue">
                                  <use xlink:href="https://demo.bootstrapious.com/bubbly/1-3/icons/orion-svg-sprite.71e9f5f2.svg#news-1"> </use>
                                </svg>
                          </div>
                        </div>
                      </div>
                      <div class="card-footer py-3 bg-blue-light">
                        <div class="row align-items-center text-blue">
                          <div class="col-10">
                            <p class="mb-0">3% increase</p>
                          </div>
                          <div class="col-2 text-end"><i class="fas fa-caret-up"></i>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- /Widget Type 1-->
                  <!-- Widget Type 1-->
                  <div class="mb-4 col-sm-6 col-lg-3 mb-4">
                    <div class="card h-100">
                      <div class="card-body">
                        <div class="d-flex align-items-center justify-content-between">
                          <div>
                            <h4 class="fw-normal text-primary">876</h4>
                            <p class="subtitle text-sm text-muted mb-0">Career</p>
                          </div>
                          <div class="flex-shrink-0 ms-3">
                                <svg class="svg-icon text-primary">
                                  <use xlink:href="https://demo.bootstrapious.com/bubbly/1-3/icons/orion-svg-sprite.71e9f5f2.svg#bookmark-1"> </use>
                                </svg>
                          </div>
                        </div>
                      </div>
                      <div class="card-footer py-3 bg-primary-light">
                        <div class="row align-items-center text-primary">
                          <div class="col-10">
                            <p class="mb-0">10% increase</p>
                          </div>
                          <div class="col-2 text-end"><i class="fas fa-caret-up"></i>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- /Widget Type 1-->
                  <!-- Widget Type 1-->
                  <div class="mb-4 col-sm-6 col-lg-3 mb-4">
                    <div class="card h-100">
                      <div class="card-body">
                        <div class="d-flex align-items-center justify-content-between">
                          <div>
                            <h4 class="fw-normal text-green">3,500</h4>
                            <p class="subtitle text-sm text-muted mb-0">Subscriber</p>
                          </div>
                          <div class="flex-shrink-0 ms-3">
                                <svg class="svg-icon text-green">
                                  <use xlink:href="https://demo.bootstrapious.com/bubbly/1-3/icons/orion-svg-sprite.71e9f5f2.svg#world-map-1"> </use>
                                </svg>
                          </div>
                        </div>
                      </div>
                      <div class="card-footer py-3 bg-green-light">
                        <div class="row align-items-center text-green">
                          <div class="col-10">
                            <p class="mb-0">5% decrease</p>
                          </div>
                          <div class="col-2 text-end"><i class="fas fa-caret-down"></i>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- /Widget Type 1-->
            </div>
            <div class="row">
              <!-- Sales-->
              <div class="col-xl-9 mb-4">
                <div class="card">
                  <div class="card-header">
                    <h5 class="card-heading">Sales by channel</h5>
                        <div class="card-header-more">
                          <button class="btn-header-more" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></button>
                          <div class="dropdown-menu dropdown-menu-end text-sm"><a class="dropdown-item" href="#!"><i class="fas fa-expand-arrows-alt opacity-5 me-2"></i>Expand</a><a class="dropdown-item" href="#!"><i class="far fa-window-minimize opacity-5 me-2"></i>Minimize</a><a class="dropdown-item" href="#!"><i class="fas fa-redo opacity-5 me-2"></i> Reload</a><a class="dropdown-item" href="#!"><i class="far fa-trash-alt opacity-5 me-2"></i> Remove        </a></div>
                        </div>
                  </div>
                  <div class="card-body"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                    <div class="row mb-5">
                      <div class="col-12 col-sm-auto flex-sm-grow-1 py-3">
                        <h3 class="subtitle text-gray-500">Total Revenue</h3>
                        <div class="h1 text-primary">$19,200</div>
                        <p class="mb-0"><span class="text-muted me-3">+$2,032 </span><span class="badge badge-success-light"><i class="fas fa-arrow-up me-2"></i>19.5%</span></p>
                      </div>
                      <div class="col-6 col-sm-auto flex-sm-grow-1 border-start py-3 d-flex align-items-center">
                        <div>
                          <h3 class="subtitle text-gray-500 fw-normal">Organic Search </h3>
                          <div class="h4 fw-normal text-dark">$19,200</div>
                          <p class="mb-0"><span class="text-muted me-2">+$2,123 </span><span class="badge badge-success-light"><i class="fas fa-arrow-up me-2"></i>21.3%</span></p>
                        </div>
                      </div>
                      <div class="col-6 col-sm-auto flex-sm-grow-1 border-start py-3 d-flex align-items-center">
                        <div>
                          <h3 class="subtitle text-gray-500 fw-normal">Facebook Ads </h3>
                          <div class="h4 fw-normal text-dark">$2,500</div>
                          <p class="mb-0"><span class="text-muted me-2">-$233 </span><span class="badge badge-danger-light"><i class="fas fa-arrow-down me-2"></i>-2.1%           </span></p>
                        </div>
                      </div>
                      <div class="col-auto d-none d-md-flex d-xl-none d-xxl-flex align-items-center">
                        <div class="icon icon-xl ms-2 bg-primary-light">
                              <svg class="svg-icon text-primary">
                                <use xlink:href="https://demo.bootstrapious.com/bubbly/1-3/icons/orion-svg-sprite.71e9f5f2.svg#pay-1"> </use>
                              </svg>
                        </div>
                      </div>
                    </div>
                    <canvas id="barChart" style="display: block; height: 416px; width: 833px;" width="1041" height="520" class="chartjs-render-monitor"></canvas>
                    <ul class="mt-4 text-gray-500 list-inline card-text text-center">
                      <li class="list-inline-item"> <span class="indicator bg-primary"></span>Organic Search </li>
                      <li class="list-inline-item"><span class="indicator" style="background: #d0d2f3"> </span>Facebook Ads </li>
                    </ul>
                  </div>
                </div>
              </div>
              <!-- /Sales-->
              <!-- <Latest activity>-->
              <div class="col-xl-3">
                <div class="card-adjust-height-xl">
                  <div class="card mb-4 mb-xl-0">
                    <div class="card-header">
                      <h5 class="card-heading">Latest activity</h5>
                          <div class="card-header-more">
                            <button class="btn-header-more" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></button>
                            <div class="dropdown-menu dropdown-menu-end text-sm"><a class="dropdown-item" href="#!"><i class="fas fa-expand-arrows-alt opacity-5 me-2"></i>Expand</a><a class="dropdown-item" href="#!"><i class="far fa-window-minimize opacity-5 me-2"></i>Minimize</a><a class="dropdown-item" href="#!"><i class="fas fa-redo opacity-5 me-2"></i> Reload</a><a class="dropdown-item" href="#!"><i class="far fa-trash-alt opacity-5 me-2"></i> Remove        </a></div>
                          </div>
                    </div>
                    <div class="card-body">
                      <div class="list-group list-group-flush list-group-timeline">
                        <div class="list-group-item px-0">
                          <div class="row">
                            <div class="col-auto"><img class="avatar p-1 me-2" src="imgs/avatar-0.jpg" alt="Nielsen Cobb"></div>
                            <div class="col ms-n3 pt-2 text-sm text-gray-800"><strong class="text-dark">Nielsen Cobb </strong> subscribed to your newsletter.
                              <div class="text-gray-500 small">3m ago</div>
                            </div>
                          </div>
                        </div>
                        <!-- /.list-group-item-->
                        <div class="list-group-item px-0">
                          <div class="row">
                            <div class="col-auto"><img class="avatar p-1 me-2" src="imgs/avatar-1.jpg" alt="Margret Cote"></div>
                            <div class="col ms-n3 pt-2 text-sm text-gray-800"><strong class="text-dark">Margret Cote </strong> liked your post 🎉
                              <div class="text-gray-500 small">4m ago</div>
                            </div>
                          </div>
                        </div>
                        <!-- /.list-group-item-->
                        <div class="list-group-item px-0">
                          <div class="row">
                            <div class="col-auto"><img class="avatar p-1 me-2" src="imgs/avatar-2.jpg" alt="Rachel Vinson"></div>
                            <div class="col ms-n3 pt-2 text-sm text-gray-800"><strong class="text-dark">Rachel Vinson </strong> placed an order.
                              <div class="text-gray-500 small">5m ago</div>
                            </div>
                          </div>
                        </div>
                        <!-- /.list-group-item-->
                        <div class="list-group-item px-0">
                          <div class="row">
                            <div class="col-auto"><img class="avatar p-1 me-2" src="imgs/avatar-3.jpg" alt="Gabrielle Aguirre"></div>
                            <div class="col ms-n3 pt-2 text-sm text-gray-800"><strong class="text-dark">Gabrielle Aguirre </strong>commented on "How to season your new grill."
                              <div class="text-gray-500 small">6m ago</div>
                            </div>
                          </div>
                        </div>
                        <!-- /.list-group-item-->
                        <div class="list-group-item px-0">
                          <div class="row">
                            <div class="col-auto"><img class="avatar p-1 me-2" src="imgs/avatar-4.jpg" alt="Spears Collier"></div>
                            <div class="col ms-n3 pt-2 text-sm text-gray-800"><strong class="text-dark">Spears Collier </strong> subscribed to your newsletter.
                              <div class="text-gray-500 small">7m ago</div>
                            </div>
                          </div>
                        </div>
                        <!-- /.list-group-item-->
                        <div class="list-group-item px-0">
                          <div class="row">
                            <div class="col-auto"><img class="avatar p-1 me-2" src="imgs/avatar-5.jpg" alt="Keisha Thomas"></div>
                            <div class="col ms-n3 pt-2 text-sm text-gray-800"><strong class="text-dark">Keisha Thomas </strong> liked your post 🎉
                              <div class="text-gray-500 small">8m ago</div>
                            </div>
                          </div>
                        </div>
                        <!-- /.list-group-item-->
                        <div class="list-group-item px-0">
                          <div class="row">
                            <div class="col-auto"><img class="avatar p-1 me-2" src="imgs/avatar-6.jpg" alt="Elisabeth Key"></div>
                            <div class="col ms-n3 pt-2 text-sm text-gray-800"><strong class="text-dark">Elisabeth Key </strong> placed an order.
                              <div class="text-gray-500 small">9m ago</div>
                            </div>
                          </div>
                        </div>
                        <!-- /.list-group-item-->
                        <div class="list-group-item px-0">
                          <div class="row">
                            <div class="col-auto"><img class="avatar p-1 me-2" src="imgs/avatar-7.jpg" alt="Patel Mack"></div>
                            <div class="col ms-n3 pt-2 text-sm text-gray-800"><strong class="text-dark">Patel Mack </strong>commented on "How to season your new grill."
                              <div class="text-gray-500 small">10m ago</div>
                            </div>
                          </div>
                        </div>
                        <!-- /.list-group-item-->
                        <div class="list-group-item px-0">
                          <div class="row">
                            <div class="col-auto"><img class="avatar p-1 me-2" src="imgs/avatar-8.jpg" alt="Erika Whitaker"></div>
                            <div class="col ms-n3 pt-2 text-sm text-gray-800"><strong class="text-dark">Erika Whitaker </strong> subscribed to your newsletter.
                              <div class="text-gray-500 small">11m ago</div>
                            </div>
                          </div>
                        </div>
                        <!-- /.list-group-item-->
                        <div class="list-group-item px-0">
                          <div class="row">
                            <div class="col-auto"><img class="avatar p-1 me-2" src="imgs/avatar-9.jpg" alt="Meyers Swanson"></div>
                            <div class="col ms-n3 pt-2 text-sm text-gray-800"><strong class="text-dark">Meyers Swanson </strong> liked your post 🎉
                              <div class="text-gray-500 small">12m ago</div>
                            </div>
                          </div>
                        </div>
                        <!-- /.list-group-item-->
                        <div class="list-group-item px-0">
                          <div class="row">
                            <div class="col-auto"><img class="avatar p-1 me-2" src="imgs/avatar-10.jpg" alt="Townsend Sloan"></div>
                            <div class="col ms-n3 pt-2 text-sm text-gray-800"><strong class="text-dark">Townsend Sloan </strong> placed an order.
                              <div class="text-gray-500 small">13m ago</div>
                            </div>
                          </div>
                        </div>
                        <!-- /.list-group-item-->
                        <div class="list-group-item px-0">
                          <div class="row">
                            <div class="col-auto"><img class="avatar p-1 me-2" src="imgs/avatar-11.jpg" alt="Millicent Henry"></div>
                            <div class="col ms-n3 pt-2 text-sm text-gray-800"><strong class="text-dark">Millicent Henry </strong>commented on "How to season your new grill."
                              <div class="text-gray-500 small">14m ago</div>
                            </div>
                          </div>
                        </div>
                        <!-- /.list-group-item-->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- </Latest activity>-->
            </div>
            <div class="card card-table mb-4">
              <div class="card-header">
                <h5 class="card-heading"> Latest Contracts</h5>
                    <div class="card-header-more">
                      <button class="btn-header-more" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></button>
                      <div class="dropdown-menu dropdown-menu-end text-sm"><a class="dropdown-item" href="#!"><i class="fas fa-expand-arrows-alt opacity-5 me-2"></i>Expand</a><a class="dropdown-item" href="#!"><i class="far fa-window-minimize opacity-5 me-2"></i>Minimize</a><a class="dropdown-item" href="#!"><i class="fas fa-redo opacity-5 me-2"></i> Reload</a><a class="dropdown-item" href="#!"><i class="far fa-trash-alt opacity-5 me-2"></i> Remove        </a></div>
                    </div>
              </div>
              <div class="card-body">
                <div class="preload-wrapper opacity-10">
                  <div class="table-responsive">
                    <div class="dataTable-wrapper dataTable-loading no-footer sortable searchable fixed-columns"><div class="dataTable-top"><div class="dataTable-dropdown"><span class="me-2" id="categoryBulkActionOrders">
                    <select class="form-select form-select-sm d-inline w-auto" name="categoryBulkAction">
                      <option>Bulk Actions</option>
                      <option>Delete</option>
                    </select>
                    <button class="btn btn-sm btn-outline-primary align-top">Apply</button></span><label><select class="dataTable-selector form-select form-select-sm"><option value="5" selected="">5</option><option value="10">10</option><option value="15">15</option><option value="20">20</option><option value="25">25</option></select> entries per page</label></div><div class="dataTable-search"><input class="dataTable-input form-control form-control-sm" placeholder="Search..." type="text"></div></div><div class="dataTable-container border-0"><table class="table table-hover text-sm text-gray-700 mb-0 dataTable-table" id="ordersDatatable">
                      <thead>
                        <tr><th data-sortable="" style="width: 41.9048%;"><a href="#" class="dataTable-sorter">Name</a></th><th data-sortable="" style="width: 13.1108%;"><a href="#" class="dataTable-sorter">Company</a></th><th data-sortable="" style="width: 11.9491%;"><a href="#" class="dataTable-sorter">Status</a></th><th data-sortable="" style="width: 16.8449%;"><a href="#" class="dataTable-sorter">Contract</a></th><th data-sortable="" style="width: 16.1811%;"><a href="#" class="dataTable-sorter">Date</a></th></tr>
                      </thead>
                      <tbody><tr class="align-middle"><td>
                            <div class="d-flex align-items-center"><span class="avatar p-1 me-2"><span class="avatar-text avatar-primary-light">N                                                </span></span>
                              <div class="pt-1"><strong>Nielsen Cobb</strong><br><span class="text-muted text-sm">nielsencobb@memora.com</span></div>
                            </div>
                          </td><td> <strong>Memora</strong><br><span class="text-muted">Graniteville</span></td><td><span class="badge badge-success-light"> <span class="indicator"></span>Open</span></td><td style="min-width: 125px;">
                            <div class="d-flex align-items-center"><span class="me-2">30%</span>
                              <div class="progress progress-table">
                                <div class="progress-bar bg-undefined" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width:30%">  </div>
                              </div>
                            </div>
                          </td><td style="max-width: 120px">
                            <div class="d-flex align-items-center justify-content-between"><span class="me-3">2021/11/26</span>
                                  <div>
                                    <button class="btn-header-more" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></button>
                                    <div class="dropdown-menu dropdown-menu-end text-sm"><a class="dropdown-item" href="#!"><i class="fas fa-expand-arrows-alt opacity-5 me-2"></i>Expand</a><a class="dropdown-item" href="#!"><i class="far fa-edit opacity-5 me-2"></i>Edit</a><a class="dropdown-item" href="#!"><i class="far fa-trash-alt opacity-5 me-2"></i> Remove</a></div>
                                  </div>
                            </div>
                          </td></tr><tr class="align-middle"><td>
                            <div class="d-flex align-items-center"><img class="avatar p-1 me-2" src="imgs/avatar-1.jpg" alt="Margret Cote">
                              <div class="pt-1"><strong>Margret Cote</strong><br><span class="text-muted text-sm">margretcote@zilidium.com</span></div>
                            </div>
                          </td><td> <strong>Zilidium</strong><br><span class="text-muted">Foxworth</span></td><td><span class="badge badge-danger-light"> <span class="indicator"></span>Closed</span></td><td style="min-width: 125px;">
                            <div class="d-flex align-items-center"><span class="me-2">13%</span>
                              <div class="progress progress-table">
                                <div class="progress-bar bg-undefined" role="progressbar" aria-valuenow="13" aria-valuemin="0" aria-valuemax="100" style="width:13%">  </div>
                              </div>
                            </div>
                          </td><td style="max-width: 120px">
                            <div class="d-flex align-items-center justify-content-between"><span class="me-3">2021/03/01</span>
                                  <div>
                                    <button class="btn-header-more" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></button>
                                    <div class="dropdown-menu dropdown-menu-end text-sm"><a class="dropdown-item" href="#!"><i class="fas fa-expand-arrows-alt opacity-5 me-2"></i>Expand</a><a class="dropdown-item" href="#!"><i class="far fa-edit opacity-5 me-2"></i>Edit</a><a class="dropdown-item" href="#!"><i class="far fa-trash-alt opacity-5 me-2"></i> Remove</a></div>
                                  </div>
                            </div>
                          </td></tr><tr class="align-middle"><td>
                            <div class="d-flex align-items-center"><img class="avatar p-1 me-2" src="imgs/avatar-2.jpg" alt="Rachel Vinson">
                              <div class="pt-1"><strong>Rachel Vinson</strong><br><span class="text-muted text-sm">rachelvinson@chorizon.com</span></div>
                            </div>
                          </td><td> <strong>Chorizon</strong><br><span class="text-muted">Eastmont</span></td><td><span class="badge badge-warning-light"> <span class="indicator"></span>On Hold</span></td><td style="min-width: 125px;">
                            <div class="d-flex align-items-center"><span class="me-2">100%</span>
                              <div class="progress progress-table">
                                <div class="progress-bar bg-undefined" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">  </div>
                              </div>
                            </div>
                          </td><td style="max-width: 120px">
                            <div class="d-flex align-items-center justify-content-between"><span class="me-3">2021/07/03</span>
                                  <div>
                                    <button class="btn-header-more" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></button>
                                    <div class="dropdown-menu dropdown-menu-end text-sm"><a class="dropdown-item" href="#!"><i class="fas fa-expand-arrows-alt opacity-5 me-2"></i>Expand</a><a class="dropdown-item" href="#!"><i class="far fa-edit opacity-5 me-2"></i>Edit</a><a class="dropdown-item" href="#!"><i class="far fa-trash-alt opacity-5 me-2"></i> Remove</a></div>
                                  </div>
                            </div>
                          </td></tr><tr class="align-middle"><td>
                            <div class="d-flex align-items-center"><span class="avatar p-1 me-2"><span class="avatar-text avatar-warning-light">G                                                </span></span>
                              <div class="pt-1"><strong>Gabrielle Aguirre</strong><br><span class="text-muted text-sm">gabrielleaguirre@comverges.com</span></div>
                            </div>
                          </td><td> <strong>Comverges</strong><br><span class="text-muted">Whitewater</span></td><td><span class="badge badge-info-light"> <span class="indicator"></span>In Progress</span></td><td style="min-width: 125px;">
                            <div class="d-flex align-items-center"><span class="me-2">64%</span>
                              <div class="progress progress-table">
                                <div class="progress-bar bg-undefined" role="progressbar" aria-valuenow="64" aria-valuemin="0" aria-valuemax="100" style="width:64%">  </div>
                              </div>
                            </div>
                          </td><td style="max-width: 120px">
                            <div class="d-flex align-items-center justify-content-between"><span class="me-3">2021/08/07</span>
                                  <div>
                                    <button class="btn-header-more" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></button>
                                    <div class="dropdown-menu dropdown-menu-end text-sm"><a class="dropdown-item" href="#!"><i class="fas fa-expand-arrows-alt opacity-5 me-2"></i>Expand</a><a class="dropdown-item" href="#!"><i class="far fa-edit opacity-5 me-2"></i>Edit</a><a class="dropdown-item" href="#!"><i class="far fa-trash-alt opacity-5 me-2"></i> Remove</a></div>
                                  </div>
                            </div>
                          </td></tr><tr class="align-middle"><td>
                            <div class="d-flex align-items-center"><img class="avatar p-1 me-2" src="imgs/avatar-4.jpg" alt="Spears Collier">
                              <div class="pt-1"><strong>Spears Collier</strong><br><span class="text-muted text-sm">spearscollier@remold.com</span></div>
                            </div>
                          </td><td> <strong>Remold</strong><br><span class="text-muted">Hebron</span></td><td><span class="badge badge-success-light"> <span class="indicator"></span>Open</span></td><td style="min-width: 125px;">
                            <div class="d-flex align-items-center"><span class="me-2">6%</span>
                              <div class="progress progress-table">
                                <div class="progress-bar bg-undefined" role="progressbar" aria-valuenow="6" aria-valuemin="0" aria-valuemax="100" style="width:6%">  </div>
                              </div>
                            </div>
                          </td><td style="max-width: 120px">
                            <div class="d-flex align-items-center justify-content-between"><span class="me-3">2021/05/23</span>
                                  <div>
                                    <button class="btn-header-more" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></button>
                                    <div class="dropdown-menu dropdown-menu-end text-sm"><a class="dropdown-item" href="#!"><i class="fas fa-expand-arrows-alt opacity-5 me-2"></i>Expand</a><a class="dropdown-item" href="#!"><i class="far fa-edit opacity-5 me-2"></i>Edit</a><a class="dropdown-item" href="#!"><i class="far fa-trash-alt opacity-5 me-2"></i> Remove</a></div>
                                  </div>
                            </div>
                          </td></tr></tbody>
                    </table></div><div class="dataTable-bottom"><div class="dataTable-info">Showing 1 to 5 of 30 entries</div><nav class="dataTable-pagination"><ul class="dataTable-pagination-list"><li class="active"><a href="#" data-page="1">1</a></li><li class=""><a href="#" data-page="2">2</a></li><li class=""><a href="#" data-page="3">3</a></li><li class=""><a href="#" data-page="4">4</a></li><li class=""><a href="#" data-page="5">5</a></li><li class=""><a href="#" data-page="6">6</a></li><li class="pager"><a href="#" data-page="2">›</a></li></ul></nav></div></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
                  <!-- Widget Type 12-->
                  <div class="col-md-6 mb-4">   
                    <div class="card h-100">
                      <div class="card-body">
                        <div class="row gx-2 gx-lg-4 gy-5">
                          <div class="col-sm-5">
                            <div class="h2">625</div>
                            <p class="subtitle">New Customers</p>
                            <div class="progress">
                              <div class="progress-bar bg-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:60%"></div>
                            </div>
                          </div>
                          <div class="col-sm-7">
                            <div class="row">
                              <div class="col-6 text-center">
                                <div class="h3">254</div>
                                <p class="text-muted fw-normal">Affiliates</p>
                                <hr>
                                <p class="text-muted mb-0">+125</p>
                              </div>
                              <div class="col-6 text-center">
                                <div class="h3">328</div>
                                <p class="text-muted">SEM</p>
                                <hr>
                                <p class="text-muted mb-0">+144</p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- /Widget Type 12-->
                  <!-- Widget Type 13-->
                  <div class="col-md-6 mb-4">   
                    <div class="card h-100">
                      <div class="card-body d-flex align-items-center">
                        <div class="row gy-5 flex-fill"> 
                          <div class="col-sm-6">
                            <div class="row">
                              <div class="col-sm-2 text-lg"><i class="fas fa-arrow-down text-danger"></i></div>
                              <div class="col-sm-10">
                                <h2>1,112</h2>
                                <h6 class="text-muted fw-normal p-b-20 p-t-10">Affiliate Sales</h6>
                                <div class="progress">
                                  <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width:75%"></div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="row">
                              <div class="col-sm-2 text-lg"><i class="fas fa-arrow-up text-success"></i></div>
                              <div class="col-sm-10">
                                <h2>258</h2>
                                <h6 class="text-muted fw-normal p-b-20 p-t-10">Ads Sales</h6>
                                <div class="progress">
                                  <div class="progress-bar bg-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:50%"> </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- /Widget Type 13-->
            </div>
            <div class="row"> 
              <!-- <Projects Widget>-->
              <div class="col-lg-4 mb-4 mb-lg-0">
                <div class="card h-100">
                  <div class="card-header">
                    <h5 class="card-heading">Project updates</h5>
                        <div class="card-header-more">
                          <button class="btn-header-more" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></button>
                          <div class="dropdown-menu dropdown-menu-end text-sm"><a class="dropdown-item" href="#!"><i class="fas fa-expand-arrows-alt opacity-5 me-2"></i>Expand</a><a class="dropdown-item" href="#!"><i class="far fa-window-minimize opacity-5 me-2"></i>Minimize</a><a class="dropdown-item" href="#!"><i class="fas fa-redo opacity-5 me-2"></i> Reload</a><a class="dropdown-item" href="#!"><i class="far fa-trash-alt opacity-5 me-2"></i> Remove</a></div>
                        </div>
                  </div>
                  <div class="card-body">
                    <div class="card-text">
                      <p class="mb-2"><strong>Publish New Theme </strong><span class="float-end text-gray-500 text-sm">10 mins ago </span></p>
                      <p class="card-text"><img class="avatar avatar-sm avatar-stacked p-1" src="imgs/avatar-0.jpg" alt="User" data-bs-toggle="tooltip" data-placement="top" title="" data-bs-original-title="Nielsen Cobb" aria-label="Nielsen Cobb"><img class="avatar avatar-sm avatar-stacked p-1" src="imgs/avatar-1.jpg" alt="User" data-bs-toggle="tooltip" data-placement="top" title="" data-bs-original-title="Margret Cote" aria-label="Margret Cote">
                      </p>
                      <p class="mb-2"><strong>Internal Linkbuilding </strong><span class="float-end text-gray-500 text-sm">2 hours ago </span></p>
                      <p class="card-text"><img class="avatar avatar-sm avatar-stacked p-1" src="imgs/avatar-1.jpg" alt="User" data-bs-toggle="tooltip" data-placement="top" title="" data-bs-original-title="Nielsen Cobb" aria-label="Nielsen Cobb"><img class="avatar avatar-sm avatar-stacked p-1" src="imgs/avatar-2.jpg" alt="User" data-bs-toggle="tooltip" data-placement="top" title="" data-bs-original-title="Margret Cote" aria-label="Margret Cote"><img class="avatar avatar-sm avatar-stacked p-1" src="imgs/avatar-3.jpg" alt="User" data-bs-toggle="tooltip" data-placement="top" title="" data-bs-original-title="Rachel Vinson" aria-label="Rachel Vinson"><img class="avatar avatar-sm avatar-stacked p-1" src="imgs/avatar-4.jpg" alt="User" data-bs-toggle="tooltip" data-placement="top" title="" data-bs-original-title="Gabrielle Aguirre" aria-label="Gabrielle Aguirre">
                      </p>
                      <p class="mb-2"><strong>New Writer Onboarding </strong><span class="float-end text-gray-500 text-sm">3 days ago </span></p>
                      <p class="card-text"><img class="avatar avatar-sm avatar-stacked p-1" src="imgs/avatar-2.jpg" alt="User" data-bs-toggle="tooltip" data-placement="top" title="" data-bs-original-title="Nielsen Cobb" aria-label="Nielsen Cobb"><img class="avatar avatar-sm avatar-stacked p-1" src="imgs/avatar-3.jpg" alt="User" data-bs-toggle="tooltip" data-placement="top" title="" data-bs-original-title="Margret Cote" aria-label="Margret Cote"><img class="avatar avatar-sm avatar-stacked p-1" src="imgs/avatar-4.jpg" alt="User" data-bs-toggle="tooltip" data-placement="top" title="" data-bs-original-title="Rachel Vinson" aria-label="Rachel Vinson">
                      </p>
                      <p class="mb-2"><strong>Blog Post Drafts </strong><span class="float-end text-gray-500 text-sm">5 days ago </span></p>
                      <p class="card-text"><img class="avatar avatar-sm avatar-stacked p-1" src="imgs/avatar-3.jpg" alt="User" data-bs-toggle="tooltip" data-placement="top" title="" data-bs-original-title="Nielsen Cobb" aria-label="Nielsen Cobb"><img class="avatar avatar-sm avatar-stacked p-1" src="imgs/avatar-4.jpg" alt="User" data-bs-toggle="tooltip" data-placement="top" title="" data-bs-original-title="Margret Cote" aria-label="Margret Cote"><img class="avatar avatar-sm avatar-stacked p-1" src="imgs/avatar-5.jpg" alt="User" data-bs-toggle="tooltip" data-placement="top" title="" data-bs-original-title="Rachel Vinson" aria-label="Rachel Vinson"><img class="avatar avatar-sm avatar-stacked p-1" src="imgs/avatar-6.jpg" alt="User" data-bs-toggle="tooltip" data-placement="top" title="" data-bs-original-title="Gabrielle Aguirre" aria-label="Gabrielle Aguirre"><img class="avatar avatar-sm avatar-stacked p-1" src="imgs/avatar-7.jpg" alt="User" data-bs-toggle="tooltip" data-placement="top" title="" data-bs-original-title="Spears Collier" aria-label="Spears Collier">
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <!-- </Projects Widget>-->
              <div class="col-lg-4 mb-4 mb-lg-0">
                <div class="card h-100">
                  <div class="card-header">
                    <h5 class="card-heading">Closed projects</h5>
                        <div class="card-header-more">
                          <button class="btn-header-more" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></button>
                          <div class="dropdown-menu dropdown-menu-end text-sm"><a class="dropdown-item" href="#!"><i class="fas fa-expand-arrows-alt opacity-5 me-2"></i>Expand</a><a class="dropdown-item" href="#!"><i class="far fa-window-minimize opacity-5 me-2"></i>Minimize</a><a class="dropdown-item" href="#!"><i class="fas fa-redo opacity-5 me-2"></i> Reload</a><a class="dropdown-item" href="#!"><i class="far fa-trash-alt opacity-5 me-2"></i> Remove</a></div>
                        </div>
                  </div>
                  <div class="card-body d-flex align-items-center"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                    <canvas id="donut3" width="401" height="200" style="display: block; height: 160px; width: 321px;" class="chartjs-render-monitor"></canvas>
                  </div>
                  <div class="card-footer bg-white">
                    <h3 class="subtitle text-gray-500 fw-normal text-center">Total closed projects</h3>
                    <div class="row justify-content-center align-items-center">
                      <div class="col-auto">
                        <div class="h4 mb-0">2,235</div>
                      </div>
                      <div class="col-auto"> <span class="text-muted me-2">+128</span><span class="badge badge-success-light"><i class="fas fa-arrow-up me-2"></i>21.3%</span></div>
                    </div>
                    <div class="row mt-4 card-text text-sm justify-content-center">
                      <div class="col-auto"><span class="indicator" style="background: #0d6efd"> </span><span class="text-gray-500">Sandra</span>
                        <div class="ms-3 h6">250</div>
                      </div>
                      <div class="col-auto"><span class="indicator" style="background: #3d8bfd"> </span><span class="text-gray-500">Becky</span>
                        <div class="ms-3 h6">50</div>
                      </div>
                      <div class="col-auto"><span class="indicator" style="background: #6ea8fe"> </span><span class="text-gray-500">Julie</span>
                        <div class="ms-3 h6">100</div>
                      </div>
                      <div class="col-auto"><span class="indicator" style="background: #9ec5fe"> </span><span class="text-gray-500">Romero</span>
                        <div class="ms-3 h6">40</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="card h-100">
                  <div class="card-header">
                    <h5 class="card-heading">Tickets solved</h5>
                        <div class="card-header-more">
                          <button class="btn-header-more" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></button>
                          <div class="dropdown-menu dropdown-menu-end text-sm"><a class="dropdown-item" href="#!"><i class="fas fa-expand-arrows-alt opacity-5 me-2"></i>Expand</a><a class="dropdown-item" href="#!"><i class="far fa-window-minimize opacity-5 me-2"></i>Minimize</a><a class="dropdown-item" href="#!"><i class="fas fa-redo opacity-5 me-2"></i> Reload</a><a class="dropdown-item" href="#!"><i class="far fa-trash-alt opacity-5 me-2"></i> Remove</a></div>
                        </div>
                  </div>
                  <div class="card-body d-flex align-items-center"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                    <canvas id="pieChartCustom3" width="401" height="200" style="display: block; height: 160px; width: 321px;" class="chartjs-render-monitor"></canvas>
                  </div>
                  <div class="card-footer bg-white">
                    <h3 class="subtitle text-gray-500 fw-normal text-center">Tickets solved</h3>
                    <div class="row justify-content-center align-items-center">
                      <div class="col-auto">
                        <div class="h4 mb-0">530</div>
                      </div>
                      <div class="col-auto"> <span class="text-muted me-2">-85</span><span class="badge badge-danger-light"><i class="fas fa-arrow-down me-2"></i>-15.6%</span></div>
                    </div>
                    <div class="row mt-4 card-text text-sm justify-content-center">
                      <div class="col-auto"><span class="indicator" style="background: #6610f2"> </span><span class="text-gray-500">John</span>
                        <div class="ms-3 h6">300</div>
                      </div>
                      <div class="col-auto"><span class="indicator" style="background: #8540f5"> </span><span class="text-gray-500">Mark</span>
                        <div class="ms-3 h6">50</div>
                      </div>
                      <div class="col-auto"><span class="indicator" style="background: #a370f7"> </span><span class="text-gray-500">Frank</span>
                        <div class="ms-3 h6">100</div>
                      </div>
                      <div class="col-auto"><span class="indicator" style="background: #c29ffa"> </span><span class="text-gray-500">Danny</span>
                        <div class="ms-3 h6">80</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          
            <div class="row mb-3">
              <!-- <At a glance>-->
              <div class="col-lg-5">
                <div class="card mb-4">
                  <div class="card-header">
                    <h5 class="card-heading">At a glance</h5>
                        <div class="card-header-more">
                          <button class="btn-header-more" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></button>
                          <div class="dropdown-menu dropdown-menu-end text-sm"><a class="dropdown-item" href="#!"><i class="fas fa-expand-arrows-alt opacity-5 me-2"></i>Expand</a><a class="dropdown-item" href="#!"><i class="far fa-window-minimize opacity-5 me-2"></i>Minimize</a><a class="dropdown-item" href="#!"><i class="fas fa-redo opacity-5 me-2"></i> Reload</a><a class="dropdown-item" href="#!"><i class="far fa-trash-alt opacity-5 me-2"></i> Remove        </a></div>
                        </div>
                  </div>
                  <div class="card-body">
                    <ul class="row list-unstyled card-text gy-4">
                      <li class="col-sm-6 d-flex align-items-center">
                        <div class="icon icon-md me-2 bg-blue-light">
                              <svg class="svg-icon text-blue svg-icon-sm">
                                <use xlink:href="icons/orion-svg-sprite.71e9f5f2.svg#reading-1"> </use>
                              </svg>
                        </div><a class="text-dark text-sm" href="#!"><strong>112</strong> Posts</a>
                      </li>
                      <li class="col-sm-6 d-flex align-items-center">
                        <div class="icon icon-md me-2 bg-blue-light">
                              <svg class="svg-icon text-blue svg-icon-sm">
                                <use xlink:href="icons/orion-svg-sprite.71e9f5f2.svg#news-1"> </use>
                              </svg>
                        </div><a class="text-dark text-sm" href="#!"><strong>5</strong> Pages</a>
                      </li>
                      <li class="col-sm-6 d-flex align-items-center">
                        <div class="icon icon-md me-2 bg-blue-light">
                              <svg class="svg-icon text-blue svg-icon-sm">
                                <use xlink:href="icons/orion-svg-sprite.71e9f5f2.svg#chat-bubble-1"> </use>
                              </svg>
                        </div><a class="text-dark text-sm" href="#!"><strong>283</strong> Comments</a>
                      </li>
                      <li class="col-sm-6 d-flex align-items-center">
                        <div class="icon icon-md me-2 bg-blue-light">
                              <svg class="svg-icon text-blue svg-icon-sm">
                                <use xlink:href="icons/orion-svg-sprite.71e9f5f2.svg#time-1"> </use>
                              </svg>
                        </div><a class="text-dark text-sm" href="#!"><strong>4</strong> Comments in moderation</a>
                      </li>
                    </ul>
                  </div>
                  <div class="card-footer py-4">
                    <p class="text-muted card-text text-sm">WordPress 5.9 running <a href="#!">Bubbly theme</a>.</p>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header">
                    <h5 class="card-heading">Site health status</h5>
                        <div class="card-header-more">
                          <button class="btn-header-more" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></button>
                          <div class="dropdown-menu dropdown-menu-end text-sm"><a class="dropdown-item" href="#!"><i class="fas fa-expand-arrows-alt opacity-5 me-2"></i>Expand</a><a class="dropdown-item" href="#!"><i class="far fa-window-minimize opacity-5 me-2"></i>Minimize</a><a class="dropdown-item" href="#!"><i class="fas fa-redo opacity-5 me-2"></i> Reload</a><a class="dropdown-item" href="#!"><i class="far fa-trash-alt opacity-5 me-2"></i> Remove        </a></div>
                        </div>
                  </div>
                  <div class="card-body">
                    <div class="d-md-flex align-items-center">
                      <div class="icon icon-xl bg-orange-light mx-auto me-3">
                            <svg class="svg-icon text-orange">
                              <use xlink:href="icons/orion-svg-sprite.71e9f5f2.svg#first-aid-kit-1"> </use>
                            </svg>
                      </div>
                      <div class="text-muted">
                        <p class="card-text">Your site has critical issues that should be addressed as soon as possible to improve its performance and security.</p>
                        <p class="card-text">Take a look at the 8 items on the <a href="#!">Site Health screen</a>.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- </At a glance>-->
              <div class="col-lg-7 mb-5">
                <div class="card">
                  <div class="card-header">
                    <h5 class="card-heading">Quick Draft</h5>
                        <div class="card-header-more">
                          <button class="btn-header-more" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></button>
                          <div class="dropdown-menu dropdown-menu-end text-sm"><a class="dropdown-item" href="#!"><i class="fas fa-expand-arrows-alt opacity-5 me-2"></i>Expand</a><a class="dropdown-item" href="#!"><i class="far fa-window-minimize opacity-5 me-2"></i>Minimize</a><a class="dropdown-item" href="#!"><i class="fas fa-redo opacity-5 me-2"></i> Reload</a><a class="dropdown-item" href="#!"><i class="far fa-trash-alt opacity-5 me-2"></i> Remove        </a></div>
                        </div>
                  </div>
                  <div class="card-body">
                    <div class="mb-4">
                      <label class="form-label" for="quickDraftTitle">Title</label>
                      <input class="form-control" id="quickDraftTitle" type="text">
                      <div class="form-text">The title is how it appears on your site.</div>
                    </div>
                    <div class="mb-4">
                      <label class="form-label" for="quickDraftText">Content</label>
                      <textarea class="form-control" id="quickDraftText" name="quickDraftText" rows="5" placeholder="What's on your mind?"></textarea>
                      <div class="form-text">The description is not prominent by default; however, some themes may show it.</div>
                    </div>
                    <button class="btn btn-primary">Save draft</button>
                  </div>
                  <div class="card-footer py-4">
                    <h6>Your Recent Drafts</h6>
                    <ul class="list-unstyled text-sm card-text">
                      <li class="mb-2"><a href="#!">Gear</a>
                        <time class="text-muted">January 5, 2022</time>
                      </li>
                      <li> <a href="#!">Stories</a>
                        <time class="text-muted">January 2, 2022</time>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-lg-8"> 
                <div class="card card-table">
                  <div class="card-header">
                    <h5 class="card-heading"> Latest Posts</h5>
                        <div class="card-header-more">
                          <button class="btn-header-more" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></button>
                          <div class="dropdown-menu dropdown-menu-end text-sm"><a class="dropdown-item" href="#!"><i class="fas fa-expand-arrows-alt opacity-5 me-2"></i>Expand</a><a class="dropdown-item" href="#!"><i class="far fa-window-minimize opacity-5 me-2"></i>Minimize</a><a class="dropdown-item" href="#!"><i class="fas fa-redo opacity-5 me-2"></i> Reload</a><a class="dropdown-item" href="#!"><i class="far fa-trash-alt opacity-5 me-2"></i> Remove        </a></div>
                        </div>
                  </div>
                  <div class="preload-wrapper text-sm opacity-10">
                    <div class="dataTable-wrapper dataTable-loading no-footer sortable searchable fixed-columns"><div class="dataTable-top"><div class="dataTable-dropdown"><span class="me-2" id="categoryBulkAction">
                      <select class="form-select form-select-sm d-inline w-auto" name="categoryBulkAction">
                        <option>Bulk Actions</option>
                        <option>Delete</option>
                      </select>
                      <button class="btn btn-sm btn-outline-primary align-top">Apply</button></span><label><select class="dataTable-selector form-select form-select-sm"><option value="5" selected="">5</option><option value="10">10</option><option value="15">15</option><option value="20">20</option><option value="25">25</option></select> entries per page</label></div><div class="dataTable-search"><input class="dataTable-input form-control form-control-sm" placeholder="Search..." type="text"></div></div><div class="dataTable-container border-0"><table class="table table-hover align-middle mb-0 dataTable-table" id="postDatatable">
                      <thead>
                        <tr><th data-sortable="false" style="width: 9.81456%;"> </th><th data-sortable="" style="width: 30.3245%;"><a href="#" class="dataTable-sorter">Title</a></th><th data-sortable="" style="width: 20.1324%;"><a href="#" class="dataTable-sorter">Author</a></th><th data-sortable="" style="width: 21.6424%;"><a href="#" class="dataTable-sorter">Categories</a></th><th data-sortable="" style="width: 18.1192%;"><a href="#" class="dataTable-sorter">Date</a></th></tr>
                      </thead>
                      <tbody><tr><td><span class="form-check"><input class="form-check-input" type="checkbox"></span></td><td> <a class="text-reset text-decoration-none" href="cms-post-new.html"><img class="img-fluid rounded me-3" src="https://d19m59y37dris4.cloudfront.net/bubbly/1-3/img/photos/w-100/soroush-zargar-zFSUhqGual8-unsplash.jpg" alt="Memora" width="75"><strong>Memora</strong></a></td><td>Nielsen Cobb</td><td>Gear</td><td>2021/10/24</td></tr><tr><td><span class="form-check"><input class="form-check-input" type="checkbox"></span></td><td> <a class="text-reset text-decoration-none" href="cms-post-new.html"><img class="img-fluid rounded me-3" src="https://d19m59y37dris4.cloudfront.net/bubbly/1-3/img/photos/w-100/faruk-kaymak-CNVvDjVDiVI-unsplash.jpg" alt="Zilidium" width="75"><strong>Zilidium</strong></a></td><td>Margret Cote</td><td>Stories</td><td>2021/01/12</td></tr><tr><td><span class="form-check"><input class="form-check-input" type="checkbox"></span></td><td> <a class="text-reset text-decoration-none" href="cms-post-new.html"><img class="img-fluid rounded me-3" src="https://d19m59y37dris4.cloudfront.net/bubbly/1-3/img/photos/w-100/aron-visuals-3jBU9TbKW7o-unsplash.jpg" alt="Chorizon" width="75"><strong>Chorizon</strong></a></td><td>Rachel Vinson</td><td>Tips &amp; Tricks</td><td>2021/09/11</td></tr><tr><td><span class="form-check"><input class="form-check-input" type="checkbox"></span></td><td> <a class="text-reset text-decoration-none" href="cms-post-new.html"><img class="img-fluid rounded me-3" src="https://d19m59y37dris4.cloudfront.net/bubbly/1-3/img/photos/w-100/elizabeth-gottwald-dnIWYrliZfU-unsplash.jpg" alt="Comverges" width="75"><strong>Comverges</strong></a></td><td>Gabrielle Aguirre</td><td>Trips</td><td>2021/03/24</td></tr><tr><td><span class="form-check"><input class="form-check-input" type="checkbox"></span></td><td> <a class="text-reset text-decoration-none" href="cms-post-new.html"><img class="img-fluid rounded me-3" src="https://d19m59y37dris4.cloudfront.net/bubbly/1-3/img/photos/w-100/willian-justen-de-vasconcellos-4hMET7vYTAQ-unsplash.jpg" alt="Remold" width="75"><strong>Remold</strong></a></td><td>Spears Collier</td><td>Gear</td><td>2021/06/21</td></tr></tbody>
                    </table></div><div class="dataTable-bottom"><div class="dataTable-info">Showing 1 to 5 of 20 entries</div><nav class="dataTable-pagination"><ul class="dataTable-pagination-list"><li class="active"><a href="#" data-page="1">1</a></li><li class=""><a href="#" data-page="2">2</a></li><li class=""><a href="#" data-page="3">3</a></li><li class=""><a href="#" data-page="4">4</a></li><li class="pager"><a href="#" data-page="2">›</a></li></ul></nav></div></div>
                  </div>
                </div>
              </div>
              <!-- <Authors>-->
              <div class="col-lg-4">
                <div class="card h-100">
                  <div class="card-header">
                    <h5 class="card-heading"> Popular Authors</h5>
                        <div class="card-header-more">
                          <button class="btn-header-more" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></button>
                          <div class="dropdown-menu dropdown-menu-end text-sm"><a class="dropdown-item" href="#!"><i class="fas fa-expand-arrows-alt opacity-5 me-2"></i>Expand</a><a class="dropdown-item" href="#!"><i class="far fa-window-minimize opacity-5 me-2"></i>Minimize</a><a class="dropdown-item" href="#!"><i class="fas fa-redo opacity-5 me-2"></i> Reload</a><a class="dropdown-item" href="#!"><i class="far fa-trash-alt opacity-5 me-2"></i> Remove        </a></div>
                        </div>
                  </div>
                  <div class="card-body pb-2">
                    <div class="d-flex align-items-center mb-3"><img class="avatar p-1 me-2" src="imgs/avatar-0.jpg" alt="Nielsen Cobb">
                      <div class="mt-1"><a class="text-dark fw-bold text-decoration-none" href="#!">Nielsen Cobb</a>
                        <p class="text-muted text-sm mb-0">Memora</p>
                      </div>
                    </div>
                    <div class="d-flex align-items-center mb-3"><img class="avatar p-1 me-2" src="imgs/avatar-1.jpg" alt="Margret Cote">
                      <div class="mt-1"><a class="text-dark fw-bold text-decoration-none" href="#!">Margret Cote</a>
                        <p class="text-muted text-sm mb-0">Zilidium</p>
                      </div>
                    </div>
                    <div class="d-flex align-items-center mb-3"><img class="avatar p-1 me-2" src="imgs/avatar-2.jpg" alt="Rachel Vinson">
                      <div class="mt-1"><a class="text-dark fw-bold text-decoration-none" href="#!">Rachel Vinson</a>
                        <p class="text-muted text-sm mb-0">Chorizon</p>
                      </div>
                    </div>
                    <div class="d-flex align-items-center mb-3"><img class="avatar p-1 me-2" src="imgs/avatar-3.jpg" alt="Gabrielle Aguirre">
                      <div class="mt-1"><a class="text-dark fw-bold text-decoration-none" href="#!">Gabrielle Aguirre</a>
                        <p class="text-muted text-sm mb-0">Comverges</p>
                      </div>
                    </div>
                    <div class="d-flex align-items-center mb-3"><img class="avatar p-1 me-2" src="imgs/avatar-4.jpg" alt="Spears Collier">
                      <div class="mt-1"><a class="text-dark fw-bold text-decoration-none" href="#!">Spears Collier</a>
                        <p class="text-muted text-sm mb-0">Remold</p>
                      </div>
                    </div>
                    <div class="d-flex align-items-center mb-3"><img class="avatar p-1 me-2" src="imgs/avatar-5.jpg" alt="Keisha Thomas">
                      <div class="mt-1"><a class="text-dark fw-bold text-decoration-none" href="#!">Keisha Thomas</a>
                        <p class="text-muted text-sm mb-0">Euron</p>
                      </div>
                    </div>
                    <div class="d-flex align-items-center mb-3"><img class="avatar p-1 me-2" src="imgs/avatar-6.jpg" alt="Elisabeth Key">
                      <div class="mt-1"><a class="text-dark fw-bold text-decoration-none" href="#!">Elisabeth Key</a>
                        <p class="text-muted text-sm mb-0">Netagy</p>
                      </div>
                    </div>
                    <div class="d-flex align-items-center mb-3"><img class="avatar p-1 me-2" src="imgs/avatar-7.jpg" alt="Patel Mack">
                      <div class="mt-1"><a class="text-dark fw-bold text-decoration-none" href="#!">Patel Mack</a>
                        <p class="text-muted text-sm mb-0">Zedalis</p>
                      </div>
                    </div>
                  </div>
                  <div class="card-footer text-end"><a class="btn btn-outline-primary" href="#!">View all people</a></div>
                </div>
              </div>
              <!-- </Authors>-->
            </div>