<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Acclivis</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="robots" content="noindex">
  <!-- Google fonts - Popppins for copy-->
  <link rel="preconnect" href="https://fonts.gstatic.com/">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;700&amp;display=swap" rel="stylesheet">
  <!-- Font awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="css/prism-toolbar.css">
  <link rel="stylesheet" href="css/prism-okaidia.css">
  <!-- The Main Theme stylesheet (Contains also Bootstrap CSS)-->
  <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
  <!-- Custom stylesheet - for your changes-->
  <link rel="stylesheet" href="css/custom.css">
  <link rel="stylesheet" href="css/editor.css">
  <link rel="stylesheet" href="css/skin.min.css">

  <!-- Favicon-->
  <link rel="shortcut icon" href="imgs/favicon.png">
</head>

<body>
  <!-- navbar-->
  <?php
    include 'header.php';
  ?>
  <div class="d-flex align-items-stretch">
    <?php
      include 'sidebar.php';
    ?>

    <div class="page-holder bg-gray-100">
      <div class="container-fluid px-lg-4 px-xl-5">
        <!-- Page Header-->
        <div class="page-header">
          <h1 class="page-heading"><?php echo $headtitle; ?></h1>
        </div>
        <section class="mb-3 mb-lg-5">
          <?php
            include $file;
          ?>
        </section>
      </div>
      <?php
        include 'footer.php';
      ?>
    </div>
  </div>
  <!-- JavaScript files-->
  <script src="js/bootstrap.bundle.min.js"></script>
  <script src="js/js.cookie.min.js"></script>
  <!-- Data Tables-->
  <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest"></script>
  <!-- Init Charts on Homepage-->
  <script src="js/Chart.min.js"></script>
  <script src="js/charts-defaults.js"></script>
  <script src="js/index-default.js"></script>
  <!-- Main Theme JS File-->
  <script src="js/theme.js"></script>
  <!-- Prism for syntax highlighting-->
  <script src="js/prism.js"></script>
  <script src="js/prism-normalize-whitespace.min.js"></script>
  <script src="js/prism-toolbar.min.js"></script>
  <script src="js/prism-copy-to-clipboard.min.js"></script>
  <script type="text/javascript">
    // Optional
    Prism.plugins.NormalizeWhitespace.setDefaults({
      'remove-trailing': true,
      'remove-indent': true,
      'left-trim': true,
      'right-trim': true,
    });
  </script>
  <!-- FontAwesome CSS - loading as last, so it doesn't block rendering-->
  <link rel="stylesheet" href="css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
</body>

</html>