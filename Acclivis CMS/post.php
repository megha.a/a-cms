<div class="card card-table">
  <div class="preload-wrapper opacity-10">
    <div class="dataTable-wrapper dataTable-loading no-footer sortable searchable fixed-columns">
      <div class="dataTable-top">
        <div class="dataTable-dropdown"><span class="me-2" id="categoryBulkAction">
            <select class="form-select form-select-sm d-inline w-auto" name="categoryBulkAction">
              <option>Bulk Actions</option>
              <option>Delete</option>
            </select>
            <button class="btn btn-sm btn-outline-primary align-top">Apply</button></span><label><select class="dataTable-selector form-select form-select-sm">
              <option value="5">5</option>
              <option value="10" selected="">10</option>
              <option value="15">15</option>
              <option value="20">20</option>
              <option value="25">25</option>
            </select> entries per page</label></div>
        <div class="dataTable-search"><input class="dataTable-input form-control form-control-sm" placeholder="Search..." type="text"></div>
      </div>
      <div class="dataTable-container border-0">
        <table class="table table-hover align-middle mb-0 dataTable-table" id="postDatatable">
          <thead>
            <tr>
              <th data-sortable="false"> </th>
              <th data-sortable=""><a href="#" class="dataTable-sorter">Title</a></th>
              <th data-sortable=""><a href="#" class="dataTable-sorter">Author</a></th>
              <th data-sortable=""><a href="#" class="dataTable-sorter">Categories</a></th>
              <th data-sortable=""><a href="#" class="dataTable-sorter">Date</a></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><span class="form-check"><input class="form-check-input" type="checkbox"></span></td>
              <td> <a class="text-reset text-decoration-none" href="cms-post-new.html"><img class="img-fluid rounded me-3" src="https://d19m59y37dris4.cloudfront.net/bubbly/1-3/img/photos/w-100/soroush-zargar-zFSUhqGual8-unsplash.jpg" alt="Memora" width="100"><strong>Memora</strong></a></td>
              <td>Nielsen Cobb</td>
              <td>Gear</td>
              <td>2020/07/16</td>
            </tr>
            <tr>
              <td><span class="form-check"><input class="form-check-input" type="checkbox"></span></td>
              <td> <a class="text-reset text-decoration-none" href="cms-post-new.html"><img class="img-fluid rounded me-3" src="https://d19m59y37dris4.cloudfront.net/bubbly/1-3/img/photos/w-100/faruk-kaymak-CNVvDjVDiVI-unsplash.jpg" alt="Zilidium" width="100"><strong>Zilidium</strong></a></td>
              <td>Margret Cote</td>
              <td>Stories</td>
              <td>2020/05/09</td>
            </tr>
            <tr>
              <td><span class="form-check"><input class="form-check-input" type="checkbox"></span></td>
              <td> <a class="text-reset text-decoration-none" href="cms-post-new.html"><img class="img-fluid rounded me-3" src="https://d19m59y37dris4.cloudfront.net/bubbly/1-3/img/photos/w-100/aron-visuals-3jBU9TbKW7o-unsplash.jpg" alt="Chorizon" width="100"><strong>Chorizon</strong></a></td>
              <td>Rachel Vinson</td>
              <td>Tips &amp; Tricks</td>
              <td>2020/01/21</td>
            </tr>
            <tr>
              <td><span class="form-check"><input class="form-check-input" type="checkbox"></span></td>
              <td> <a class="text-reset text-decoration-none" href="cms-post-new.html"><img class="img-fluid rounded me-3" src="https://d19m59y37dris4.cloudfront.net/bubbly/1-3/img/photos/w-100/elizabeth-gottwald-dnIWYrliZfU-unsplash.jpg" alt="Comverges" width="100"><strong>Comverges</strong></a></td>
              <td>Gabrielle Aguirre</td>
              <td>Trips</td>
              <td>2020/11/11</td>
            </tr>
            <tr>
              <td><span class="form-check"><input class="form-check-input" type="checkbox"></span></td>
              <td> <a class="text-reset text-decoration-none" href="cms-post-new.html"><img class="img-fluid rounded me-3" src="https://d19m59y37dris4.cloudfront.net/bubbly/1-3/img/photos/w-100/willian-justen-de-vasconcellos-4hMET7vYTAQ-unsplash.jpg" alt="Remold" width="100"><strong>Remold</strong></a></td>
              <td>Spears Collier</td>
              <td>Gear</td>
              <td>2020/03/17</td>
            </tr>
            <tr>
              <td><span class="form-check"><input class="form-check-input" type="checkbox"></span></td>
              <td> <a class="text-reset text-decoration-none" href="cms-post-new.html"><img class="img-fluid rounded me-3" src="https://d19m59y37dris4.cloudfront.net/bubbly/1-3/img/photos/w-100/sorasak-_UIN-pFfJ7c-unsplash.jpg" alt="Euron" width="100"><strong>Euron</strong></a></td>
              <td>Keisha Thomas</td>
              <td>Stories</td>
              <td>2020/11/06</td>
            </tr>
            <tr>
              <td><span class="form-check"><input class="form-check-input" type="checkbox"></span></td>
              <td> <a class="text-reset text-decoration-none" href="cms-post-new.html"><img class="img-fluid rounded me-3" src="https://d19m59y37dris4.cloudfront.net/bubbly/1-3/img/photos/w-100/montylov-ktZZiHb-GoI-unsplash.jpg" alt="Netagy" width="100"><strong>Netagy</strong></a></td>
              <td>Elisabeth Key</td>
              <td>Tips &amp; Tricks</td>
              <td>2020/07/05</td>
            </tr>
            <tr>
              <td><span class="form-check"><input class="form-check-input" type="checkbox"></span></td>
              <td> <a class="text-reset text-decoration-none" href="cms-post-new.html"><img class="img-fluid rounded me-3" src="https://d19m59y37dris4.cloudfront.net/bubbly/1-3/img/photos/w-100/joseph-barrientos-Ji_G7Bu1MoM-unsplash.jpg" alt="Zedalis" width="100"><strong>Zedalis</strong></a></td>
              <td>Patel Mack</td>
              <td>Trips</td>
              <td>2020/03/18</td>
            </tr>
            <tr>
              <td><span class="form-check"><input class="form-check-input" type="checkbox"></span></td>
              <td> <a class="text-reset text-decoration-none" href="cms-post-new.html"><img class="img-fluid rounded me-3" src="https://d19m59y37dris4.cloudfront.net/bubbly/1-3/img/photos/w-100/henrique-ferreira-RKsLQoSnuTc-unsplash.jpg" alt="Uniworld" width="100"><strong>Uniworld</strong></a></td>
              <td>Erika Whitaker</td>
              <td>Uncategorized</td>
              <td>2020/01/15</td>
            </tr>
            <tr>
              <td><span class="form-check"><input class="form-check-input" type="checkbox"></span></td>
              <td> <a class="text-reset text-decoration-none" href="cms-post-new.html"><img class="img-fluid rounded me-3" src="https://d19m59y37dris4.cloudfront.net/bubbly/1-3/img/photos/w-100/luca-bravo-O453M2Liufs-unsplash.jpg" alt="Candecor" width="100"><strong>Candecor</strong></a></td>
              <td>Meyers Swanson</td>
              <td>Gear</td>
              <td>2020/10/28</td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="dataTable-bottom">
        <div class="dataTable-info">Showing 1 to 10 of 100 entries</div>
        <nav class="dataTable-pagination">
          <ul class="dataTable-pagination-list">
            <li class="active"><a href="#" data-page="1">1</a></li>
            <li class=""><a href="#" data-page="2">2</a></li>
            <li class=""><a href="#" data-page="3">3</a></li>
            <li class=""><a href="#" data-page="4">4</a></li>
            <li class=""><a href="#" data-page="5">5</a></li>
            <li class=""><a href="#" data-page="6">6</a></li>
            <li class=""><a href="#" data-page="7">7</a></li>
            <li class="ellipsis"><a href="#">…</a></li>
            <li class=""><a href="#" data-page="10">10</a></li>
            <li class="pager"><a href="#" data-page="2">›</a></li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
</div>