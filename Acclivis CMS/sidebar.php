<?php
?>
<div class="sidebar py-3" id="sidebar">
        <h6 class="sidebar-heading">Main</h6>
        <ul class="list-unstyled">
              <li class="sidebar-list-item"><a class="sidebar-link text-muted active" href="#" data-bs-target="#dashboardsDropdown" role="button" aria-expanded="true" data-bs-toggle="collapse"> 
                      <svg class="svg-icon svg-icon-md me-3">
                        <use xlink:href="https://demo.bootstrapious.com/bubbly/1-3/icons/orion-svg-sprite.71e9f5f2.svg#real-estate-1"> </use>
                      </svg><span class="sidebar-link-title">Dashboards </span></a>
                <ul class="sidebar-menu list-unstyled collapse show" id="dashboardsDropdown">
                  <li class="sidebar-list-item"><a class="sidebar-link active text-muted" href="index.html">Default</a></li>
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="index.php?action=cms">CMS</a></li>
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="index.php?action=e-commerce">E-commerce</a></li>
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="index.php?action=projects">Projects</a></li>
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="index.php?action=charts">Charts</a></li>
                </ul>
              </li>
              <li class="sidebar-list-item"><a class="sidebar-link text-muted " href="#" data-bs-target="#cmsDropdown" role="button" aria-expanded="false" data-bs-toggle="collapse"> 
                      <svg class="svg-icon svg-icon-md me-3">
                        <use xlink:href="https://demo.bootstrapious.com/bubbly/1-3/icons/orion-svg-sprite.71e9f5f2.svg#reading-1"> </use>
                      </svg><span class="sidebar-link-title">CMS </span></a>
                <ul class="sidebar-menu list-unstyled collapse " id="cmsDropdown">
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="index.php?action=post">Posts</a></li>
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="index.php?action=post-new">Add new post</a></li>
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="index.php?action=category">Categories</a></li>
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="index.php?action=media">Media library</a></li>
                </ul>
              </li>
              <li class="sidebar-list-item"><a class="sidebar-link text-muted " href="#" data-bs-target="#widgetsDropdown" role="button" aria-expanded="false" data-bs-toggle="collapse"> 
                      <svg class="svg-icon svg-icon-md me-3">
                        <use xlink:href="https://demo.bootstrapious.com/bubbly/1-3/icons/orion-svg-sprite.71e9f5f2.svg#statistic-1"> </use>
                      </svg><span class="sidebar-link-title">Widgets </span></a>
                <ul class="sidebar-menu list-unstyled collapse " id="widgetsDropdown">
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="widgets-stats.html">Stats</a></li>
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="widgets-data.html">Data</a></li>
                </ul>
              </li>
              <li class="sidebar-list-item"><a class="sidebar-link text-muted " href="#" data-bs-target="#e-commerceDropdown" role="button" aria-expanded="false" data-bs-toggle="collapse"> 
                      <svg class="svg-icon svg-icon-md me-3">
                        <use xlink:href="https://demo.bootstrapious.com/bubbly/1-3/icons/orion-svg-sprite.71e9f5f2.svg#delivery-truck-1"> </use>
                      </svg><span class="sidebar-link-title">E-commerce </span></a>
                <ul class="sidebar-menu list-unstyled collapse " id="e-commerceDropdown">
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="index.php?action=e-commerce-products">Products</a></li>
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="index.php?action=e-commerce-product-new">Products - New</a></li>
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="index.php?action=e-commerce-orders">Orders</a></li>
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="index.php?action=e-commerce-order">Order - Detail</a></li>
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="index.php?action=e-commerce-customers">Customers</a></li>
                </ul>
              </li>
              <li class="sidebar-list-item"><a class="sidebar-link text-muted " href="#" data-bs-target="#pagesDropdown" role="button" aria-expanded="false" data-bs-toggle="collapse"> 
                      <svg class="svg-icon svg-icon-md me-3">
                        <use xlink:href="https://demo.bootstrapious.com/bubbly/1-3/icons/orion-svg-sprite.71e9f5f2.svg#paper-stack-1"> </use>
                      </svg><span class="sidebar-link-title">Pages </span></a>
                <ul class="sidebar-menu list-unstyled collapse " id="pagesDropdown">
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="index.php?action=all-pages">All Pages</a></li>
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="index.php?action=add-new-page">Add New Page</a></li>
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="index.php?action=pages-profile">Profile</a></li>
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="index.php?action=pages-pricing">Pricing table</a></li>
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="index.php?action=pages-contacts">Contacts</a></li>
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="index.php?action=pages-invoice">Invoice</a></li>
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="index.php?action=pages-knowledge-base">Knowledge base</a></li>
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="index.php?action=pages-knowledge-base-topic">Knowledge base - Topic</a></li>
                </ul>
              </li>
              <li class="sidebar-list-item"><a class="sidebar-link text-muted " href="#" data-bs-target="#userDropdown" role="button" aria-expanded="false" data-bs-toggle="collapse"> 
                      <svg class="svg-icon svg-icon-md me-3">
                        <use xlink:href="https://demo.bootstrapious.com/bubbly/1-3/icons/orion-svg-sprite.71e9f5f2.svg#man-1"> </use>
                      </svg><span class="sidebar-link-title">User </span></a>
                <ul class="sidebar-menu list-unstyled collapse " id="userDropdown">
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="login.html">Login page</a></li>
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="register.html">Register</a></li>
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="login-2.html">Login v.2 <span class="badge bg-info ms-2 text-decoration-none">New</span></a></li>
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="register-2.html">Register v.2 <span class="badge bg-info ms-2 text-decoration-none">New</span></a></li>
                </ul>
              </li>
              <li class="sidebar-list-item"><a class="sidebar-link text-muted " href="#" data-bs-target="#componentsDropdown" role="button" aria-expanded="false" data-bs-toggle="collapse"> 
                      <svg class="svg-icon svg-icon-md me-3">
                        <use xlink:href="https://demo.bootstrapious.com/bubbly/1-3/icons/orion-svg-sprite.71e9f5f2.svg#sorting-1"> </use>
                      </svg><span class="sidebar-link-title">Components </span></a>
                <ul class="sidebar-menu list-unstyled collapse " id="componentsDropdown">
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="components-cards.html">Cards</a></li>
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="components-calendar.html">Calendar</a></li>
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="components-gallery.html">Gallery</a></li>
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="components-loading-buttons.html">Loading buttons</a></li>
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="components-map.html">Maps</a></li>
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="components-notifications.html">Notifications</a></li>
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="components-preloader.html">Preloaders</a></li>
                </ul>
              </li>
              <li class="sidebar-list-item"><a class="sidebar-link text-muted " href="#" data-bs-target="#chartsDropdown" role="button" aria-expanded="false" data-bs-toggle="collapse"> 
                      <svg class="svg-icon svg-icon-md me-3">
                        <use xlink:href="https://demo.bootstrapious.com/bubbly/1-3/icons/orion-svg-sprite.71e9f5f2.svg#pie-chart-1"> </use>
                      </svg><span class="sidebar-link-title">Charts </span></a>
                <ul class="sidebar-menu list-unstyled collapse " id="chartsDropdown">
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="charts.html">Charts</a></li>
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="charts-gauge-sparkline.html">Gauge + Sparkline</a></li>
                </ul>
              </li>
              <li class="sidebar-list-item"><a class="sidebar-link text-muted " href="#" data-bs-target="#formsDropdown" role="button" aria-expanded="false" data-bs-toggle="collapse"> 
                      <svg class="svg-icon svg-icon-md me-3">
                        <use xlink:href="https://demo.bootstrapious.com/bubbly/1-3/icons/orion-svg-sprite.71e9f5f2.svg#file-storage-1"> </use>
                      </svg><span class="sidebar-link-title">Forms </span></a>
                <ul class="sidebar-menu list-unstyled collapse " id="formsDropdown">
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="forms.html">Basic forms</a></li>
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="forms-advanced.html">Advanced forms</a></li>
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="forms-dropzone.html">Files upload</a></li>
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="forms-texteditor.html">Text editor</a></li>
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="forms-validation.html">Validation</a></li>
                </ul>
              </li>
              <li class="sidebar-list-item"><a class="sidebar-link text-muted " href="#" data-bs-target="#tablesDropdown" role="button" aria-expanded="false" data-bs-toggle="collapse"> 
                      <svg class="svg-icon svg-icon-md me-3">
                        <use xlink:href="https://demo.bootstrapious.com/bubbly/1-3/icons/orion-svg-sprite.71e9f5f2.svg#grid-1"> </use>
                      </svg><span class="sidebar-link-title">Tables </span></a>
                <ul class="sidebar-menu list-unstyled collapse " id="tablesDropdown">
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="tables.html">Bootstrap tables</a></li>
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="tables-datatable.html">Datatable</a></li>
                </ul>
              </li>
        </ul>
        <h6 class="sidebar-heading">Docs</h6>
        <ul class="list-unstyled">
              <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="introduction.html">
                      <svg class="svg-icon svg-icon-md me-3">
                        <use xlink:href="https://demo.bootstrapious.com/bubbly/1-3/icons/orion-svg-sprite.71e9f5f2.svg#angle-brackets-1"> </use>
                      </svg><span class="sidebar-link-title">Introduction</span></a></li>
              <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="directory-structure.html">
                      <svg class="svg-icon svg-icon-md me-3">
                        <use xlink:href="https://demo.bootstrapious.com/bubbly/1-3/icons/orion-svg-sprite.71e9f5f2.svg#table-content-1"> </use>
                      </svg><span class="sidebar-link-title">Directory structure</span></a></li>
              <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="gulp.html">
                      <svg class="svg-icon svg-icon-md me-3">
                        <use xlink:href="https://demo.bootstrapious.com/bubbly/1-3/icons/orion-svg-sprite.71e9f5f2.svg#keyboard-1"> </use>
                      </svg><span class="sidebar-link-title">Gulp.js</span></a></li>
              <li class="sidebar-list-item"><a class="sidebar-link text-muted " href="#" data-bs-target="#cssDropdown" role="button" aria-expanded="false" data-bs-toggle="collapse"> 
                      <svg class="svg-icon svg-icon-md me-3">
                        <use xlink:href="https://demo.bootstrapious.com/bubbly/1-3/icons/orion-svg-sprite.71e9f5f2.svg#design-1"> </use>
                      </svg><span class="sidebar-link-title">CSS </span></a>
                <ul class="sidebar-menu list-unstyled collapse " id="cssDropdown">
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="components-theme.html">CSS Components</a></li>
                  <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="customizing-css.html">Customizing CSS</a></li>
                </ul>
              </li>
              <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="credits.html">
                      <svg class="svg-icon svg-icon-md me-3">
                        <use xlink:href="https://demo.bootstrapious.com/bubbly/1-3/icons/orion-svg-sprite.71e9f5f2.svg#star-medal-1"> </use>
                      </svg><span class="sidebar-link-title">Credits</span></a></li>
              <li class="sidebar-list-item"><a class="sidebar-link text-muted" href="changelog.html">
                      <svg class="svg-icon svg-icon-md me-3">
                        <use xlink:href="https://demo.bootstrapious.com/bubbly/1-3/icons/orion-svg-sprite.71e9f5f2.svg#new-1"> </use>
                      </svg><span class="sidebar-link-title">Changelog</span></a></li>
        </ul>
      </div> 