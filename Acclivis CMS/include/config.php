<?php 
$host = 'localhost';
$user = 'root';
$pass = '';
$database = 'accliviscms';

$base_url='https://'.$_SERVER['HTTP_HOST'].'/';

$conn = new mysqli($host, $user, $pass, $database);
$conn->set_charset("utf8");
return $conn;
?>