<?php include 'toolbar.php';
?>
<div class="row mb-5">
    <div class="col-lg-8 col-xxl-9 mb-4 mb-lg-0">
        <div class="card">
            <div class="card-body">
                <!-- Accordion Start -->

                <div id="accordion">
                    <div class="card">
                        <div class="card-header">
                            <a class="card-link" data-toggle="collapse" href="#collapseOne">Banner</a>
                        </div>
                        <div id="collapseOne" class="collapse show" data-parent="#accordion">
                            <div class="card-body">
                                <label class="form-label" for="bannerName">Title</label>
                                <input class="form-control mb-4" id="bannerName" type="text">

                                <label class="form-label" for="bannerDesc">Description</label>
                                <div id="bannerDesc">
                                    <div id="toolbar-container" class="ql-toolbar ql-snow"><span class="ql-formats">
                                            <span class="ql-font ql-picker"><span class="ql-picker-label" tabindex="0" role="button" aria-expanded="false" aria-controls="ql-picker-options-0"><svg viewBox="0 0 18 18">
                                                        <polygon class="ql-stroke" points="7 11 9 13 11 11 7 11"></polygon>
                                                        <polygon class="ql-stroke" points="7 7 9 5 11 7 7 7"></polygon>
                                                    </svg></span><span class="ql-picker-options" aria-hidden="true" tabindex="-1" id="ql-picker-options-0"><span tabindex="0" role="button" class="ql-picker-item"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="serif"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="monospace"></span></span></span><select class="ql-font" style="display: none;">
                                                <option selected="selected"></option>
                                                <option value="serif"></option>
                                                <option value="monospace"></option>
                                            </select>
                                            <span class="ql-size ql-picker"><span class="ql-picker-label" tabindex="0" role="button" aria-expanded="false" aria-controls="ql-picker-options-1"><svg viewBox="0 0 18 18">
                                                        <polygon class="ql-stroke" points="7 11 9 13 11 11 7 11"></polygon>
                                                        <polygon class="ql-stroke" points="7 7 9 5 11 7 7 7"></polygon>
                                                    </svg></span><span class="ql-picker-options" aria-hidden="true" tabindex="-1" id="ql-picker-options-1"><span tabindex="0" role="button" class="ql-picker-item" data-value="small"></span><span tabindex="0" role="button" class="ql-picker-item"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="large"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="huge"></span></span></span><select class="ql-size" style="display: none;">
                                                <option value="small"></option>
                                                <option selected="selected"></option>
                                                <option value="large"></option>
                                                <option value="huge"></option>
                                            </select></span><span class="ql-formats">
                                            <button class="ql-bold" type="button"><svg viewBox="0 0 18 18">
                                                    <path class="ql-stroke" d="M5,4H9.5A2.5,2.5,0,0,1,12,6.5v0A2.5,2.5,0,0,1,9.5,9H5A0,0,0,0,1,5,9V4A0,0,0,0,1,5,4Z"></path>
                                                    <path class="ql-stroke" d="M5,9h5.5A2.5,2.5,0,0,1,13,11.5v0A2.5,2.5,0,0,1,10.5,14H5a0,0,0,0,1,0,0V9A0,0,0,0,1,5,9Z"></path>
                                                </svg></button>
                                            <button class="ql-italic" type="button"><svg viewBox="0 0 18 18">
                                                    <line class="ql-stroke" x1="7" x2="13" y1="4" y2="4"></line>
                                                    <line class="ql-stroke" x1="5" x2="11" y1="14" y2="14"></line>
                                                    <line class="ql-stroke" x1="8" x2="10" y1="14" y2="4"></line>
                                                </svg></button>
                                            <button class="ql-underline" type="button"><svg viewBox="0 0 18 18">
                                                    <path class="ql-stroke" d="M5,3V9a4.012,4.012,0,0,0,4,4H9a4.012,4.012,0,0,0,4-4V3"></path>
                                                    <rect class="ql-fill" height="1" rx="0.5" ry="0.5" width="12" x="3" y="15"></rect>
                                                </svg></button>
                                            <button class="ql-strike" type="button"><svg viewBox="0 0 18 18">
                                                    <line class="ql-stroke ql-thin" x1="15.5" x2="2.5" y1="8.5" y2="9.5"></line>
                                                    <path class="ql-fill" d="M9.007,8C6.542,7.791,6,7.519,6,6.5,6,5.792,7.283,5,9,5c1.571,0,2.765.679,2.969,1.309a1,1,0,0,0,1.9-.617C13.356,4.106,11.354,3,9,3,6.2,3,4,4.538,4,6.5a3.2,3.2,0,0,0,.5,1.843Z"></path>
                                                    <path class="ql-fill" d="M8.984,10C11.457,10.208,12,10.479,12,11.5c0,0.708-1.283,1.5-3,1.5-1.571,0-2.765-.679-2.969-1.309a1,1,0,1,0-1.9.617C4.644,13.894,6.646,15,9,15c2.8,0,5-1.538,5-3.5a3.2,3.2,0,0,0-.5-1.843Z"></path>
                                                </svg></button></span><span class="ql-formats">
                                            <span class="ql-color ql-picker ql-color-picker"><span class="ql-picker-label" tabindex="0" role="button" aria-expanded="false" aria-controls="ql-picker-options-2"><svg viewBox="0 0 18 18">
                                                        <line class="ql-color-label ql-stroke ql-transparent" x1="3" x2="15" y1="15" y2="15"></line>
                                                        <polyline class="ql-stroke" points="5.5 11 9 3 12.5 11"></polyline>
                                                        <line class="ql-stroke" x1="11.63" x2="6.38" y1="9" y2="9"></line>
                                                    </svg></span><span class="ql-picker-options" aria-hidden="true" tabindex="-1" id="ql-picker-options-2"><span tabindex="0" role="button" class="ql-picker-item ql-primary"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#e60000" style="background-color: rgb(230, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#ff9900" style="background-color: rgb(255, 153, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#ffff00" style="background-color: rgb(255, 255, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#008a00" style="background-color: rgb(0, 138, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#0066cc" style="background-color: rgb(0, 102, 204);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#9933ff" style="background-color: rgb(153, 51, 255);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffffff" style="background-color: rgb(255, 255, 255);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#facccc" style="background-color: rgb(250, 204, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffebcc" style="background-color: rgb(255, 235, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffffcc" style="background-color: rgb(255, 255, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#cce8cc" style="background-color: rgb(204, 232, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#cce0f5" style="background-color: rgb(204, 224, 245);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ebd6ff" style="background-color: rgb(235, 214, 255);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#bbbbbb" style="background-color: rgb(187, 187, 187);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#f06666" style="background-color: rgb(240, 102, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffc266" style="background-color: rgb(255, 194, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffff66" style="background-color: rgb(255, 255, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#66b966" style="background-color: rgb(102, 185, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#66a3e0" style="background-color: rgb(102, 163, 224);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#c285ff" style="background-color: rgb(194, 133, 255);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#888888" style="background-color: rgb(136, 136, 136);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#a10000" style="background-color: rgb(161, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#b26b00" style="background-color: rgb(178, 107, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#b2b200" style="background-color: rgb(178, 178, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#006100" style="background-color: rgb(0, 97, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#0047b2" style="background-color: rgb(0, 71, 178);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#6b24b2" style="background-color: rgb(107, 36, 178);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#444444" style="background-color: rgb(68, 68, 68);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#5c0000" style="background-color: rgb(92, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#663d00" style="background-color: rgb(102, 61, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#666600" style="background-color: rgb(102, 102, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#003700" style="background-color: rgb(0, 55, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#002966" style="background-color: rgb(0, 41, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#3d1466" style="background-color: rgb(61, 20, 102);"></span></span></span><select class="ql-color" style="display: none;">
                                                <option selected="selected"></option>
                                                <option value="#e60000"></option>
                                                <option value="#ff9900"></option>
                                                <option value="#ffff00"></option>
                                                <option value="#008a00"></option>
                                                <option value="#0066cc"></option>
                                                <option value="#9933ff"></option>
                                                <option value="#ffffff"></option>
                                                <option value="#facccc"></option>
                                                <option value="#ffebcc"></option>
                                                <option value="#ffffcc"></option>
                                                <option value="#cce8cc"></option>
                                                <option value="#cce0f5"></option>
                                                <option value="#ebd6ff"></option>
                                                <option value="#bbbbbb"></option>
                                                <option value="#f06666"></option>
                                                <option value="#ffc266"></option>
                                                <option value="#ffff66"></option>
                                                <option value="#66b966"></option>
                                                <option value="#66a3e0"></option>
                                                <option value="#c285ff"></option>
                                                <option value="#888888"></option>
                                                <option value="#a10000"></option>
                                                <option value="#b26b00"></option>
                                                <option value="#b2b200"></option>
                                                <option value="#006100"></option>
                                                <option value="#0047b2"></option>
                                                <option value="#6b24b2"></option>
                                                <option value="#444444"></option>
                                                <option value="#5c0000"></option>
                                                <option value="#663d00"></option>
                                                <option value="#666600"></option>
                                                <option value="#003700"></option>
                                                <option value="#002966"></option>
                                                <option value="#3d1466"></option>
                                            </select>
                                            <span class="ql-background ql-picker ql-color-picker"><span class="ql-picker-label" tabindex="0" role="button" aria-expanded="false" aria-controls="ql-picker-options-3"><svg viewBox="0 0 18 18">
                                                        <g class="ql-fill ql-color-label">
                                                            <polygon points="6 6.868 6 6 5 6 5 7 5.942 7 6 6.868"></polygon>
                                                            <rect height="1" width="1" x="4" y="4"></rect>
                                                            <polygon points="6.817 5 6 5 6 6 6.38 6 6.817 5"></polygon>
                                                            <rect height="1" width="1" x="2" y="6"></rect>
                                                            <rect height="1" width="1" x="3" y="5"></rect>
                                                            <rect height="1" width="1" x="4" y="7"></rect>
                                                            <polygon points="4 11.439 4 11 3 11 3 12 3.755 12 4 11.439"></polygon>
                                                            <rect height="1" width="1" x="2" y="12"></rect>
                                                            <rect height="1" width="1" x="2" y="9"></rect>
                                                            <rect height="1" width="1" x="2" y="15"></rect>
                                                            <polygon points="4.63 10 4 10 4 11 4.192 11 4.63 10"></polygon>
                                                            <rect height="1" width="1" x="3" y="8"></rect>
                                                            <path d="M10.832,4.2L11,4.582V4H10.708A1.948,1.948,0,0,1,10.832,4.2Z"></path>
                                                            <path d="M7,4.582L7.168,4.2A1.929,1.929,0,0,1,7.292,4H7V4.582Z"></path>
                                                            <path d="M8,13H7.683l-0.351.8a1.933,1.933,0,0,1-.124.2H8V13Z"></path>
                                                            <rect height="1" width="1" x="12" y="2"></rect>
                                                            <rect height="1" width="1" x="11" y="3"></rect>
                                                            <path d="M9,3H8V3.282A1.985,1.985,0,0,1,9,3Z"></path>
                                                            <rect height="1" width="1" x="2" y="3"></rect>
                                                            <rect height="1" width="1" x="6" y="2"></rect>
                                                            <rect height="1" width="1" x="3" y="2"></rect>
                                                            <rect height="1" width="1" x="5" y="3"></rect>
                                                            <rect height="1" width="1" x="9" y="2"></rect>
                                                            <rect height="1" width="1" x="15" y="14"></rect>
                                                            <polygon points="13.447 10.174 13.469 10.225 13.472 10.232 13.808 11 14 11 14 10 13.37 10 13.447 10.174"></polygon>
                                                            <rect height="1" width="1" x="13" y="7"></rect>
                                                            <rect height="1" width="1" x="15" y="5"></rect>
                                                            <rect height="1" width="1" x="14" y="6"></rect>
                                                            <rect height="1" width="1" x="15" y="8"></rect>
                                                            <rect height="1" width="1" x="14" y="9"></rect>
                                                            <path d="M3.775,14H3v1H4V14.314A1.97,1.97,0,0,1,3.775,14Z"></path>
                                                            <rect height="1" width="1" x="14" y="3"></rect>
                                                            <polygon points="12 6.868 12 6 11.62 6 12 6.868"></polygon>
                                                            <rect height="1" width="1" x="15" y="2"></rect>
                                                            <rect height="1" width="1" x="12" y="5"></rect>
                                                            <rect height="1" width="1" x="13" y="4"></rect>
                                                            <polygon points="12.933 9 13 9 13 8 12.495 8 12.933 9"></polygon>
                                                            <rect height="1" width="1" x="9" y="14"></rect>
                                                            <rect height="1" width="1" x="8" y="15"></rect>
                                                            <path d="M6,14.926V15H7V14.316A1.993,1.993,0,0,1,6,14.926Z"></path>
                                                            <rect height="1" width="1" x="5" y="15"></rect>
                                                            <path d="M10.668,13.8L10.317,13H10v1h0.792A1.947,1.947,0,0,1,10.668,13.8Z"></path>
                                                            <rect height="1" width="1" x="11" y="15"></rect>
                                                            <path d="M14.332,12.2a1.99,1.99,0,0,1,.166.8H15V12H14.245Z"></path>
                                                            <rect height="1" width="1" x="14" y="15"></rect>
                                                            <rect height="1" width="1" x="15" y="11"></rect>
                                                        </g>
                                                        <polyline class="ql-stroke" points="5.5 13 9 5 12.5 13"></polyline>
                                                        <line class="ql-stroke" x1="11.63" x2="6.38" y1="11" y2="11"></line>
                                                    </svg></span><span class="ql-picker-options" aria-hidden="true" tabindex="-1" id="ql-picker-options-3"><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#000000" style="background-color: rgb(0, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#e60000" style="background-color: rgb(230, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#ff9900" style="background-color: rgb(255, 153, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#ffff00" style="background-color: rgb(255, 255, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#008a00" style="background-color: rgb(0, 138, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#0066cc" style="background-color: rgb(0, 102, 204);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#9933ff" style="background-color: rgb(153, 51, 255);"></span><span tabindex="0" role="button" class="ql-picker-item"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#facccc" style="background-color: rgb(250, 204, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffebcc" style="background-color: rgb(255, 235, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffffcc" style="background-color: rgb(255, 255, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#cce8cc" style="background-color: rgb(204, 232, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#cce0f5" style="background-color: rgb(204, 224, 245);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ebd6ff" style="background-color: rgb(235, 214, 255);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#bbbbbb" style="background-color: rgb(187, 187, 187);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#f06666" style="background-color: rgb(240, 102, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffc266" style="background-color: rgb(255, 194, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffff66" style="background-color: rgb(255, 255, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#66b966" style="background-color: rgb(102, 185, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#66a3e0" style="background-color: rgb(102, 163, 224);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#c285ff" style="background-color: rgb(194, 133, 255);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#888888" style="background-color: rgb(136, 136, 136);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#a10000" style="background-color: rgb(161, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#b26b00" style="background-color: rgb(178, 107, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#b2b200" style="background-color: rgb(178, 178, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#006100" style="background-color: rgb(0, 97, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#0047b2" style="background-color: rgb(0, 71, 178);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#6b24b2" style="background-color: rgb(107, 36, 178);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#444444" style="background-color: rgb(68, 68, 68);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#5c0000" style="background-color: rgb(92, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#663d00" style="background-color: rgb(102, 61, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#666600" style="background-color: rgb(102, 102, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#003700" style="background-color: rgb(0, 55, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#002966" style="background-color: rgb(0, 41, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#3d1466" style="background-color: rgb(61, 20, 102);"></span></span></span><select class="ql-background" style="display: none;">
                                                <option value="#000000"></option>
                                                <option value="#e60000"></option>
                                                <option value="#ff9900"></option>
                                                <option value="#ffff00"></option>
                                                <option value="#008a00"></option>
                                                <option value="#0066cc"></option>
                                                <option value="#9933ff"></option>
                                                <option selected="selected"></option>
                                                <option value="#facccc"></option>
                                                <option value="#ffebcc"></option>
                                                <option value="#ffffcc"></option>
                                                <option value="#cce8cc"></option>
                                                <option value="#cce0f5"></option>
                                                <option value="#ebd6ff"></option>
                                                <option value="#bbbbbb"></option>
                                                <option value="#f06666"></option>
                                                <option value="#ffc266"></option>
                                                <option value="#ffff66"></option>
                                                <option value="#66b966"></option>
                                                <option value="#66a3e0"></option>
                                                <option value="#c285ff"></option>
                                                <option value="#888888"></option>
                                                <option value="#a10000"></option>
                                                <option value="#b26b00"></option>
                                                <option value="#b2b200"></option>
                                                <option value="#006100"></option>
                                                <option value="#0047b2"></option>
                                                <option value="#6b24b2"></option>
                                                <option value="#444444"></option>
                                                <option value="#5c0000"></option>
                                                <option value="#663d00"></option>
                                                <option value="#666600"></option>
                                                <option value="#003700"></option>
                                                <option value="#002966"></option>
                                                <option value="#3d1466"></option>
                                            </select></span><span class="ql-formats">
                                            <button class="ql-script" value="sub" type="button"><svg viewBox="0 0 18 18">
                                                    <path class="ql-fill" d="M15.5,15H13.861a3.858,3.858,0,0,0,1.914-2.975,1.8,1.8,0,0,0-1.6-1.751A1.921,1.921,0,0,0,12.021,11.7a0.50013,0.50013,0,1,0,.957.291h0a0.914,0.914,0,0,1,1.053-.725,0.81,0.81,0,0,1,.744.762c0,1.076-1.16971,1.86982-1.93971,2.43082A1.45639,1.45639,0,0,0,12,15.5a0.5,0.5,0,0,0,.5.5h3A0.5,0.5,0,0,0,15.5,15Z"></path>
                                                    <path class="ql-fill" d="M9.65,5.241a1,1,0,0,0-1.409.108L6,7.964,3.759,5.349A1,1,0,0,0,2.192,6.59178Q2.21541,6.6213,2.241,6.649L4.684,9.5,2.241,12.35A1,1,0,0,0,3.71,13.70722q0.02557-.02768.049-0.05722L6,11.036,8.241,13.65a1,1,0,1,0,1.567-1.24277Q9.78459,12.3777,9.759,12.35L7.316,9.5,9.759,6.651A1,1,0,0,0,9.65,5.241Z"></path>
                                                </svg></button>
                                            <button class="ql-script" value="super" type="button"><svg viewBox="0 0 18 18">
                                                    <path class="ql-fill" d="M15.5,7H13.861a4.015,4.015,0,0,0,1.914-2.975,1.8,1.8,0,0,0-1.6-1.751A1.922,1.922,0,0,0,12.021,3.7a0.5,0.5,0,1,0,.957.291,0.917,0.917,0,0,1,1.053-.725,0.81,0.81,0,0,1,.744.762c0,1.077-1.164,1.925-1.934,2.486A1.423,1.423,0,0,0,12,7.5a0.5,0.5,0,0,0,.5.5h3A0.5,0.5,0,0,0,15.5,7Z"></path>
                                                    <path class="ql-fill" d="M9.651,5.241a1,1,0,0,0-1.41.108L6,7.964,3.759,5.349a1,1,0,1,0-1.519,1.3L4.683,9.5,2.241,12.35a1,1,0,1,0,1.519,1.3L6,11.036,8.241,13.65a1,1,0,0,0,1.519-1.3L7.317,9.5,9.759,6.651A1,1,0,0,0,9.651,5.241Z"></path>
                                                </svg></button></span><span class="ql-formats">
                                            <button class="ql-header" value="1" type="button"><svg viewBox="0 0 18 18">
                                                    <path class="ql-fill" d="M10,4V14a1,1,0,0,1-2,0V10H3v4a1,1,0,0,1-2,0V4A1,1,0,0,1,3,4V8H8V4a1,1,0,0,1,2,0Zm6.06787,9.209H14.98975V7.59863a.54085.54085,0,0,0-.605-.60547h-.62744a1.01119,1.01119,0,0,0-.748.29688L11.645,8.56641a.5435.5435,0,0,0-.022.8584l.28613.30762a.53861.53861,0,0,0,.84717.0332l.09912-.08789a1.2137,1.2137,0,0,0,.2417-.35254h.02246s-.01123.30859-.01123.60547V13.209H12.041a.54085.54085,0,0,0-.605.60547v.43945a.54085.54085,0,0,0,.605.60547h4.02686a.54085.54085,0,0,0,.605-.60547v-.43945A.54085.54085,0,0,0,16.06787,13.209Z"></path>
                                                </svg></button>
                                            <button class="ql-header" value="2" type="button"><svg viewBox="0 0 18 18">
                                                    <path class="ql-fill" d="M16.73975,13.81445v.43945a.54085.54085,0,0,1-.605.60547H11.855a.58392.58392,0,0,1-.64893-.60547V14.0127c0-2.90527,3.39941-3.42187,3.39941-4.55469a.77675.77675,0,0,0-.84717-.78125,1.17684,1.17684,0,0,0-.83594.38477c-.2749.26367-.561.374-.85791.13184l-.4292-.34082c-.30811-.24219-.38525-.51758-.1543-.81445a2.97155,2.97155,0,0,1,2.45361-1.17676,2.45393,2.45393,0,0,1,2.68408,2.40918c0,2.45312-3.1792,2.92676-3.27832,3.93848h2.79443A.54085.54085,0,0,1,16.73975,13.81445ZM9,3A.99974.99974,0,0,0,8,4V8H3V4A1,1,0,0,0,1,4V14a1,1,0,0,0,2,0V10H8v4a1,1,0,0,0,2,0V4A.99974.99974,0,0,0,9,3Z"></path>
                                                </svg></button>
                                            <button class="ql-blockquote" type="button"><svg viewBox="0 0 18 18">
                                                    <rect class="ql-fill ql-stroke" height="3" width="3" x="4" y="5"></rect>
                                                    <rect class="ql-fill ql-stroke" height="3" width="3" x="11" y="5"></rect>
                                                    <path class="ql-even ql-fill ql-stroke" d="M7,8c0,4.031-3,5-3,5"></path>
                                                    <path class="ql-even ql-fill ql-stroke" d="M14,8c0,4.031-3,5-3,5"></path>
                                                </svg></button>
                                            <button class="ql-code-block" type="button"><svg viewBox="0 0 18 18">
                                                    <polyline class="ql-even ql-stroke" points="5 7 3 9 5 11"></polyline>
                                                    <polyline class="ql-even ql-stroke" points="13 7 15 9 13 11"></polyline>
                                                    <line class="ql-stroke" x1="10" x2="8" y1="5" y2="13"></line>
                                                </svg></button></span><span class="ql-formats">
                                            <button class="ql-list" value="ordered" type="button"><svg viewBox="0 0 18 18">
                                                    <line class="ql-stroke" x1="7" x2="15" y1="4" y2="4"></line>
                                                    <line class="ql-stroke" x1="7" x2="15" y1="9" y2="9"></line>
                                                    <line class="ql-stroke" x1="7" x2="15" y1="14" y2="14"></line>
                                                    <line class="ql-stroke ql-thin" x1="2.5" x2="4.5" y1="5.5" y2="5.5"></line>
                                                    <path class="ql-fill" d="M3.5,6A0.5,0.5,0,0,1,3,5.5V3.085l-0.276.138A0.5,0.5,0,0,1,2.053,3c-0.124-.247-0.023-0.324.224-0.447l1-.5A0.5,0.5,0,0,1,4,2.5v3A0.5,0.5,0,0,1,3.5,6Z"></path>
                                                    <path class="ql-stroke ql-thin" d="M4.5,10.5h-2c0-.234,1.85-1.076,1.85-2.234A0.959,0.959,0,0,0,2.5,8.156"></path>
                                                    <path class="ql-stroke ql-thin" d="M2.5,14.846a0.959,0.959,0,0,0,1.85-.109A0.7,0.7,0,0,0,3.75,14a0.688,0.688,0,0,0,.6-0.736,0.959,0.959,0,0,0-1.85-.109"></path>
                                                </svg></button>
                                            <button class="ql-list" value="bullet" type="button"><svg viewBox="0 0 18 18">
                                                    <line class="ql-stroke" x1="6" x2="15" y1="4" y2="4"></line>
                                                    <line class="ql-stroke" x1="6" x2="15" y1="9" y2="9"></line>
                                                    <line class="ql-stroke" x1="6" x2="15" y1="14" y2="14"></line>
                                                    <line class="ql-stroke" x1="3" x2="3" y1="4" y2="4"></line>
                                                    <line class="ql-stroke" x1="3" x2="3" y1="9" y2="9"></line>
                                                    <line class="ql-stroke" x1="3" x2="3" y1="14" y2="14"></line>
                                                </svg></button>
                                            <button class="ql-indent" value="-1" type="button"><svg viewBox="0 0 18 18">
                                                    <line class="ql-stroke" x1="3" x2="15" y1="14" y2="14"></line>
                                                    <line class="ql-stroke" x1="3" x2="15" y1="4" y2="4"></line>
                                                    <line class="ql-stroke" x1="9" x2="15" y1="9" y2="9"></line>
                                                    <polyline class="ql-stroke" points="5 7 5 11 3 9 5 7"></polyline>
                                                </svg></button>
                                            <button class="ql-indent" value="+1" type="button"><svg viewBox="0 0 18 18">
                                                    <line class="ql-stroke" x1="3" x2="15" y1="14" y2="14"></line>
                                                    <line class="ql-stroke" x1="3" x2="15" y1="4" y2="4"></line>
                                                    <line class="ql-stroke" x1="9" x2="15" y1="9" y2="9"></line>
                                                    <polyline class="ql-fill ql-stroke" points="3 7 3 11 5 9 3 7"></polyline>
                                                </svg></button></span><span class="ql-formats">
                                            <button class="ql-direction" value="rtl" type="button"><svg viewBox="0 0 18 18">
                                                    <polygon class="ql-stroke ql-fill" points="3 11 5 9 3 7 3 11"></polygon>
                                                    <line class="ql-stroke ql-fill" x1="15" x2="11" y1="4" y2="4"></line>
                                                    <path class="ql-fill" d="M11,3a3,3,0,0,0,0,6h1V3H11Z"></path>
                                                    <rect class="ql-fill" height="11" width="1" x="11" y="4"></rect>
                                                    <rect class="ql-fill" height="11" width="1" x="13" y="4"></rect>
                                                </svg><svg viewBox="0 0 18 18">
                                                    <polygon class="ql-stroke ql-fill" points="15 12 13 10 15 8 15 12"></polygon>
                                                    <line class="ql-stroke ql-fill" x1="9" x2="5" y1="4" y2="4"></line>
                                                    <path class="ql-fill" d="M5,3A3,3,0,0,0,5,9H6V3H5Z"></path>
                                                    <rect class="ql-fill" height="11" width="1" x="5" y="4"></rect>
                                                    <rect class="ql-fill" height="11" width="1" x="7" y="4"></rect>
                                                </svg></button>
                                            <span class="ql-align ql-picker ql-icon-picker"><span class="ql-picker-label" tabindex="0" role="button" aria-expanded="false" aria-controls="ql-picker-options-4"><svg viewBox="0 0 18 18">
                                                        <line class="ql-stroke" x1="3" x2="15" y1="9" y2="9"></line>
                                                        <line class="ql-stroke" x1="3" x2="13" y1="14" y2="14"></line>
                                                        <line class="ql-stroke" x1="3" x2="9" y1="4" y2="4"></line>
                                                    </svg></span><span class="ql-picker-options" aria-hidden="true" tabindex="-1" id="ql-picker-options-4"><span tabindex="0" role="button" class="ql-picker-item"><svg viewBox="0 0 18 18">
                                                            <line class="ql-stroke" x1="3" x2="15" y1="9" y2="9"></line>
                                                            <line class="ql-stroke" x1="3" x2="13" y1="14" y2="14"></line>
                                                            <line class="ql-stroke" x1="3" x2="9" y1="4" y2="4"></line>
                                                        </svg></span><span tabindex="0" role="button" class="ql-picker-item" data-value="center"><svg viewBox="0 0 18 18">
                                                            <line class="ql-stroke" x1="15" x2="3" y1="9" y2="9"></line>
                                                            <line class="ql-stroke" x1="14" x2="4" y1="14" y2="14"></line>
                                                            <line class="ql-stroke" x1="12" x2="6" y1="4" y2="4"></line>
                                                        </svg></span><span tabindex="0" role="button" class="ql-picker-item" data-value="right"><svg viewBox="0 0 18 18">
                                                            <line class="ql-stroke" x1="15" x2="3" y1="9" y2="9"></line>
                                                            <line class="ql-stroke" x1="15" x2="5" y1="14" y2="14"></line>
                                                            <line class="ql-stroke" x1="15" x2="9" y1="4" y2="4"></line>
                                                        </svg></span><span tabindex="0" role="button" class="ql-picker-item" data-value="justify"><svg viewBox="0 0 18 18">
                                                            <line class="ql-stroke" x1="15" x2="3" y1="9" y2="9"></line>
                                                            <line class="ql-stroke" x1="15" x2="3" y1="14" y2="14"></line>
                                                            <line class="ql-stroke" x1="15" x2="3" y1="4" y2="4"></line>
                                                        </svg></span></span></span><select class="ql-align" style="display: none;">
                                                <option selected="selected"></option>
                                                <option value="center"></option>
                                                <option value="right"></option>
                                                <option value="justify"></option>
                                            </select></span><span class="ql-formats">
                                            <button class="ql-link" type="button"><svg viewBox="0 0 18 18">
                                                    <line class="ql-stroke" x1="7" x2="11" y1="7" y2="11"></line>
                                                    <path class="ql-even ql-stroke" d="M8.9,4.577a3.476,3.476,0,0,1,.36,4.679A3.476,3.476,0,0,1,4.577,8.9C3.185,7.5,2.035,6.4,4.217,4.217S7.5,3.185,8.9,4.577Z"></path>
                                                    <path class="ql-even ql-stroke" d="M13.423,9.1a3.476,3.476,0,0,0-4.679-.36,3.476,3.476,0,0,0,.36,4.679c1.392,1.392,2.5,2.542,4.679.36S14.815,10.5,13.423,9.1Z"></path>
                                                </svg></button></span><span class="ql-formats">
                                            <button class="ql-clean" type="button"><svg class="" viewBox="0 0 18 18">
                                                    <line class="ql-stroke" x1="5" x2="13" y1="3" y2="3"></line>
                                                    <line class="ql-stroke" x1="6" x2="9.35" y1="12" y2="3"></line>
                                                    <line class="ql-stroke" x1="11" x2="15" y1="11" y2="15"></line>
                                                    <line class="ql-stroke" x1="15" x2="11" y1="11" y2="15"></line>
                                                    <rect class="ql-fill" height="1" rx="0.5" ry="0.5" width="7" x="2" y="14"></rect>
                                                </svg></button></span></div>
                                    <div class="bg-white ql-container ql-snow" id="editor-container" style="min-height: 300px; max-height: 1000px;overflow-y: auto;">
                                        <div class="ql-editor" data-gramm="false" contenteditable="true" data-placeholder="Compose an epic...">
                                            <h2></h2>
                                            <p><br></p>
                                            <p></p>
                                        </div>
                                        <div class="ql-clipboard" contenteditable="true" tabindex="-1"></div>
                                        <div class="ql-tooltip ql-hidden"><a class="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></a><input type="text" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>
                                    </div>
                                </div>
                                <label class="form-label" for="bannerImg">Banner Image</label><br>
                                <button class="btn btn-outline-primary mb-4">Add Media</button><br>
                                <label class="form-label" for="bannerAltTxt">Alternative Text</label>
                                <input class="form-control mb-4" id="bannerAltTxt" type="text">
                                <!-- <div id="editorWithToolbar">
          <div id="toolbar-container" class="ql-toolbar ql-snow"><span class="ql-formats">
              <span class="ql-font ql-picker"><span class="ql-picker-label" tabindex="0" role="button" aria-expanded="false" aria-controls="ql-picker-options-0"><svg viewBox="0 0 18 18">
                    <polygon class="ql-stroke" points="7 11 9 13 11 11 7 11"></polygon>
                    <polygon class="ql-stroke" points="7 7 9 5 11 7 7 7"></polygon>
                  </svg></span><span class="ql-picker-options" aria-hidden="true" tabindex="-1" id="ql-picker-options-0"><span tabindex="0" role="button" class="ql-picker-item"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="serif"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="monospace"></span></span></span><select class="ql-font" style="display: none;">
                <option selected="selected"></option>
                <option value="serif"></option>
                <option value="monospace"></option>
              </select>
              <span class="ql-size ql-picker"><span class="ql-picker-label" tabindex="0" role="button" aria-expanded="false" aria-controls="ql-picker-options-1"><svg viewBox="0 0 18 18">
                    <polygon class="ql-stroke" points="7 11 9 13 11 11 7 11"></polygon>
                    <polygon class="ql-stroke" points="7 7 9 5 11 7 7 7"></polygon>
                  </svg></span><span class="ql-picker-options" aria-hidden="true" tabindex="-1" id="ql-picker-options-1"><span tabindex="0" role="button" class="ql-picker-item" data-value="small"></span><span tabindex="0" role="button" class="ql-picker-item"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="large"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="huge"></span></span></span><select class="ql-size" style="display: none;">
                <option value="small"></option>
                <option selected="selected"></option>
                <option value="large"></option>
                <option value="huge"></option>
              </select></span><span class="ql-formats">
              <button class="ql-bold" type="button"><svg viewBox="0 0 18 18">
                  <path class="ql-stroke" d="M5,4H9.5A2.5,2.5,0,0,1,12,6.5v0A2.5,2.5,0,0,1,9.5,9H5A0,0,0,0,1,5,9V4A0,0,0,0,1,5,4Z"></path>
                  <path class="ql-stroke" d="M5,9h5.5A2.5,2.5,0,0,1,13,11.5v0A2.5,2.5,0,0,1,10.5,14H5a0,0,0,0,1,0,0V9A0,0,0,0,1,5,9Z"></path>
                </svg></button>
              <button class="ql-italic" type="button"><svg viewBox="0 0 18 18">
                  <line class="ql-stroke" x1="7" x2="13" y1="4" y2="4"></line>
                  <line class="ql-stroke" x1="5" x2="11" y1="14" y2="14"></line>
                  <line class="ql-stroke" x1="8" x2="10" y1="14" y2="4"></line>
                </svg></button>
              <button class="ql-underline" type="button"><svg viewBox="0 0 18 18">
                  <path class="ql-stroke" d="M5,3V9a4.012,4.012,0,0,0,4,4H9a4.012,4.012,0,0,0,4-4V3"></path>
                  <rect class="ql-fill" height="1" rx="0.5" ry="0.5" width="12" x="3" y="15"></rect>
                </svg></button>
              <button class="ql-strike" type="button"><svg viewBox="0 0 18 18">
                  <line class="ql-stroke ql-thin" x1="15.5" x2="2.5" y1="8.5" y2="9.5"></line>
                  <path class="ql-fill" d="M9.007,8C6.542,7.791,6,7.519,6,6.5,6,5.792,7.283,5,9,5c1.571,0,2.765.679,2.969,1.309a1,1,0,0,0,1.9-.617C13.356,4.106,11.354,3,9,3,6.2,3,4,4.538,4,6.5a3.2,3.2,0,0,0,.5,1.843Z"></path>
                  <path class="ql-fill" d="M8.984,10C11.457,10.208,12,10.479,12,11.5c0,0.708-1.283,1.5-3,1.5-1.571,0-2.765-.679-2.969-1.309a1,1,0,1,0-1.9.617C4.644,13.894,6.646,15,9,15c2.8,0,5-1.538,5-3.5a3.2,3.2,0,0,0-.5-1.843Z"></path>
                </svg></button></span><span class="ql-formats">
              <span class="ql-color ql-picker ql-color-picker"><span class="ql-picker-label" tabindex="0" role="button" aria-expanded="false" aria-controls="ql-picker-options-2"><svg viewBox="0 0 18 18">
                    <line class="ql-color-label ql-stroke ql-transparent" x1="3" x2="15" y1="15" y2="15"></line>
                    <polyline class="ql-stroke" points="5.5 11 9 3 12.5 11"></polyline>
                    <line class="ql-stroke" x1="11.63" x2="6.38" y1="9" y2="9"></line>
                  </svg></span><span class="ql-picker-options" aria-hidden="true" tabindex="-1" id="ql-picker-options-2"><span tabindex="0" role="button" class="ql-picker-item ql-primary"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#e60000" style="background-color: rgb(230, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#ff9900" style="background-color: rgb(255, 153, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#ffff00" style="background-color: rgb(255, 255, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#008a00" style="background-color: rgb(0, 138, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#0066cc" style="background-color: rgb(0, 102, 204);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#9933ff" style="background-color: rgb(153, 51, 255);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffffff" style="background-color: rgb(255, 255, 255);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#facccc" style="background-color: rgb(250, 204, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffebcc" style="background-color: rgb(255, 235, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffffcc" style="background-color: rgb(255, 255, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#cce8cc" style="background-color: rgb(204, 232, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#cce0f5" style="background-color: rgb(204, 224, 245);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ebd6ff" style="background-color: rgb(235, 214, 255);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#bbbbbb" style="background-color: rgb(187, 187, 187);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#f06666" style="background-color: rgb(240, 102, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffc266" style="background-color: rgb(255, 194, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffff66" style="background-color: rgb(255, 255, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#66b966" style="background-color: rgb(102, 185, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#66a3e0" style="background-color: rgb(102, 163, 224);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#c285ff" style="background-color: rgb(194, 133, 255);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#888888" style="background-color: rgb(136, 136, 136);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#a10000" style="background-color: rgb(161, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#b26b00" style="background-color: rgb(178, 107, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#b2b200" style="background-color: rgb(178, 178, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#006100" style="background-color: rgb(0, 97, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#0047b2" style="background-color: rgb(0, 71, 178);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#6b24b2" style="background-color: rgb(107, 36, 178);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#444444" style="background-color: rgb(68, 68, 68);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#5c0000" style="background-color: rgb(92, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#663d00" style="background-color: rgb(102, 61, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#666600" style="background-color: rgb(102, 102, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#003700" style="background-color: rgb(0, 55, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#002966" style="background-color: rgb(0, 41, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#3d1466" style="background-color: rgb(61, 20, 102);"></span></span></span><select class="ql-color" style="display: none;">
                <option selected="selected"></option>
                <option value="#e60000"></option>
                <option value="#ff9900"></option>
                <option value="#ffff00"></option>
                <option value="#008a00"></option>
                <option value="#0066cc"></option>
                <option value="#9933ff"></option>
                <option value="#ffffff"></option>
                <option value="#facccc"></option>
                <option value="#ffebcc"></option>
                <option value="#ffffcc"></option>
                <option value="#cce8cc"></option>
                <option value="#cce0f5"></option>
                <option value="#ebd6ff"></option>
                <option value="#bbbbbb"></option>
                <option value="#f06666"></option>
                <option value="#ffc266"></option>
                <option value="#ffff66"></option>
                <option value="#66b966"></option>
                <option value="#66a3e0"></option>
                <option value="#c285ff"></option>
                <option value="#888888"></option>
                <option value="#a10000"></option>
                <option value="#b26b00"></option>
                <option value="#b2b200"></option>
                <option value="#006100"></option>
                <option value="#0047b2"></option>
                <option value="#6b24b2"></option>
                <option value="#444444"></option>
                <option value="#5c0000"></option>
                <option value="#663d00"></option>
                <option value="#666600"></option>
                <option value="#003700"></option>
                <option value="#002966"></option>
                <option value="#3d1466"></option>
              </select>
              <span class="ql-background ql-picker ql-color-picker"><span class="ql-picker-label" tabindex="0" role="button" aria-expanded="false" aria-controls="ql-picker-options-3"><svg viewBox="0 0 18 18">
                    <g class="ql-fill ql-color-label">
                      <polygon points="6 6.868 6 6 5 6 5 7 5.942 7 6 6.868"></polygon>
                      <rect height="1" width="1" x="4" y="4"></rect>
                      <polygon points="6.817 5 6 5 6 6 6.38 6 6.817 5"></polygon>
                      <rect height="1" width="1" x="2" y="6"></rect>
                      <rect height="1" width="1" x="3" y="5"></rect>
                      <rect height="1" width="1" x="4" y="7"></rect>
                      <polygon points="4 11.439 4 11 3 11 3 12 3.755 12 4 11.439"></polygon>
                      <rect height="1" width="1" x="2" y="12"></rect>
                      <rect height="1" width="1" x="2" y="9"></rect>
                      <rect height="1" width="1" x="2" y="15"></rect>
                      <polygon points="4.63 10 4 10 4 11 4.192 11 4.63 10"></polygon>
                      <rect height="1" width="1" x="3" y="8"></rect>
                      <path d="M10.832,4.2L11,4.582V4H10.708A1.948,1.948,0,0,1,10.832,4.2Z"></path>
                      <path d="M7,4.582L7.168,4.2A1.929,1.929,0,0,1,7.292,4H7V4.582Z"></path>
                      <path d="M8,13H7.683l-0.351.8a1.933,1.933,0,0,1-.124.2H8V13Z"></path>
                      <rect height="1" width="1" x="12" y="2"></rect>
                      <rect height="1" width="1" x="11" y="3"></rect>
                      <path d="M9,3H8V3.282A1.985,1.985,0,0,1,9,3Z"></path>
                      <rect height="1" width="1" x="2" y="3"></rect>
                      <rect height="1" width="1" x="6" y="2"></rect>
                      <rect height="1" width="1" x="3" y="2"></rect>
                      <rect height="1" width="1" x="5" y="3"></rect>
                      <rect height="1" width="1" x="9" y="2"></rect>
                      <rect height="1" width="1" x="15" y="14"></rect>
                      <polygon points="13.447 10.174 13.469 10.225 13.472 10.232 13.808 11 14 11 14 10 13.37 10 13.447 10.174"></polygon>
                      <rect height="1" width="1" x="13" y="7"></rect>
                      <rect height="1" width="1" x="15" y="5"></rect>
                      <rect height="1" width="1" x="14" y="6"></rect>
                      <rect height="1" width="1" x="15" y="8"></rect>
                      <rect height="1" width="1" x="14" y="9"></rect>
                      <path d="M3.775,14H3v1H4V14.314A1.97,1.97,0,0,1,3.775,14Z"></path>
                      <rect height="1" width="1" x="14" y="3"></rect>
                      <polygon points="12 6.868 12 6 11.62 6 12 6.868"></polygon>
                      <rect height="1" width="1" x="15" y="2"></rect>
                      <rect height="1" width="1" x="12" y="5"></rect>
                      <rect height="1" width="1" x="13" y="4"></rect>
                      <polygon points="12.933 9 13 9 13 8 12.495 8 12.933 9"></polygon>
                      <rect height="1" width="1" x="9" y="14"></rect>
                      <rect height="1" width="1" x="8" y="15"></rect>
                      <path d="M6,14.926V15H7V14.316A1.993,1.993,0,0,1,6,14.926Z"></path>
                      <rect height="1" width="1" x="5" y="15"></rect>
                      <path d="M10.668,13.8L10.317,13H10v1h0.792A1.947,1.947,0,0,1,10.668,13.8Z"></path>
                      <rect height="1" width="1" x="11" y="15"></rect>
                      <path d="M14.332,12.2a1.99,1.99,0,0,1,.166.8H15V12H14.245Z"></path>
                      <rect height="1" width="1" x="14" y="15"></rect>
                      <rect height="1" width="1" x="15" y="11"></rect>
                    </g>
                    <polyline class="ql-stroke" points="5.5 13 9 5 12.5 13"></polyline>
                    <line class="ql-stroke" x1="11.63" x2="6.38" y1="11" y2="11"></line>
                  </svg></span><span class="ql-picker-options" aria-hidden="true" tabindex="-1" id="ql-picker-options-3"><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#000000" style="background-color: rgb(0, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#e60000" style="background-color: rgb(230, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#ff9900" style="background-color: rgb(255, 153, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#ffff00" style="background-color: rgb(255, 255, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#008a00" style="background-color: rgb(0, 138, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#0066cc" style="background-color: rgb(0, 102, 204);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#9933ff" style="background-color: rgb(153, 51, 255);"></span><span tabindex="0" role="button" class="ql-picker-item"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#facccc" style="background-color: rgb(250, 204, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffebcc" style="background-color: rgb(255, 235, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffffcc" style="background-color: rgb(255, 255, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#cce8cc" style="background-color: rgb(204, 232, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#cce0f5" style="background-color: rgb(204, 224, 245);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ebd6ff" style="background-color: rgb(235, 214, 255);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#bbbbbb" style="background-color: rgb(187, 187, 187);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#f06666" style="background-color: rgb(240, 102, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffc266" style="background-color: rgb(255, 194, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffff66" style="background-color: rgb(255, 255, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#66b966" style="background-color: rgb(102, 185, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#66a3e0" style="background-color: rgb(102, 163, 224);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#c285ff" style="background-color: rgb(194, 133, 255);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#888888" style="background-color: rgb(136, 136, 136);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#a10000" style="background-color: rgb(161, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#b26b00" style="background-color: rgb(178, 107, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#b2b200" style="background-color: rgb(178, 178, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#006100" style="background-color: rgb(0, 97, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#0047b2" style="background-color: rgb(0, 71, 178);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#6b24b2" style="background-color: rgb(107, 36, 178);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#444444" style="background-color: rgb(68, 68, 68);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#5c0000" style="background-color: rgb(92, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#663d00" style="background-color: rgb(102, 61, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#666600" style="background-color: rgb(102, 102, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#003700" style="background-color: rgb(0, 55, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#002966" style="background-color: rgb(0, 41, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#3d1466" style="background-color: rgb(61, 20, 102);"></span></span></span><select class="ql-background" style="display: none;">
                <option value="#000000"></option>
                <option value="#e60000"></option>
                <option value="#ff9900"></option>
                <option value="#ffff00"></option>
                <option value="#008a00"></option>
                <option value="#0066cc"></option>
                <option value="#9933ff"></option>
                <option selected="selected"></option>
                <option value="#facccc"></option>
                <option value="#ffebcc"></option>
                <option value="#ffffcc"></option>
                <option value="#cce8cc"></option>
                <option value="#cce0f5"></option>
                <option value="#ebd6ff"></option>
                <option value="#bbbbbb"></option>
                <option value="#f06666"></option>
                <option value="#ffc266"></option>
                <option value="#ffff66"></option>
                <option value="#66b966"></option>
                <option value="#66a3e0"></option>
                <option value="#c285ff"></option>
                <option value="#888888"></option>
                <option value="#a10000"></option>
                <option value="#b26b00"></option>
                <option value="#b2b200"></option>
                <option value="#006100"></option>
                <option value="#0047b2"></option>
                <option value="#6b24b2"></option>
                <option value="#444444"></option>
                <option value="#5c0000"></option>
                <option value="#663d00"></option>
                <option value="#666600"></option>
                <option value="#003700"></option>
                <option value="#002966"></option>
                <option value="#3d1466"></option>
              </select></span><span class="ql-formats">
              <button class="ql-script" value="sub" type="button"><svg viewBox="0 0 18 18">
                  <path class="ql-fill" d="M15.5,15H13.861a3.858,3.858,0,0,0,1.914-2.975,1.8,1.8,0,0,0-1.6-1.751A1.921,1.921,0,0,0,12.021,11.7a0.50013,0.50013,0,1,0,.957.291h0a0.914,0.914,0,0,1,1.053-.725,0.81,0.81,0,0,1,.744.762c0,1.076-1.16971,1.86982-1.93971,2.43082A1.45639,1.45639,0,0,0,12,15.5a0.5,0.5,0,0,0,.5.5h3A0.5,0.5,0,0,0,15.5,15Z"></path>
                  <path class="ql-fill" d="M9.65,5.241a1,1,0,0,0-1.409.108L6,7.964,3.759,5.349A1,1,0,0,0,2.192,6.59178Q2.21541,6.6213,2.241,6.649L4.684,9.5,2.241,12.35A1,1,0,0,0,3.71,13.70722q0.02557-.02768.049-0.05722L6,11.036,8.241,13.65a1,1,0,1,0,1.567-1.24277Q9.78459,12.3777,9.759,12.35L7.316,9.5,9.759,6.651A1,1,0,0,0,9.65,5.241Z"></path>
                </svg></button>
              <button class="ql-script" value="super" type="button"><svg viewBox="0 0 18 18">
                  <path class="ql-fill" d="M15.5,7H13.861a4.015,4.015,0,0,0,1.914-2.975,1.8,1.8,0,0,0-1.6-1.751A1.922,1.922,0,0,0,12.021,3.7a0.5,0.5,0,1,0,.957.291,0.917,0.917,0,0,1,1.053-.725,0.81,0.81,0,0,1,.744.762c0,1.077-1.164,1.925-1.934,2.486A1.423,1.423,0,0,0,12,7.5a0.5,0.5,0,0,0,.5.5h3A0.5,0.5,0,0,0,15.5,7Z"></path>
                  <path class="ql-fill" d="M9.651,5.241a1,1,0,0,0-1.41.108L6,7.964,3.759,5.349a1,1,0,1,0-1.519,1.3L4.683,9.5,2.241,12.35a1,1,0,1,0,1.519,1.3L6,11.036,8.241,13.65a1,1,0,0,0,1.519-1.3L7.317,9.5,9.759,6.651A1,1,0,0,0,9.651,5.241Z"></path>
                </svg></button></span><span class="ql-formats">
              <button class="ql-header" value="1" type="button"><svg viewBox="0 0 18 18">
                  <path class="ql-fill" d="M10,4V14a1,1,0,0,1-2,0V10H3v4a1,1,0,0,1-2,0V4A1,1,0,0,1,3,4V8H8V4a1,1,0,0,1,2,0Zm6.06787,9.209H14.98975V7.59863a.54085.54085,0,0,0-.605-.60547h-.62744a1.01119,1.01119,0,0,0-.748.29688L11.645,8.56641a.5435.5435,0,0,0-.022.8584l.28613.30762a.53861.53861,0,0,0,.84717.0332l.09912-.08789a1.2137,1.2137,0,0,0,.2417-.35254h.02246s-.01123.30859-.01123.60547V13.209H12.041a.54085.54085,0,0,0-.605.60547v.43945a.54085.54085,0,0,0,.605.60547h4.02686a.54085.54085,0,0,0,.605-.60547v-.43945A.54085.54085,0,0,0,16.06787,13.209Z"></path>
                </svg></button>
              <button class="ql-header" value="2" type="button"><svg viewBox="0 0 18 18">
                  <path class="ql-fill" d="M16.73975,13.81445v.43945a.54085.54085,0,0,1-.605.60547H11.855a.58392.58392,0,0,1-.64893-.60547V14.0127c0-2.90527,3.39941-3.42187,3.39941-4.55469a.77675.77675,0,0,0-.84717-.78125,1.17684,1.17684,0,0,0-.83594.38477c-.2749.26367-.561.374-.85791.13184l-.4292-.34082c-.30811-.24219-.38525-.51758-.1543-.81445a2.97155,2.97155,0,0,1,2.45361-1.17676,2.45393,2.45393,0,0,1,2.68408,2.40918c0,2.45312-3.1792,2.92676-3.27832,3.93848h2.79443A.54085.54085,0,0,1,16.73975,13.81445ZM9,3A.99974.99974,0,0,0,8,4V8H3V4A1,1,0,0,0,1,4V14a1,1,0,0,0,2,0V10H8v4a1,1,0,0,0,2,0V4A.99974.99974,0,0,0,9,3Z"></path>
                </svg></button>
              <button class="ql-blockquote" type="button"><svg viewBox="0 0 18 18">
                  <rect class="ql-fill ql-stroke" height="3" width="3" x="4" y="5"></rect>
                  <rect class="ql-fill ql-stroke" height="3" width="3" x="11" y="5"></rect>
                  <path class="ql-even ql-fill ql-stroke" d="M7,8c0,4.031-3,5-3,5"></path>
                  <path class="ql-even ql-fill ql-stroke" d="M14,8c0,4.031-3,5-3,5"></path>
                </svg></button>
              <button class="ql-code-block" type="button"><svg viewBox="0 0 18 18">
                  <polyline class="ql-even ql-stroke" points="5 7 3 9 5 11"></polyline>
                  <polyline class="ql-even ql-stroke" points="13 7 15 9 13 11"></polyline>
                  <line class="ql-stroke" x1="10" x2="8" y1="5" y2="13"></line>
                </svg></button></span><span class="ql-formats">
              <button class="ql-list" value="ordered" type="button"><svg viewBox="0 0 18 18">
                  <line class="ql-stroke" x1="7" x2="15" y1="4" y2="4"></line>
                  <line class="ql-stroke" x1="7" x2="15" y1="9" y2="9"></line>
                  <line class="ql-stroke" x1="7" x2="15" y1="14" y2="14"></line>
                  <line class="ql-stroke ql-thin" x1="2.5" x2="4.5" y1="5.5" y2="5.5"></line>
                  <path class="ql-fill" d="M3.5,6A0.5,0.5,0,0,1,3,5.5V3.085l-0.276.138A0.5,0.5,0,0,1,2.053,3c-0.124-.247-0.023-0.324.224-0.447l1-.5A0.5,0.5,0,0,1,4,2.5v3A0.5,0.5,0,0,1,3.5,6Z"></path>
                  <path class="ql-stroke ql-thin" d="M4.5,10.5h-2c0-.234,1.85-1.076,1.85-2.234A0.959,0.959,0,0,0,2.5,8.156"></path>
                  <path class="ql-stroke ql-thin" d="M2.5,14.846a0.959,0.959,0,0,0,1.85-.109A0.7,0.7,0,0,0,3.75,14a0.688,0.688,0,0,0,.6-0.736,0.959,0.959,0,0,0-1.85-.109"></path>
                </svg></button>
              <button class="ql-list" value="bullet" type="button"><svg viewBox="0 0 18 18">
                  <line class="ql-stroke" x1="6" x2="15" y1="4" y2="4"></line>
                  <line class="ql-stroke" x1="6" x2="15" y1="9" y2="9"></line>
                  <line class="ql-stroke" x1="6" x2="15" y1="14" y2="14"></line>
                  <line class="ql-stroke" x1="3" x2="3" y1="4" y2="4"></line>
                  <line class="ql-stroke" x1="3" x2="3" y1="9" y2="9"></line>
                  <line class="ql-stroke" x1="3" x2="3" y1="14" y2="14"></line>
                </svg></button>
              <button class="ql-indent" value="-1" type="button"><svg viewBox="0 0 18 18">
                  <line class="ql-stroke" x1="3" x2="15" y1="14" y2="14"></line>
                  <line class="ql-stroke" x1="3" x2="15" y1="4" y2="4"></line>
                  <line class="ql-stroke" x1="9" x2="15" y1="9" y2="9"></line>
                  <polyline class="ql-stroke" points="5 7 5 11 3 9 5 7"></polyline>
                </svg></button>
              <button class="ql-indent" value="+1" type="button"><svg viewBox="0 0 18 18">
                  <line class="ql-stroke" x1="3" x2="15" y1="14" y2="14"></line>
                  <line class="ql-stroke" x1="3" x2="15" y1="4" y2="4"></line>
                  <line class="ql-stroke" x1="9" x2="15" y1="9" y2="9"></line>
                  <polyline class="ql-fill ql-stroke" points="3 7 3 11 5 9 3 7"></polyline>
                </svg></button></span><span class="ql-formats">
              <button class="ql-direction" value="rtl" type="button"><svg viewBox="0 0 18 18">
                  <polygon class="ql-stroke ql-fill" points="3 11 5 9 3 7 3 11"></polygon>
                  <line class="ql-stroke ql-fill" x1="15" x2="11" y1="4" y2="4"></line>
                  <path class="ql-fill" d="M11,3a3,3,0,0,0,0,6h1V3H11Z"></path>
                  <rect class="ql-fill" height="11" width="1" x="11" y="4"></rect>
                  <rect class="ql-fill" height="11" width="1" x="13" y="4"></rect>
                </svg><svg viewBox="0 0 18 18">
                  <polygon class="ql-stroke ql-fill" points="15 12 13 10 15 8 15 12"></polygon>
                  <line class="ql-stroke ql-fill" x1="9" x2="5" y1="4" y2="4"></line>
                  <path class="ql-fill" d="M5,3A3,3,0,0,0,5,9H6V3H5Z"></path>
                  <rect class="ql-fill" height="11" width="1" x="5" y="4"></rect>
                  <rect class="ql-fill" height="11" width="1" x="7" y="4"></rect>
                </svg></button>
              <span class="ql-align ql-picker ql-icon-picker"><span class="ql-picker-label" tabindex="0" role="button" aria-expanded="false" aria-controls="ql-picker-options-4"><svg viewBox="0 0 18 18">
                    <line class="ql-stroke" x1="3" x2="15" y1="9" y2="9"></line>
                    <line class="ql-stroke" x1="3" x2="13" y1="14" y2="14"></line>
                    <line class="ql-stroke" x1="3" x2="9" y1="4" y2="4"></line>
                  </svg></span><span class="ql-picker-options" aria-hidden="true" tabindex="-1" id="ql-picker-options-4"><span tabindex="0" role="button" class="ql-picker-item"><svg viewBox="0 0 18 18">
                      <line class="ql-stroke" x1="3" x2="15" y1="9" y2="9"></line>
                      <line class="ql-stroke" x1="3" x2="13" y1="14" y2="14"></line>
                      <line class="ql-stroke" x1="3" x2="9" y1="4" y2="4"></line>
                    </svg></span><span tabindex="0" role="button" class="ql-picker-item" data-value="center"><svg viewBox="0 0 18 18">
                      <line class="ql-stroke" x1="15" x2="3" y1="9" y2="9"></line>
                      <line class="ql-stroke" x1="14" x2="4" y1="14" y2="14"></line>
                      <line class="ql-stroke" x1="12" x2="6" y1="4" y2="4"></line>
                    </svg></span><span tabindex="0" role="button" class="ql-picker-item" data-value="right"><svg viewBox="0 0 18 18">
                      <line class="ql-stroke" x1="15" x2="3" y1="9" y2="9"></line>
                      <line class="ql-stroke" x1="15" x2="5" y1="14" y2="14"></line>
                      <line class="ql-stroke" x1="15" x2="9" y1="4" y2="4"></line>
                    </svg></span><span tabindex="0" role="button" class="ql-picker-item" data-value="justify"><svg viewBox="0 0 18 18">
                      <line class="ql-stroke" x1="15" x2="3" y1="9" y2="9"></line>
                      <line class="ql-stroke" x1="15" x2="3" y1="14" y2="14"></line>
                      <line class="ql-stroke" x1="15" x2="3" y1="4" y2="4"></line>
                    </svg></span></span></span><select class="ql-align" style="display: none;">
                <option selected="selected"></option>
                <option value="center"></option>
                <option value="right"></option>
                <option value="justify"></option>
              </select></span><span class="ql-formats">
              <button class="ql-link" type="button"><svg viewBox="0 0 18 18">
                  <line class="ql-stroke" x1="7" x2="11" y1="7" y2="11"></line>
                  <path class="ql-even ql-stroke" d="M8.9,4.577a3.476,3.476,0,0,1,.36,4.679A3.476,3.476,0,0,1,4.577,8.9C3.185,7.5,2.035,6.4,4.217,4.217S7.5,3.185,8.9,4.577Z"></path>
                  <path class="ql-even ql-stroke" d="M13.423,9.1a3.476,3.476,0,0,0-4.679-.36,3.476,3.476,0,0,0,.36,4.679c1.392,1.392,2.5,2.542,4.679.36S14.815,10.5,13.423,9.1Z"></path>
                </svg></button></span><span class="ql-formats">
              <button class="ql-clean" type="button"><svg class="" viewBox="0 0 18 18">
                  <line class="ql-stroke" x1="5" x2="13" y1="3" y2="3"></line>
                  <line class="ql-stroke" x1="6" x2="9.35" y1="12" y2="3"></line>
                  <line class="ql-stroke" x1="11" x2="15" y1="11" y2="15"></line>
                  <line class="ql-stroke" x1="15" x2="11" y1="11" y2="15"></line>
                  <rect class="ql-fill" height="1" rx="0.5" ry="0.5" width="7" x="2" y="14"></rect>
                </svg></button></span></div>
          <div class="bg-white ql-container ql-snow" id="editor-container" style="min-height: auto; max-height: 1000px;overflow-y: auto;">
            <div class="ql-editor" data-gramm="false" contenteditable="true" data-placeholder="Compose an epic...">
              <h2></h2>
              <p><br></p>
              <p></p>
            </div>
            <div class="ql-clipboard" contenteditable="true" tabindex="-1"></div>
            <div class="ql-tooltip ql-hidden"><a class="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></a><input type="text" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>
          </div>
        </div> -->
                            </div>
                        </div>
                    </div>


                    <div class="card">
                        <div class="card-header">
                            <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">Marquee</a>
                        </div>
                        <div id="collapseTwo" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                <label class="form-label" for="marquee">Marquee</label>
                                <input class="form-control mb-4" id="marquee" type="text">
                            </div>
                        </div>
                    </div>


                    <div class="card">
                        <div class="card-header">
                            <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">About us</a>
                        </div>
                        <div id="collapseThree" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                <label class="form-label" for="aboutDec">Description</label>
                                <div id="aboutDec">
                                    <div id="toolbar-container" class="ql-toolbar ql-snow"><span class="ql-formats">
                                            <span class="ql-font ql-picker"><span class="ql-picker-label" tabindex="0" role="button" aria-expanded="false" aria-controls="ql-picker-options-0"><svg viewBox="0 0 18 18">
                                                        <polygon class="ql-stroke" points="7 11 9 13 11 11 7 11"></polygon>
                                                        <polygon class="ql-stroke" points="7 7 9 5 11 7 7 7"></polygon>
                                                    </svg></span><span class="ql-picker-options" aria-hidden="true" tabindex="-1" id="ql-picker-options-0"><span tabindex="0" role="button" class="ql-picker-item"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="serif"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="monospace"></span></span></span><select class="ql-font" style="display: none;">
                                                <option selected="selected"></option>
                                                <option value="serif"></option>
                                                <option value="monospace"></option>
                                            </select>
                                            <span class="ql-size ql-picker"><span class="ql-picker-label" tabindex="0" role="button" aria-expanded="false" aria-controls="ql-picker-options-1"><svg viewBox="0 0 18 18">
                                                        <polygon class="ql-stroke" points="7 11 9 13 11 11 7 11"></polygon>
                                                        <polygon class="ql-stroke" points="7 7 9 5 11 7 7 7"></polygon>
                                                    </svg></span><span class="ql-picker-options" aria-hidden="true" tabindex="-1" id="ql-picker-options-1"><span tabindex="0" role="button" class="ql-picker-item" data-value="small"></span><span tabindex="0" role="button" class="ql-picker-item"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="large"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="huge"></span></span></span><select class="ql-size" style="display: none;">
                                                <option value="small"></option>
                                                <option selected="selected"></option>
                                                <option value="large"></option>
                                                <option value="huge"></option>
                                            </select></span><span class="ql-formats">
                                            <button class="ql-bold" type="button"><svg viewBox="0 0 18 18">
                                                    <path class="ql-stroke" d="M5,4H9.5A2.5,2.5,0,0,1,12,6.5v0A2.5,2.5,0,0,1,9.5,9H5A0,0,0,0,1,5,9V4A0,0,0,0,1,5,4Z"></path>
                                                    <path class="ql-stroke" d="M5,9h5.5A2.5,2.5,0,0,1,13,11.5v0A2.5,2.5,0,0,1,10.5,14H5a0,0,0,0,1,0,0V9A0,0,0,0,1,5,9Z"></path>
                                                </svg></button>
                                            <button class="ql-italic" type="button"><svg viewBox="0 0 18 18">
                                                    <line class="ql-stroke" x1="7" x2="13" y1="4" y2="4"></line>
                                                    <line class="ql-stroke" x1="5" x2="11" y1="14" y2="14"></line>
                                                    <line class="ql-stroke" x1="8" x2="10" y1="14" y2="4"></line>
                                                </svg></button>
                                            <button class="ql-underline" type="button"><svg viewBox="0 0 18 18">
                                                    <path class="ql-stroke" d="M5,3V9a4.012,4.012,0,0,0,4,4H9a4.012,4.012,0,0,0,4-4V3"></path>
                                                    <rect class="ql-fill" height="1" rx="0.5" ry="0.5" width="12" x="3" y="15"></rect>
                                                </svg></button>
                                            <button class="ql-strike" type="button"><svg viewBox="0 0 18 18">
                                                    <line class="ql-stroke ql-thin" x1="15.5" x2="2.5" y1="8.5" y2="9.5"></line>
                                                    <path class="ql-fill" d="M9.007,8C6.542,7.791,6,7.519,6,6.5,6,5.792,7.283,5,9,5c1.571,0,2.765.679,2.969,1.309a1,1,0,0,0,1.9-.617C13.356,4.106,11.354,3,9,3,6.2,3,4,4.538,4,6.5a3.2,3.2,0,0,0,.5,1.843Z"></path>
                                                    <path class="ql-fill" d="M8.984,10C11.457,10.208,12,10.479,12,11.5c0,0.708-1.283,1.5-3,1.5-1.571,0-2.765-.679-2.969-1.309a1,1,0,1,0-1.9.617C4.644,13.894,6.646,15,9,15c2.8,0,5-1.538,5-3.5a3.2,3.2,0,0,0-.5-1.843Z"></path>
                                                </svg></button></span><span class="ql-formats">
                                            <span class="ql-color ql-picker ql-color-picker"><span class="ql-picker-label" tabindex="0" role="button" aria-expanded="false" aria-controls="ql-picker-options-2"><svg viewBox="0 0 18 18">
                                                        <line class="ql-color-label ql-stroke ql-transparent" x1="3" x2="15" y1="15" y2="15"></line>
                                                        <polyline class="ql-stroke" points="5.5 11 9 3 12.5 11"></polyline>
                                                        <line class="ql-stroke" x1="11.63" x2="6.38" y1="9" y2="9"></line>
                                                    </svg></span><span class="ql-picker-options" aria-hidden="true" tabindex="-1" id="ql-picker-options-2"><span tabindex="0" role="button" class="ql-picker-item ql-primary"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#e60000" style="background-color: rgb(230, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#ff9900" style="background-color: rgb(255, 153, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#ffff00" style="background-color: rgb(255, 255, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#008a00" style="background-color: rgb(0, 138, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#0066cc" style="background-color: rgb(0, 102, 204);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#9933ff" style="background-color: rgb(153, 51, 255);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffffff" style="background-color: rgb(255, 255, 255);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#facccc" style="background-color: rgb(250, 204, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffebcc" style="background-color: rgb(255, 235, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffffcc" style="background-color: rgb(255, 255, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#cce8cc" style="background-color: rgb(204, 232, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#cce0f5" style="background-color: rgb(204, 224, 245);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ebd6ff" style="background-color: rgb(235, 214, 255);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#bbbbbb" style="background-color: rgb(187, 187, 187);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#f06666" style="background-color: rgb(240, 102, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffc266" style="background-color: rgb(255, 194, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffff66" style="background-color: rgb(255, 255, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#66b966" style="background-color: rgb(102, 185, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#66a3e0" style="background-color: rgb(102, 163, 224);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#c285ff" style="background-color: rgb(194, 133, 255);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#888888" style="background-color: rgb(136, 136, 136);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#a10000" style="background-color: rgb(161, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#b26b00" style="background-color: rgb(178, 107, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#b2b200" style="background-color: rgb(178, 178, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#006100" style="background-color: rgb(0, 97, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#0047b2" style="background-color: rgb(0, 71, 178);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#6b24b2" style="background-color: rgb(107, 36, 178);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#444444" style="background-color: rgb(68, 68, 68);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#5c0000" style="background-color: rgb(92, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#663d00" style="background-color: rgb(102, 61, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#666600" style="background-color: rgb(102, 102, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#003700" style="background-color: rgb(0, 55, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#002966" style="background-color: rgb(0, 41, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#3d1466" style="background-color: rgb(61, 20, 102);"></span></span></span><select class="ql-color" style="display: none;">
                                                <option selected="selected"></option>
                                                <option value="#e60000"></option>
                                                <option value="#ff9900"></option>
                                                <option value="#ffff00"></option>
                                                <option value="#008a00"></option>
                                                <option value="#0066cc"></option>
                                                <option value="#9933ff"></option>
                                                <option value="#ffffff"></option>
                                                <option value="#facccc"></option>
                                                <option value="#ffebcc"></option>
                                                <option value="#ffffcc"></option>
                                                <option value="#cce8cc"></option>
                                                <option value="#cce0f5"></option>
                                                <option value="#ebd6ff"></option>
                                                <option value="#bbbbbb"></option>
                                                <option value="#f06666"></option>
                                                <option value="#ffc266"></option>
                                                <option value="#ffff66"></option>
                                                <option value="#66b966"></option>
                                                <option value="#66a3e0"></option>
                                                <option value="#c285ff"></option>
                                                <option value="#888888"></option>
                                                <option value="#a10000"></option>
                                                <option value="#b26b00"></option>
                                                <option value="#b2b200"></option>
                                                <option value="#006100"></option>
                                                <option value="#0047b2"></option>
                                                <option value="#6b24b2"></option>
                                                <option value="#444444"></option>
                                                <option value="#5c0000"></option>
                                                <option value="#663d00"></option>
                                                <option value="#666600"></option>
                                                <option value="#003700"></option>
                                                <option value="#002966"></option>
                                                <option value="#3d1466"></option>
                                            </select>
                                            <span class="ql-background ql-picker ql-color-picker"><span class="ql-picker-label" tabindex="0" role="button" aria-expanded="false" aria-controls="ql-picker-options-3"><svg viewBox="0 0 18 18">
                                                        <g class="ql-fill ql-color-label">
                                                            <polygon points="6 6.868 6 6 5 6 5 7 5.942 7 6 6.868"></polygon>
                                                            <rect height="1" width="1" x="4" y="4"></rect>
                                                            <polygon points="6.817 5 6 5 6 6 6.38 6 6.817 5"></polygon>
                                                            <rect height="1" width="1" x="2" y="6"></rect>
                                                            <rect height="1" width="1" x="3" y="5"></rect>
                                                            <rect height="1" width="1" x="4" y="7"></rect>
                                                            <polygon points="4 11.439 4 11 3 11 3 12 3.755 12 4 11.439"></polygon>
                                                            <rect height="1" width="1" x="2" y="12"></rect>
                                                            <rect height="1" width="1" x="2" y="9"></rect>
                                                            <rect height="1" width="1" x="2" y="15"></rect>
                                                            <polygon points="4.63 10 4 10 4 11 4.192 11 4.63 10"></polygon>
                                                            <rect height="1" width="1" x="3" y="8"></rect>
                                                            <path d="M10.832,4.2L11,4.582V4H10.708A1.948,1.948,0,0,1,10.832,4.2Z"></path>
                                                            <path d="M7,4.582L7.168,4.2A1.929,1.929,0,0,1,7.292,4H7V4.582Z"></path>
                                                            <path d="M8,13H7.683l-0.351.8a1.933,1.933,0,0,1-.124.2H8V13Z"></path>
                                                            <rect height="1" width="1" x="12" y="2"></rect>
                                                            <rect height="1" width="1" x="11" y="3"></rect>
                                                            <path d="M9,3H8V3.282A1.985,1.985,0,0,1,9,3Z"></path>
                                                            <rect height="1" width="1" x="2" y="3"></rect>
                                                            <rect height="1" width="1" x="6" y="2"></rect>
                                                            <rect height="1" width="1" x="3" y="2"></rect>
                                                            <rect height="1" width="1" x="5" y="3"></rect>
                                                            <rect height="1" width="1" x="9" y="2"></rect>
                                                            <rect height="1" width="1" x="15" y="14"></rect>
                                                            <polygon points="13.447 10.174 13.469 10.225 13.472 10.232 13.808 11 14 11 14 10 13.37 10 13.447 10.174"></polygon>
                                                            <rect height="1" width="1" x="13" y="7"></rect>
                                                            <rect height="1" width="1" x="15" y="5"></rect>
                                                            <rect height="1" width="1" x="14" y="6"></rect>
                                                            <rect height="1" width="1" x="15" y="8"></rect>
                                                            <rect height="1" width="1" x="14" y="9"></rect>
                                                            <path d="M3.775,14H3v1H4V14.314A1.97,1.97,0,0,1,3.775,14Z"></path>
                                                            <rect height="1" width="1" x="14" y="3"></rect>
                                                            <polygon points="12 6.868 12 6 11.62 6 12 6.868"></polygon>
                                                            <rect height="1" width="1" x="15" y="2"></rect>
                                                            <rect height="1" width="1" x="12" y="5"></rect>
                                                            <rect height="1" width="1" x="13" y="4"></rect>
                                                            <polygon points="12.933 9 13 9 13 8 12.495 8 12.933 9"></polygon>
                                                            <rect height="1" width="1" x="9" y="14"></rect>
                                                            <rect height="1" width="1" x="8" y="15"></rect>
                                                            <path d="M6,14.926V15H7V14.316A1.993,1.993,0,0,1,6,14.926Z"></path>
                                                            <rect height="1" width="1" x="5" y="15"></rect>
                                                            <path d="M10.668,13.8L10.317,13H10v1h0.792A1.947,1.947,0,0,1,10.668,13.8Z"></path>
                                                            <rect height="1" width="1" x="11" y="15"></rect>
                                                            <path d="M14.332,12.2a1.99,1.99,0,0,1,.166.8H15V12H14.245Z"></path>
                                                            <rect height="1" width="1" x="14" y="15"></rect>
                                                            <rect height="1" width="1" x="15" y="11"></rect>
                                                        </g>
                                                        <polyline class="ql-stroke" points="5.5 13 9 5 12.5 13"></polyline>
                                                        <line class="ql-stroke" x1="11.63" x2="6.38" y1="11" y2="11"></line>
                                                    </svg></span><span class="ql-picker-options" aria-hidden="true" tabindex="-1" id="ql-picker-options-3"><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#000000" style="background-color: rgb(0, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#e60000" style="background-color: rgb(230, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#ff9900" style="background-color: rgb(255, 153, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#ffff00" style="background-color: rgb(255, 255, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#008a00" style="background-color: rgb(0, 138, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#0066cc" style="background-color: rgb(0, 102, 204);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#9933ff" style="background-color: rgb(153, 51, 255);"></span><span tabindex="0" role="button" class="ql-picker-item"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#facccc" style="background-color: rgb(250, 204, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffebcc" style="background-color: rgb(255, 235, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffffcc" style="background-color: rgb(255, 255, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#cce8cc" style="background-color: rgb(204, 232, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#cce0f5" style="background-color: rgb(204, 224, 245);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ebd6ff" style="background-color: rgb(235, 214, 255);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#bbbbbb" style="background-color: rgb(187, 187, 187);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#f06666" style="background-color: rgb(240, 102, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffc266" style="background-color: rgb(255, 194, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffff66" style="background-color: rgb(255, 255, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#66b966" style="background-color: rgb(102, 185, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#66a3e0" style="background-color: rgb(102, 163, 224);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#c285ff" style="background-color: rgb(194, 133, 255);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#888888" style="background-color: rgb(136, 136, 136);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#a10000" style="background-color: rgb(161, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#b26b00" style="background-color: rgb(178, 107, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#b2b200" style="background-color: rgb(178, 178, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#006100" style="background-color: rgb(0, 97, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#0047b2" style="background-color: rgb(0, 71, 178);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#6b24b2" style="background-color: rgb(107, 36, 178);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#444444" style="background-color: rgb(68, 68, 68);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#5c0000" style="background-color: rgb(92, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#663d00" style="background-color: rgb(102, 61, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#666600" style="background-color: rgb(102, 102, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#003700" style="background-color: rgb(0, 55, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#002966" style="background-color: rgb(0, 41, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#3d1466" style="background-color: rgb(61, 20, 102);"></span></span></span><select class="ql-background" style="display: none;">
                                                <option value="#000000"></option>
                                                <option value="#e60000"></option>
                                                <option value="#ff9900"></option>
                                                <option value="#ffff00"></option>
                                                <option value="#008a00"></option>
                                                <option value="#0066cc"></option>
                                                <option value="#9933ff"></option>
                                                <option selected="selected"></option>
                                                <option value="#facccc"></option>
                                                <option value="#ffebcc"></option>
                                                <option value="#ffffcc"></option>
                                                <option value="#cce8cc"></option>
                                                <option value="#cce0f5"></option>
                                                <option value="#ebd6ff"></option>
                                                <option value="#bbbbbb"></option>
                                                <option value="#f06666"></option>
                                                <option value="#ffc266"></option>
                                                <option value="#ffff66"></option>
                                                <option value="#66b966"></option>
                                                <option value="#66a3e0"></option>
                                                <option value="#c285ff"></option>
                                                <option value="#888888"></option>
                                                <option value="#a10000"></option>
                                                <option value="#b26b00"></option>
                                                <option value="#b2b200"></option>
                                                <option value="#006100"></option>
                                                <option value="#0047b2"></option>
                                                <option value="#6b24b2"></option>
                                                <option value="#444444"></option>
                                                <option value="#5c0000"></option>
                                                <option value="#663d00"></option>
                                                <option value="#666600"></option>
                                                <option value="#003700"></option>
                                                <option value="#002966"></option>
                                                <option value="#3d1466"></option>
                                            </select></span><span class="ql-formats">
                                            <button class="ql-script" value="sub" type="button"><svg viewBox="0 0 18 18">
                                                    <path class="ql-fill" d="M15.5,15H13.861a3.858,3.858,0,0,0,1.914-2.975,1.8,1.8,0,0,0-1.6-1.751A1.921,1.921,0,0,0,12.021,11.7a0.50013,0.50013,0,1,0,.957.291h0a0.914,0.914,0,0,1,1.053-.725,0.81,0.81,0,0,1,.744.762c0,1.076-1.16971,1.86982-1.93971,2.43082A1.45639,1.45639,0,0,0,12,15.5a0.5,0.5,0,0,0,.5.5h3A0.5,0.5,0,0,0,15.5,15Z"></path>
                                                    <path class="ql-fill" d="M9.65,5.241a1,1,0,0,0-1.409.108L6,7.964,3.759,5.349A1,1,0,0,0,2.192,6.59178Q2.21541,6.6213,2.241,6.649L4.684,9.5,2.241,12.35A1,1,0,0,0,3.71,13.70722q0.02557-.02768.049-0.05722L6,11.036,8.241,13.65a1,1,0,1,0,1.567-1.24277Q9.78459,12.3777,9.759,12.35L7.316,9.5,9.759,6.651A1,1,0,0,0,9.65,5.241Z"></path>
                                                </svg></button>
                                            <button class="ql-script" value="super" type="button"><svg viewBox="0 0 18 18">
                                                    <path class="ql-fill" d="M15.5,7H13.861a4.015,4.015,0,0,0,1.914-2.975,1.8,1.8,0,0,0-1.6-1.751A1.922,1.922,0,0,0,12.021,3.7a0.5,0.5,0,1,0,.957.291,0.917,0.917,0,0,1,1.053-.725,0.81,0.81,0,0,1,.744.762c0,1.077-1.164,1.925-1.934,2.486A1.423,1.423,0,0,0,12,7.5a0.5,0.5,0,0,0,.5.5h3A0.5,0.5,0,0,0,15.5,7Z"></path>
                                                    <path class="ql-fill" d="M9.651,5.241a1,1,0,0,0-1.41.108L6,7.964,3.759,5.349a1,1,0,1,0-1.519,1.3L4.683,9.5,2.241,12.35a1,1,0,1,0,1.519,1.3L6,11.036,8.241,13.65a1,1,0,0,0,1.519-1.3L7.317,9.5,9.759,6.651A1,1,0,0,0,9.651,5.241Z"></path>
                                                </svg></button></span><span class="ql-formats">
                                            <button class="ql-header" value="1" type="button"><svg viewBox="0 0 18 18">
                                                    <path class="ql-fill" d="M10,4V14a1,1,0,0,1-2,0V10H3v4a1,1,0,0,1-2,0V4A1,1,0,0,1,3,4V8H8V4a1,1,0,0,1,2,0Zm6.06787,9.209H14.98975V7.59863a.54085.54085,0,0,0-.605-.60547h-.62744a1.01119,1.01119,0,0,0-.748.29688L11.645,8.56641a.5435.5435,0,0,0-.022.8584l.28613.30762a.53861.53861,0,0,0,.84717.0332l.09912-.08789a1.2137,1.2137,0,0,0,.2417-.35254h.02246s-.01123.30859-.01123.60547V13.209H12.041a.54085.54085,0,0,0-.605.60547v.43945a.54085.54085,0,0,0,.605.60547h4.02686a.54085.54085,0,0,0,.605-.60547v-.43945A.54085.54085,0,0,0,16.06787,13.209Z"></path>
                                                </svg></button>
                                            <button class="ql-header" value="2" type="button"><svg viewBox="0 0 18 18">
                                                    <path class="ql-fill" d="M16.73975,13.81445v.43945a.54085.54085,0,0,1-.605.60547H11.855a.58392.58392,0,0,1-.64893-.60547V14.0127c0-2.90527,3.39941-3.42187,3.39941-4.55469a.77675.77675,0,0,0-.84717-.78125,1.17684,1.17684,0,0,0-.83594.38477c-.2749.26367-.561.374-.85791.13184l-.4292-.34082c-.30811-.24219-.38525-.51758-.1543-.81445a2.97155,2.97155,0,0,1,2.45361-1.17676,2.45393,2.45393,0,0,1,2.68408,2.40918c0,2.45312-3.1792,2.92676-3.27832,3.93848h2.79443A.54085.54085,0,0,1,16.73975,13.81445ZM9,3A.99974.99974,0,0,0,8,4V8H3V4A1,1,0,0,0,1,4V14a1,1,0,0,0,2,0V10H8v4a1,1,0,0,0,2,0V4A.99974.99974,0,0,0,9,3Z"></path>
                                                </svg></button>
                                            <button class="ql-blockquote" type="button"><svg viewBox="0 0 18 18">
                                                    <rect class="ql-fill ql-stroke" height="3" width="3" x="4" y="5"></rect>
                                                    <rect class="ql-fill ql-stroke" height="3" width="3" x="11" y="5"></rect>
                                                    <path class="ql-even ql-fill ql-stroke" d="M7,8c0,4.031-3,5-3,5"></path>
                                                    <path class="ql-even ql-fill ql-stroke" d="M14,8c0,4.031-3,5-3,5"></path>
                                                </svg></button>
                                            <button class="ql-code-block" type="button"><svg viewBox="0 0 18 18">
                                                    <polyline class="ql-even ql-stroke" points="5 7 3 9 5 11"></polyline>
                                                    <polyline class="ql-even ql-stroke" points="13 7 15 9 13 11"></polyline>
                                                    <line class="ql-stroke" x1="10" x2="8" y1="5" y2="13"></line>
                                                </svg></button></span><span class="ql-formats">
                                            <button class="ql-list" value="ordered" type="button"><svg viewBox="0 0 18 18">
                                                    <line class="ql-stroke" x1="7" x2="15" y1="4" y2="4"></line>
                                                    <line class="ql-stroke" x1="7" x2="15" y1="9" y2="9"></line>
                                                    <line class="ql-stroke" x1="7" x2="15" y1="14" y2="14"></line>
                                                    <line class="ql-stroke ql-thin" x1="2.5" x2="4.5" y1="5.5" y2="5.5"></line>
                                                    <path class="ql-fill" d="M3.5,6A0.5,0.5,0,0,1,3,5.5V3.085l-0.276.138A0.5,0.5,0,0,1,2.053,3c-0.124-.247-0.023-0.324.224-0.447l1-.5A0.5,0.5,0,0,1,4,2.5v3A0.5,0.5,0,0,1,3.5,6Z"></path>
                                                    <path class="ql-stroke ql-thin" d="M4.5,10.5h-2c0-.234,1.85-1.076,1.85-2.234A0.959,0.959,0,0,0,2.5,8.156"></path>
                                                    <path class="ql-stroke ql-thin" d="M2.5,14.846a0.959,0.959,0,0,0,1.85-.109A0.7,0.7,0,0,0,3.75,14a0.688,0.688,0,0,0,.6-0.736,0.959,0.959,0,0,0-1.85-.109"></path>
                                                </svg></button>
                                            <button class="ql-list" value="bullet" type="button"><svg viewBox="0 0 18 18">
                                                    <line class="ql-stroke" x1="6" x2="15" y1="4" y2="4"></line>
                                                    <line class="ql-stroke" x1="6" x2="15" y1="9" y2="9"></line>
                                                    <line class="ql-stroke" x1="6" x2="15" y1="14" y2="14"></line>
                                                    <line class="ql-stroke" x1="3" x2="3" y1="4" y2="4"></line>
                                                    <line class="ql-stroke" x1="3" x2="3" y1="9" y2="9"></line>
                                                    <line class="ql-stroke" x1="3" x2="3" y1="14" y2="14"></line>
                                                </svg></button>
                                            <button class="ql-indent" value="-1" type="button"><svg viewBox="0 0 18 18">
                                                    <line class="ql-stroke" x1="3" x2="15" y1="14" y2="14"></line>
                                                    <line class="ql-stroke" x1="3" x2="15" y1="4" y2="4"></line>
                                                    <line class="ql-stroke" x1="9" x2="15" y1="9" y2="9"></line>
                                                    <polyline class="ql-stroke" points="5 7 5 11 3 9 5 7"></polyline>
                                                </svg></button>
                                            <button class="ql-indent" value="+1" type="button"><svg viewBox="0 0 18 18">
                                                    <line class="ql-stroke" x1="3" x2="15" y1="14" y2="14"></line>
                                                    <line class="ql-stroke" x1="3" x2="15" y1="4" y2="4"></line>
                                                    <line class="ql-stroke" x1="9" x2="15" y1="9" y2="9"></line>
                                                    <polyline class="ql-fill ql-stroke" points="3 7 3 11 5 9 3 7"></polyline>
                                                </svg></button></span><span class="ql-formats">
                                            <button class="ql-direction" value="rtl" type="button"><svg viewBox="0 0 18 18">
                                                    <polygon class="ql-stroke ql-fill" points="3 11 5 9 3 7 3 11"></polygon>
                                                    <line class="ql-stroke ql-fill" x1="15" x2="11" y1="4" y2="4"></line>
                                                    <path class="ql-fill" d="M11,3a3,3,0,0,0,0,6h1V3H11Z"></path>
                                                    <rect class="ql-fill" height="11" width="1" x="11" y="4"></rect>
                                                    <rect class="ql-fill" height="11" width="1" x="13" y="4"></rect>
                                                </svg><svg viewBox="0 0 18 18">
                                                    <polygon class="ql-stroke ql-fill" points="15 12 13 10 15 8 15 12"></polygon>
                                                    <line class="ql-stroke ql-fill" x1="9" x2="5" y1="4" y2="4"></line>
                                                    <path class="ql-fill" d="M5,3A3,3,0,0,0,5,9H6V3H5Z"></path>
                                                    <rect class="ql-fill" height="11" width="1" x="5" y="4"></rect>
                                                    <rect class="ql-fill" height="11" width="1" x="7" y="4"></rect>
                                                </svg></button>
                                            <span class="ql-align ql-picker ql-icon-picker"><span class="ql-picker-label" tabindex="0" role="button" aria-expanded="false" aria-controls="ql-picker-options-4"><svg viewBox="0 0 18 18">
                                                        <line class="ql-stroke" x1="3" x2="15" y1="9" y2="9"></line>
                                                        <line class="ql-stroke" x1="3" x2="13" y1="14" y2="14"></line>
                                                        <line class="ql-stroke" x1="3" x2="9" y1="4" y2="4"></line>
                                                    </svg></span><span class="ql-picker-options" aria-hidden="true" tabindex="-1" id="ql-picker-options-4"><span tabindex="0" role="button" class="ql-picker-item"><svg viewBox="0 0 18 18">
                                                            <line class="ql-stroke" x1="3" x2="15" y1="9" y2="9"></line>
                                                            <line class="ql-stroke" x1="3" x2="13" y1="14" y2="14"></line>
                                                            <line class="ql-stroke" x1="3" x2="9" y1="4" y2="4"></line>
                                                        </svg></span><span tabindex="0" role="button" class="ql-picker-item" data-value="center"><svg viewBox="0 0 18 18">
                                                            <line class="ql-stroke" x1="15" x2="3" y1="9" y2="9"></line>
                                                            <line class="ql-stroke" x1="14" x2="4" y1="14" y2="14"></line>
                                                            <line class="ql-stroke" x1="12" x2="6" y1="4" y2="4"></line>
                                                        </svg></span><span tabindex="0" role="button" class="ql-picker-item" data-value="right"><svg viewBox="0 0 18 18">
                                                            <line class="ql-stroke" x1="15" x2="3" y1="9" y2="9"></line>
                                                            <line class="ql-stroke" x1="15" x2="5" y1="14" y2="14"></line>
                                                            <line class="ql-stroke" x1="15" x2="9" y1="4" y2="4"></line>
                                                        </svg></span><span tabindex="0" role="button" class="ql-picker-item" data-value="justify"><svg viewBox="0 0 18 18">
                                                            <line class="ql-stroke" x1="15" x2="3" y1="9" y2="9"></line>
                                                            <line class="ql-stroke" x1="15" x2="3" y1="14" y2="14"></line>
                                                            <line class="ql-stroke" x1="15" x2="3" y1="4" y2="4"></line>
                                                        </svg></span></span></span><select class="ql-align" style="display: none;">
                                                <option selected="selected"></option>
                                                <option value="center"></option>
                                                <option value="right"></option>
                                                <option value="justify"></option>
                                            </select></span><span class="ql-formats">
                                            <button class="ql-link" type="button"><svg viewBox="0 0 18 18">
                                                    <line class="ql-stroke" x1="7" x2="11" y1="7" y2="11"></line>
                                                    <path class="ql-even ql-stroke" d="M8.9,4.577a3.476,3.476,0,0,1,.36,4.679A3.476,3.476,0,0,1,4.577,8.9C3.185,7.5,2.035,6.4,4.217,4.217S7.5,3.185,8.9,4.577Z"></path>
                                                    <path class="ql-even ql-stroke" d="M13.423,9.1a3.476,3.476,0,0,0-4.679-.36,3.476,3.476,0,0,0,.36,4.679c1.392,1.392,2.5,2.542,4.679.36S14.815,10.5,13.423,9.1Z"></path>
                                                </svg></button></span><span class="ql-formats">
                                            <button class="ql-clean" type="button"><svg class="" viewBox="0 0 18 18">
                                                    <line class="ql-stroke" x1="5" x2="13" y1="3" y2="3"></line>
                                                    <line class="ql-stroke" x1="6" x2="9.35" y1="12" y2="3"></line>
                                                    <line class="ql-stroke" x1="11" x2="15" y1="11" y2="15"></line>
                                                    <line class="ql-stroke" x1="15" x2="11" y1="11" y2="15"></line>
                                                    <rect class="ql-fill" height="1" rx="0.5" ry="0.5" width="7" x="2" y="14"></rect>
                                                </svg></button></span></div>
                                    <div class="bg-white ql-container ql-snow" id="editor-container" style="min-height: 300px; max-height: 1000px;overflow-y: auto;">
                                        <div class="ql-editor" data-gramm="false" contenteditable="true" data-placeholder="Compose an epic...">
                                            <h2></h2>
                                            <p><br></p>
                                            <p></p>
                                        </div>
                                        <div class="ql-clipboard" contenteditable="true" tabindex="-1"></div>
                                        <div class="ql-tooltip ql-hidden"><a class="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></a><input type="text" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>
                                    </div>
                                </div>
                                <!-- <input class="form-control mb-4" id="aboutDec" type="textarea"> -->
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <a class="collapsed card-link" data-toggle="collapse" href="#collapseFour">Focus Industries</a>
                        </div>
                        <div id="collapseFour" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                <label class="form-label" for="focusTitle">Title</label>
                                <input class="form-control mb-4" id="focusTitle" type="text">
                                <label class="form-label" for="focusDesc">Description</label>
                                <div id="focusDesc">
                                    <div id="toolbar-container" class="ql-toolbar ql-snow"><span class="ql-formats">
                                            <span class="ql-font ql-picker"><span class="ql-picker-label" tabindex="0" role="button" aria-expanded="false" aria-controls="ql-picker-options-0"><svg viewBox="0 0 18 18">
                                                        <polygon class="ql-stroke" points="7 11 9 13 11 11 7 11"></polygon>
                                                        <polygon class="ql-stroke" points="7 7 9 5 11 7 7 7"></polygon>
                                                    </svg></span><span class="ql-picker-options" aria-hidden="true" tabindex="-1" id="ql-picker-options-0"><span tabindex="0" role="button" class="ql-picker-item"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="serif"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="monospace"></span></span></span><select class="ql-font" style="display: none;">
                                                <option selected="selected"></option>
                                                <option value="serif"></option>
                                                <option value="monospace"></option>
                                            </select>
                                            <span class="ql-size ql-picker"><span class="ql-picker-label" tabindex="0" role="button" aria-expanded="false" aria-controls="ql-picker-options-1"><svg viewBox="0 0 18 18">
                                                        <polygon class="ql-stroke" points="7 11 9 13 11 11 7 11"></polygon>
                                                        <polygon class="ql-stroke" points="7 7 9 5 11 7 7 7"></polygon>
                                                    </svg></span><span class="ql-picker-options" aria-hidden="true" tabindex="-1" id="ql-picker-options-1"><span tabindex="0" role="button" class="ql-picker-item" data-value="small"></span><span tabindex="0" role="button" class="ql-picker-item"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="large"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="huge"></span></span></span><select class="ql-size" style="display: none;">
                                                <option value="small"></option>
                                                <option selected="selected"></option>
                                                <option value="large"></option>
                                                <option value="huge"></option>
                                            </select></span><span class="ql-formats">
                                            <button class="ql-bold" type="button"><svg viewBox="0 0 18 18">
                                                    <path class="ql-stroke" d="M5,4H9.5A2.5,2.5,0,0,1,12,6.5v0A2.5,2.5,0,0,1,9.5,9H5A0,0,0,0,1,5,9V4A0,0,0,0,1,5,4Z"></path>
                                                    <path class="ql-stroke" d="M5,9h5.5A2.5,2.5,0,0,1,13,11.5v0A2.5,2.5,0,0,1,10.5,14H5a0,0,0,0,1,0,0V9A0,0,0,0,1,5,9Z"></path>
                                                </svg></button>
                                            <button class="ql-italic" type="button"><svg viewBox="0 0 18 18">
                                                    <line class="ql-stroke" x1="7" x2="13" y1="4" y2="4"></line>
                                                    <line class="ql-stroke" x1="5" x2="11" y1="14" y2="14"></line>
                                                    <line class="ql-stroke" x1="8" x2="10" y1="14" y2="4"></line>
                                                </svg></button>
                                            <button class="ql-underline" type="button"><svg viewBox="0 0 18 18">
                                                    <path class="ql-stroke" d="M5,3V9a4.012,4.012,0,0,0,4,4H9a4.012,4.012,0,0,0,4-4V3"></path>
                                                    <rect class="ql-fill" height="1" rx="0.5" ry="0.5" width="12" x="3" y="15"></rect>
                                                </svg></button>
                                            <button class="ql-strike" type="button"><svg viewBox="0 0 18 18">
                                                    <line class="ql-stroke ql-thin" x1="15.5" x2="2.5" y1="8.5" y2="9.5"></line>
                                                    <path class="ql-fill" d="M9.007,8C6.542,7.791,6,7.519,6,6.5,6,5.792,7.283,5,9,5c1.571,0,2.765.679,2.969,1.309a1,1,0,0,0,1.9-.617C13.356,4.106,11.354,3,9,3,6.2,3,4,4.538,4,6.5a3.2,3.2,0,0,0,.5,1.843Z"></path>
                                                    <path class="ql-fill" d="M8.984,10C11.457,10.208,12,10.479,12,11.5c0,0.708-1.283,1.5-3,1.5-1.571,0-2.765-.679-2.969-1.309a1,1,0,1,0-1.9.617C4.644,13.894,6.646,15,9,15c2.8,0,5-1.538,5-3.5a3.2,3.2,0,0,0-.5-1.843Z"></path>
                                                </svg></button></span><span class="ql-formats">
                                            <span class="ql-color ql-picker ql-color-picker"><span class="ql-picker-label" tabindex="0" role="button" aria-expanded="false" aria-controls="ql-picker-options-2"><svg viewBox="0 0 18 18">
                                                        <line class="ql-color-label ql-stroke ql-transparent" x1="3" x2="15" y1="15" y2="15"></line>
                                                        <polyline class="ql-stroke" points="5.5 11 9 3 12.5 11"></polyline>
                                                        <line class="ql-stroke" x1="11.63" x2="6.38" y1="9" y2="9"></line>
                                                    </svg></span><span class="ql-picker-options" aria-hidden="true" tabindex="-1" id="ql-picker-options-2"><span tabindex="0" role="button" class="ql-picker-item ql-primary"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#e60000" style="background-color: rgb(230, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#ff9900" style="background-color: rgb(255, 153, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#ffff00" style="background-color: rgb(255, 255, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#008a00" style="background-color: rgb(0, 138, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#0066cc" style="background-color: rgb(0, 102, 204);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#9933ff" style="background-color: rgb(153, 51, 255);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffffff" style="background-color: rgb(255, 255, 255);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#facccc" style="background-color: rgb(250, 204, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffebcc" style="background-color: rgb(255, 235, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffffcc" style="background-color: rgb(255, 255, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#cce8cc" style="background-color: rgb(204, 232, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#cce0f5" style="background-color: rgb(204, 224, 245);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ebd6ff" style="background-color: rgb(235, 214, 255);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#bbbbbb" style="background-color: rgb(187, 187, 187);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#f06666" style="background-color: rgb(240, 102, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffc266" style="background-color: rgb(255, 194, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffff66" style="background-color: rgb(255, 255, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#66b966" style="background-color: rgb(102, 185, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#66a3e0" style="background-color: rgb(102, 163, 224);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#c285ff" style="background-color: rgb(194, 133, 255);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#888888" style="background-color: rgb(136, 136, 136);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#a10000" style="background-color: rgb(161, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#b26b00" style="background-color: rgb(178, 107, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#b2b200" style="background-color: rgb(178, 178, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#006100" style="background-color: rgb(0, 97, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#0047b2" style="background-color: rgb(0, 71, 178);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#6b24b2" style="background-color: rgb(107, 36, 178);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#444444" style="background-color: rgb(68, 68, 68);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#5c0000" style="background-color: rgb(92, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#663d00" style="background-color: rgb(102, 61, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#666600" style="background-color: rgb(102, 102, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#003700" style="background-color: rgb(0, 55, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#002966" style="background-color: rgb(0, 41, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#3d1466" style="background-color: rgb(61, 20, 102);"></span></span></span><select class="ql-color" style="display: none;">
                                                <option selected="selected"></option>
                                                <option value="#e60000"></option>
                                                <option value="#ff9900"></option>
                                                <option value="#ffff00"></option>
                                                <option value="#008a00"></option>
                                                <option value="#0066cc"></option>
                                                <option value="#9933ff"></option>
                                                <option value="#ffffff"></option>
                                                <option value="#facccc"></option>
                                                <option value="#ffebcc"></option>
                                                <option value="#ffffcc"></option>
                                                <option value="#cce8cc"></option>
                                                <option value="#cce0f5"></option>
                                                <option value="#ebd6ff"></option>
                                                <option value="#bbbbbb"></option>
                                                <option value="#f06666"></option>
                                                <option value="#ffc266"></option>
                                                <option value="#ffff66"></option>
                                                <option value="#66b966"></option>
                                                <option value="#66a3e0"></option>
                                                <option value="#c285ff"></option>
                                                <option value="#888888"></option>
                                                <option value="#a10000"></option>
                                                <option value="#b26b00"></option>
                                                <option value="#b2b200"></option>
                                                <option value="#006100"></option>
                                                <option value="#0047b2"></option>
                                                <option value="#6b24b2"></option>
                                                <option value="#444444"></option>
                                                <option value="#5c0000"></option>
                                                <option value="#663d00"></option>
                                                <option value="#666600"></option>
                                                <option value="#003700"></option>
                                                <option value="#002966"></option>
                                                <option value="#3d1466"></option>
                                            </select>
                                            <span class="ql-background ql-picker ql-color-picker"><span class="ql-picker-label" tabindex="0" role="button" aria-expanded="false" aria-controls="ql-picker-options-3"><svg viewBox="0 0 18 18">
                                                        <g class="ql-fill ql-color-label">
                                                            <polygon points="6 6.868 6 6 5 6 5 7 5.942 7 6 6.868"></polygon>
                                                            <rect height="1" width="1" x="4" y="4"></rect>
                                                            <polygon points="6.817 5 6 5 6 6 6.38 6 6.817 5"></polygon>
                                                            <rect height="1" width="1" x="2" y="6"></rect>
                                                            <rect height="1" width="1" x="3" y="5"></rect>
                                                            <rect height="1" width="1" x="4" y="7"></rect>
                                                            <polygon points="4 11.439 4 11 3 11 3 12 3.755 12 4 11.439"></polygon>
                                                            <rect height="1" width="1" x="2" y="12"></rect>
                                                            <rect height="1" width="1" x="2" y="9"></rect>
                                                            <rect height="1" width="1" x="2" y="15"></rect>
                                                            <polygon points="4.63 10 4 10 4 11 4.192 11 4.63 10"></polygon>
                                                            <rect height="1" width="1" x="3" y="8"></rect>
                                                            <path d="M10.832,4.2L11,4.582V4H10.708A1.948,1.948,0,0,1,10.832,4.2Z"></path>
                                                            <path d="M7,4.582L7.168,4.2A1.929,1.929,0,0,1,7.292,4H7V4.582Z"></path>
                                                            <path d="M8,13H7.683l-0.351.8a1.933,1.933,0,0,1-.124.2H8V13Z"></path>
                                                            <rect height="1" width="1" x="12" y="2"></rect>
                                                            <rect height="1" width="1" x="11" y="3"></rect>
                                                            <path d="M9,3H8V3.282A1.985,1.985,0,0,1,9,3Z"></path>
                                                            <rect height="1" width="1" x="2" y="3"></rect>
                                                            <rect height="1" width="1" x="6" y="2"></rect>
                                                            <rect height="1" width="1" x="3" y="2"></rect>
                                                            <rect height="1" width="1" x="5" y="3"></rect>
                                                            <rect height="1" width="1" x="9" y="2"></rect>
                                                            <rect height="1" width="1" x="15" y="14"></rect>
                                                            <polygon points="13.447 10.174 13.469 10.225 13.472 10.232 13.808 11 14 11 14 10 13.37 10 13.447 10.174"></polygon>
                                                            <rect height="1" width="1" x="13" y="7"></rect>
                                                            <rect height="1" width="1" x="15" y="5"></rect>
                                                            <rect height="1" width="1" x="14" y="6"></rect>
                                                            <rect height="1" width="1" x="15" y="8"></rect>
                                                            <rect height="1" width="1" x="14" y="9"></rect>
                                                            <path d="M3.775,14H3v1H4V14.314A1.97,1.97,0,0,1,3.775,14Z"></path>
                                                            <rect height="1" width="1" x="14" y="3"></rect>
                                                            <polygon points="12 6.868 12 6 11.62 6 12 6.868"></polygon>
                                                            <rect height="1" width="1" x="15" y="2"></rect>
                                                            <rect height="1" width="1" x="12" y="5"></rect>
                                                            <rect height="1" width="1" x="13" y="4"></rect>
                                                            <polygon points="12.933 9 13 9 13 8 12.495 8 12.933 9"></polygon>
                                                            <rect height="1" width="1" x="9" y="14"></rect>
                                                            <rect height="1" width="1" x="8" y="15"></rect>
                                                            <path d="M6,14.926V15H7V14.316A1.993,1.993,0,0,1,6,14.926Z"></path>
                                                            <rect height="1" width="1" x="5" y="15"></rect>
                                                            <path d="M10.668,13.8L10.317,13H10v1h0.792A1.947,1.947,0,0,1,10.668,13.8Z"></path>
                                                            <rect height="1" width="1" x="11" y="15"></rect>
                                                            <path d="M14.332,12.2a1.99,1.99,0,0,1,.166.8H15V12H14.245Z"></path>
                                                            <rect height="1" width="1" x="14" y="15"></rect>
                                                            <rect height="1" width="1" x="15" y="11"></rect>
                                                        </g>
                                                        <polyline class="ql-stroke" points="5.5 13 9 5 12.5 13"></polyline>
                                                        <line class="ql-stroke" x1="11.63" x2="6.38" y1="11" y2="11"></line>
                                                    </svg></span><span class="ql-picker-options" aria-hidden="true" tabindex="-1" id="ql-picker-options-3"><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#000000" style="background-color: rgb(0, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#e60000" style="background-color: rgb(230, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#ff9900" style="background-color: rgb(255, 153, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#ffff00" style="background-color: rgb(255, 255, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#008a00" style="background-color: rgb(0, 138, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#0066cc" style="background-color: rgb(0, 102, 204);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#9933ff" style="background-color: rgb(153, 51, 255);"></span><span tabindex="0" role="button" class="ql-picker-item"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#facccc" style="background-color: rgb(250, 204, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffebcc" style="background-color: rgb(255, 235, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffffcc" style="background-color: rgb(255, 255, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#cce8cc" style="background-color: rgb(204, 232, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#cce0f5" style="background-color: rgb(204, 224, 245);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ebd6ff" style="background-color: rgb(235, 214, 255);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#bbbbbb" style="background-color: rgb(187, 187, 187);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#f06666" style="background-color: rgb(240, 102, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffc266" style="background-color: rgb(255, 194, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffff66" style="background-color: rgb(255, 255, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#66b966" style="background-color: rgb(102, 185, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#66a3e0" style="background-color: rgb(102, 163, 224);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#c285ff" style="background-color: rgb(194, 133, 255);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#888888" style="background-color: rgb(136, 136, 136);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#a10000" style="background-color: rgb(161, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#b26b00" style="background-color: rgb(178, 107, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#b2b200" style="background-color: rgb(178, 178, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#006100" style="background-color: rgb(0, 97, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#0047b2" style="background-color: rgb(0, 71, 178);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#6b24b2" style="background-color: rgb(107, 36, 178);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#444444" style="background-color: rgb(68, 68, 68);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#5c0000" style="background-color: rgb(92, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#663d00" style="background-color: rgb(102, 61, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#666600" style="background-color: rgb(102, 102, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#003700" style="background-color: rgb(0, 55, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#002966" style="background-color: rgb(0, 41, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#3d1466" style="background-color: rgb(61, 20, 102);"></span></span></span><select class="ql-background" style="display: none;">
                                                <option value="#000000"></option>
                                                <option value="#e60000"></option>
                                                <option value="#ff9900"></option>
                                                <option value="#ffff00"></option>
                                                <option value="#008a00"></option>
                                                <option value="#0066cc"></option>
                                                <option value="#9933ff"></option>
                                                <option selected="selected"></option>
                                                <option value="#facccc"></option>
                                                <option value="#ffebcc"></option>
                                                <option value="#ffffcc"></option>
                                                <option value="#cce8cc"></option>
                                                <option value="#cce0f5"></option>
                                                <option value="#ebd6ff"></option>
                                                <option value="#bbbbbb"></option>
                                                <option value="#f06666"></option>
                                                <option value="#ffc266"></option>
                                                <option value="#ffff66"></option>
                                                <option value="#66b966"></option>
                                                <option value="#66a3e0"></option>
                                                <option value="#c285ff"></option>
                                                <option value="#888888"></option>
                                                <option value="#a10000"></option>
                                                <option value="#b26b00"></option>
                                                <option value="#b2b200"></option>
                                                <option value="#006100"></option>
                                                <option value="#0047b2"></option>
                                                <option value="#6b24b2"></option>
                                                <option value="#444444"></option>
                                                <option value="#5c0000"></option>
                                                <option value="#663d00"></option>
                                                <option value="#666600"></option>
                                                <option value="#003700"></option>
                                                <option value="#002966"></option>
                                                <option value="#3d1466"></option>
                                            </select></span><span class="ql-formats">
                                            <button class="ql-script" value="sub" type="button"><svg viewBox="0 0 18 18">
                                                    <path class="ql-fill" d="M15.5,15H13.861a3.858,3.858,0,0,0,1.914-2.975,1.8,1.8,0,0,0-1.6-1.751A1.921,1.921,0,0,0,12.021,11.7a0.50013,0.50013,0,1,0,.957.291h0a0.914,0.914,0,0,1,1.053-.725,0.81,0.81,0,0,1,.744.762c0,1.076-1.16971,1.86982-1.93971,2.43082A1.45639,1.45639,0,0,0,12,15.5a0.5,0.5,0,0,0,.5.5h3A0.5,0.5,0,0,0,15.5,15Z"></path>
                                                    <path class="ql-fill" d="M9.65,5.241a1,1,0,0,0-1.409.108L6,7.964,3.759,5.349A1,1,0,0,0,2.192,6.59178Q2.21541,6.6213,2.241,6.649L4.684,9.5,2.241,12.35A1,1,0,0,0,3.71,13.70722q0.02557-.02768.049-0.05722L6,11.036,8.241,13.65a1,1,0,1,0,1.567-1.24277Q9.78459,12.3777,9.759,12.35L7.316,9.5,9.759,6.651A1,1,0,0,0,9.65,5.241Z"></path>
                                                </svg></button>
                                            <button class="ql-script" value="super" type="button"><svg viewBox="0 0 18 18">
                                                    <path class="ql-fill" d="M15.5,7H13.861a4.015,4.015,0,0,0,1.914-2.975,1.8,1.8,0,0,0-1.6-1.751A1.922,1.922,0,0,0,12.021,3.7a0.5,0.5,0,1,0,.957.291,0.917,0.917,0,0,1,1.053-.725,0.81,0.81,0,0,1,.744.762c0,1.077-1.164,1.925-1.934,2.486A1.423,1.423,0,0,0,12,7.5a0.5,0.5,0,0,0,.5.5h3A0.5,0.5,0,0,0,15.5,7Z"></path>
                                                    <path class="ql-fill" d="M9.651,5.241a1,1,0,0,0-1.41.108L6,7.964,3.759,5.349a1,1,0,1,0-1.519,1.3L4.683,9.5,2.241,12.35a1,1,0,1,0,1.519,1.3L6,11.036,8.241,13.65a1,1,0,0,0,1.519-1.3L7.317,9.5,9.759,6.651A1,1,0,0,0,9.651,5.241Z"></path>
                                                </svg></button></span><span class="ql-formats">
                                            <button class="ql-header" value="1" type="button"><svg viewBox="0 0 18 18">
                                                    <path class="ql-fill" d="M10,4V14a1,1,0,0,1-2,0V10H3v4a1,1,0,0,1-2,0V4A1,1,0,0,1,3,4V8H8V4a1,1,0,0,1,2,0Zm6.06787,9.209H14.98975V7.59863a.54085.54085,0,0,0-.605-.60547h-.62744a1.01119,1.01119,0,0,0-.748.29688L11.645,8.56641a.5435.5435,0,0,0-.022.8584l.28613.30762a.53861.53861,0,0,0,.84717.0332l.09912-.08789a1.2137,1.2137,0,0,0,.2417-.35254h.02246s-.01123.30859-.01123.60547V13.209H12.041a.54085.54085,0,0,0-.605.60547v.43945a.54085.54085,0,0,0,.605.60547h4.02686a.54085.54085,0,0,0,.605-.60547v-.43945A.54085.54085,0,0,0,16.06787,13.209Z"></path>
                                                </svg></button>
                                            <button class="ql-header" value="2" type="button"><svg viewBox="0 0 18 18">
                                                    <path class="ql-fill" d="M16.73975,13.81445v.43945a.54085.54085,0,0,1-.605.60547H11.855a.58392.58392,0,0,1-.64893-.60547V14.0127c0-2.90527,3.39941-3.42187,3.39941-4.55469a.77675.77675,0,0,0-.84717-.78125,1.17684,1.17684,0,0,0-.83594.38477c-.2749.26367-.561.374-.85791.13184l-.4292-.34082c-.30811-.24219-.38525-.51758-.1543-.81445a2.97155,2.97155,0,0,1,2.45361-1.17676,2.45393,2.45393,0,0,1,2.68408,2.40918c0,2.45312-3.1792,2.92676-3.27832,3.93848h2.79443A.54085.54085,0,0,1,16.73975,13.81445ZM9,3A.99974.99974,0,0,0,8,4V8H3V4A1,1,0,0,0,1,4V14a1,1,0,0,0,2,0V10H8v4a1,1,0,0,0,2,0V4A.99974.99974,0,0,0,9,3Z"></path>
                                                </svg></button>
                                            <button class="ql-blockquote" type="button"><svg viewBox="0 0 18 18">
                                                    <rect class="ql-fill ql-stroke" height="3" width="3" x="4" y="5"></rect>
                                                    <rect class="ql-fill ql-stroke" height="3" width="3" x="11" y="5"></rect>
                                                    <path class="ql-even ql-fill ql-stroke" d="M7,8c0,4.031-3,5-3,5"></path>
                                                    <path class="ql-even ql-fill ql-stroke" d="M14,8c0,4.031-3,5-3,5"></path>
                                                </svg></button>
                                            <button class="ql-code-block" type="button"><svg viewBox="0 0 18 18">
                                                    <polyline class="ql-even ql-stroke" points="5 7 3 9 5 11"></polyline>
                                                    <polyline class="ql-even ql-stroke" points="13 7 15 9 13 11"></polyline>
                                                    <line class="ql-stroke" x1="10" x2="8" y1="5" y2="13"></line>
                                                </svg></button></span><span class="ql-formats">
                                            <button class="ql-list" value="ordered" type="button"><svg viewBox="0 0 18 18">
                                                    <line class="ql-stroke" x1="7" x2="15" y1="4" y2="4"></line>
                                                    <line class="ql-stroke" x1="7" x2="15" y1="9" y2="9"></line>
                                                    <line class="ql-stroke" x1="7" x2="15" y1="14" y2="14"></line>
                                                    <line class="ql-stroke ql-thin" x1="2.5" x2="4.5" y1="5.5" y2="5.5"></line>
                                                    <path class="ql-fill" d="M3.5,6A0.5,0.5,0,0,1,3,5.5V3.085l-0.276.138A0.5,0.5,0,0,1,2.053,3c-0.124-.247-0.023-0.324.224-0.447l1-.5A0.5,0.5,0,0,1,4,2.5v3A0.5,0.5,0,0,1,3.5,6Z"></path>
                                                    <path class="ql-stroke ql-thin" d="M4.5,10.5h-2c0-.234,1.85-1.076,1.85-2.234A0.959,0.959,0,0,0,2.5,8.156"></path>
                                                    <path class="ql-stroke ql-thin" d="M2.5,14.846a0.959,0.959,0,0,0,1.85-.109A0.7,0.7,0,0,0,3.75,14a0.688,0.688,0,0,0,.6-0.736,0.959,0.959,0,0,0-1.85-.109"></path>
                                                </svg></button>
                                            <button class="ql-list" value="bullet" type="button"><svg viewBox="0 0 18 18">
                                                    <line class="ql-stroke" x1="6" x2="15" y1="4" y2="4"></line>
                                                    <line class="ql-stroke" x1="6" x2="15" y1="9" y2="9"></line>
                                                    <line class="ql-stroke" x1="6" x2="15" y1="14" y2="14"></line>
                                                    <line class="ql-stroke" x1="3" x2="3" y1="4" y2="4"></line>
                                                    <line class="ql-stroke" x1="3" x2="3" y1="9" y2="9"></line>
                                                    <line class="ql-stroke" x1="3" x2="3" y1="14" y2="14"></line>
                                                </svg></button>
                                            <button class="ql-indent" value="-1" type="button"><svg viewBox="0 0 18 18">
                                                    <line class="ql-stroke" x1="3" x2="15" y1="14" y2="14"></line>
                                                    <line class="ql-stroke" x1="3" x2="15" y1="4" y2="4"></line>
                                                    <line class="ql-stroke" x1="9" x2="15" y1="9" y2="9"></line>
                                                    <polyline class="ql-stroke" points="5 7 5 11 3 9 5 7"></polyline>
                                                </svg></button>
                                            <button class="ql-indent" value="+1" type="button"><svg viewBox="0 0 18 18">
                                                    <line class="ql-stroke" x1="3" x2="15" y1="14" y2="14"></line>
                                                    <line class="ql-stroke" x1="3" x2="15" y1="4" y2="4"></line>
                                                    <line class="ql-stroke" x1="9" x2="15" y1="9" y2="9"></line>
                                                    <polyline class="ql-fill ql-stroke" points="3 7 3 11 5 9 3 7"></polyline>
                                                </svg></button></span><span class="ql-formats">
                                            <button class="ql-direction" value="rtl" type="button"><svg viewBox="0 0 18 18">
                                                    <polygon class="ql-stroke ql-fill" points="3 11 5 9 3 7 3 11"></polygon>
                                                    <line class="ql-stroke ql-fill" x1="15" x2="11" y1="4" y2="4"></line>
                                                    <path class="ql-fill" d="M11,3a3,3,0,0,0,0,6h1V3H11Z"></path>
                                                    <rect class="ql-fill" height="11" width="1" x="11" y="4"></rect>
                                                    <rect class="ql-fill" height="11" width="1" x="13" y="4"></rect>
                                                </svg><svg viewBox="0 0 18 18">
                                                    <polygon class="ql-stroke ql-fill" points="15 12 13 10 15 8 15 12"></polygon>
                                                    <line class="ql-stroke ql-fill" x1="9" x2="5" y1="4" y2="4"></line>
                                                    <path class="ql-fill" d="M5,3A3,3,0,0,0,5,9H6V3H5Z"></path>
                                                    <rect class="ql-fill" height="11" width="1" x="5" y="4"></rect>
                                                    <rect class="ql-fill" height="11" width="1" x="7" y="4"></rect>
                                                </svg></button>
                                            <span class="ql-align ql-picker ql-icon-picker"><span class="ql-picker-label" tabindex="0" role="button" aria-expanded="false" aria-controls="ql-picker-options-4"><svg viewBox="0 0 18 18">
                                                        <line class="ql-stroke" x1="3" x2="15" y1="9" y2="9"></line>
                                                        <line class="ql-stroke" x1="3" x2="13" y1="14" y2="14"></line>
                                                        <line class="ql-stroke" x1="3" x2="9" y1="4" y2="4"></line>
                                                    </svg></span><span class="ql-picker-options" aria-hidden="true" tabindex="-1" id="ql-picker-options-4"><span tabindex="0" role="button" class="ql-picker-item"><svg viewBox="0 0 18 18">
                                                            <line class="ql-stroke" x1="3" x2="15" y1="9" y2="9"></line>
                                                            <line class="ql-stroke" x1="3" x2="13" y1="14" y2="14"></line>
                                                            <line class="ql-stroke" x1="3" x2="9" y1="4" y2="4"></line>
                                                        </svg></span><span tabindex="0" role="button" class="ql-picker-item" data-value="center"><svg viewBox="0 0 18 18">
                                                            <line class="ql-stroke" x1="15" x2="3" y1="9" y2="9"></line>
                                                            <line class="ql-stroke" x1="14" x2="4" y1="14" y2="14"></line>
                                                            <line class="ql-stroke" x1="12" x2="6" y1="4" y2="4"></line>
                                                        </svg></span><span tabindex="0" role="button" class="ql-picker-item" data-value="right"><svg viewBox="0 0 18 18">
                                                            <line class="ql-stroke" x1="15" x2="3" y1="9" y2="9"></line>
                                                            <line class="ql-stroke" x1="15" x2="5" y1="14" y2="14"></line>
                                                            <line class="ql-stroke" x1="15" x2="9" y1="4" y2="4"></line>
                                                        </svg></span><span tabindex="0" role="button" class="ql-picker-item" data-value="justify"><svg viewBox="0 0 18 18">
                                                            <line class="ql-stroke" x1="15" x2="3" y1="9" y2="9"></line>
                                                            <line class="ql-stroke" x1="15" x2="3" y1="14" y2="14"></line>
                                                            <line class="ql-stroke" x1="15" x2="3" y1="4" y2="4"></line>
                                                        </svg></span></span></span><select class="ql-align" style="display: none;">
                                                <option selected="selected"></option>
                                                <option value="center"></option>
                                                <option value="right"></option>
                                                <option value="justify"></option>
                                            </select></span><span class="ql-formats">
                                            <button class="ql-link" type="button"><svg viewBox="0 0 18 18">
                                                    <line class="ql-stroke" x1="7" x2="11" y1="7" y2="11"></line>
                                                    <path class="ql-even ql-stroke" d="M8.9,4.577a3.476,3.476,0,0,1,.36,4.679A3.476,3.476,0,0,1,4.577,8.9C3.185,7.5,2.035,6.4,4.217,4.217S7.5,3.185,8.9,4.577Z"></path>
                                                    <path class="ql-even ql-stroke" d="M13.423,9.1a3.476,3.476,0,0,0-4.679-.36,3.476,3.476,0,0,0,.36,4.679c1.392,1.392,2.5,2.542,4.679.36S14.815,10.5,13.423,9.1Z"></path>
                                                </svg></button></span><span class="ql-formats">
                                            <button class="ql-clean" type="button"><svg class="" viewBox="0 0 18 18">
                                                    <line class="ql-stroke" x1="5" x2="13" y1="3" y2="3"></line>
                                                    <line class="ql-stroke" x1="6" x2="9.35" y1="12" y2="3"></line>
                                                    <line class="ql-stroke" x1="11" x2="15" y1="11" y2="15"></line>
                                                    <line class="ql-stroke" x1="15" x2="11" y1="11" y2="15"></line>
                                                    <rect class="ql-fill" height="1" rx="0.5" ry="0.5" width="7" x="2" y="14"></rect>
                                                </svg></button></span></div>
                                    <div class="bg-white ql-container ql-snow" id="editor-container" style="min-height: 300px; max-height: 1000px;overflow-y: auto;">
                                        <div class="ql-editor" data-gramm="false" contenteditable="true" data-placeholder="Compose an epic...">
                                            <h2></h2>
                                            <p><br></p>
                                            <p></p>
                                        </div>
                                        <div class="ql-clipboard" contenteditable="true" tabindex="-1"></div>
                                        <div class="ql-tooltip ql-hidden"><a class="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></a><input type="text" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <a class="collapsed card-link" data-toggle="collapse" href="#collapseFive">Testimonials</a>
                        </div>
                        <div id="collapseFive" class="collapse" data-parent="#accordion">
                            <div class="card-body">

                                <label class="form-label" for="testimonialsDesc">Description</label>
                                <div id="testimonialsDesc">
                                    <div id="toolbar-container" class="ql-toolbar ql-snow"><span class="ql-formats">
                                            <span class="ql-font ql-picker"><span class="ql-picker-label" tabindex="0" role="button" aria-expanded="false" aria-controls="ql-picker-options-0"><svg viewBox="0 0 18 18">
                                                        <polygon class="ql-stroke" points="7 11 9 13 11 11 7 11"></polygon>
                                                        <polygon class="ql-stroke" points="7 7 9 5 11 7 7 7"></polygon>
                                                    </svg></span><span class="ql-picker-options" aria-hidden="true" tabindex="-1" id="ql-picker-options-0"><span tabindex="0" role="button" class="ql-picker-item"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="serif"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="monospace"></span></span></span><select class="ql-font" style="display: none;">
                                                <option selected="selected"></option>
                                                <option value="serif"></option>
                                                <option value="monospace"></option>
                                            </select>
                                            <span class="ql-size ql-picker"><span class="ql-picker-label" tabindex="0" role="button" aria-expanded="false" aria-controls="ql-picker-options-1"><svg viewBox="0 0 18 18">
                                                        <polygon class="ql-stroke" points="7 11 9 13 11 11 7 11"></polygon>
                                                        <polygon class="ql-stroke" points="7 7 9 5 11 7 7 7"></polygon>
                                                    </svg></span><span class="ql-picker-options" aria-hidden="true" tabindex="-1" id="ql-picker-options-1"><span tabindex="0" role="button" class="ql-picker-item" data-value="small"></span><span tabindex="0" role="button" class="ql-picker-item"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="large"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="huge"></span></span></span><select class="ql-size" style="display: none;">
                                                <option value="small"></option>
                                                <option selected="selected"></option>
                                                <option value="large"></option>
                                                <option value="huge"></option>
                                            </select></span><span class="ql-formats">
                                            <button class="ql-bold" type="button"><svg viewBox="0 0 18 18">
                                                    <path class="ql-stroke" d="M5,4H9.5A2.5,2.5,0,0,1,12,6.5v0A2.5,2.5,0,0,1,9.5,9H5A0,0,0,0,1,5,9V4A0,0,0,0,1,5,4Z"></path>
                                                    <path class="ql-stroke" d="M5,9h5.5A2.5,2.5,0,0,1,13,11.5v0A2.5,2.5,0,0,1,10.5,14H5a0,0,0,0,1,0,0V9A0,0,0,0,1,5,9Z"></path>
                                                </svg></button>
                                            <button class="ql-italic" type="button"><svg viewBox="0 0 18 18">
                                                    <line class="ql-stroke" x1="7" x2="13" y1="4" y2="4"></line>
                                                    <line class="ql-stroke" x1="5" x2="11" y1="14" y2="14"></line>
                                                    <line class="ql-stroke" x1="8" x2="10" y1="14" y2="4"></line>
                                                </svg></button>
                                            <button class="ql-underline" type="button"><svg viewBox="0 0 18 18">
                                                    <path class="ql-stroke" d="M5,3V9a4.012,4.012,0,0,0,4,4H9a4.012,4.012,0,0,0,4-4V3"></path>
                                                    <rect class="ql-fill" height="1" rx="0.5" ry="0.5" width="12" x="3" y="15"></rect>
                                                </svg></button>
                                            <button class="ql-strike" type="button"><svg viewBox="0 0 18 18">
                                                    <line class="ql-stroke ql-thin" x1="15.5" x2="2.5" y1="8.5" y2="9.5"></line>
                                                    <path class="ql-fill" d="M9.007,8C6.542,7.791,6,7.519,6,6.5,6,5.792,7.283,5,9,5c1.571,0,2.765.679,2.969,1.309a1,1,0,0,0,1.9-.617C13.356,4.106,11.354,3,9,3,6.2,3,4,4.538,4,6.5a3.2,3.2,0,0,0,.5,1.843Z"></path>
                                                    <path class="ql-fill" d="M8.984,10C11.457,10.208,12,10.479,12,11.5c0,0.708-1.283,1.5-3,1.5-1.571,0-2.765-.679-2.969-1.309a1,1,0,1,0-1.9.617C4.644,13.894,6.646,15,9,15c2.8,0,5-1.538,5-3.5a3.2,3.2,0,0,0-.5-1.843Z"></path>
                                                </svg></button></span><span class="ql-formats">
                                            <span class="ql-color ql-picker ql-color-picker"><span class="ql-picker-label" tabindex="0" role="button" aria-expanded="false" aria-controls="ql-picker-options-2"><svg viewBox="0 0 18 18">
                                                        <line class="ql-color-label ql-stroke ql-transparent" x1="3" x2="15" y1="15" y2="15"></line>
                                                        <polyline class="ql-stroke" points="5.5 11 9 3 12.5 11"></polyline>
                                                        <line class="ql-stroke" x1="11.63" x2="6.38" y1="9" y2="9"></line>
                                                    </svg></span><span class="ql-picker-options" aria-hidden="true" tabindex="-1" id="ql-picker-options-2"><span tabindex="0" role="button" class="ql-picker-item ql-primary"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#e60000" style="background-color: rgb(230, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#ff9900" style="background-color: rgb(255, 153, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#ffff00" style="background-color: rgb(255, 255, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#008a00" style="background-color: rgb(0, 138, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#0066cc" style="background-color: rgb(0, 102, 204);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#9933ff" style="background-color: rgb(153, 51, 255);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffffff" style="background-color: rgb(255, 255, 255);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#facccc" style="background-color: rgb(250, 204, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffebcc" style="background-color: rgb(255, 235, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffffcc" style="background-color: rgb(255, 255, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#cce8cc" style="background-color: rgb(204, 232, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#cce0f5" style="background-color: rgb(204, 224, 245);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ebd6ff" style="background-color: rgb(235, 214, 255);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#bbbbbb" style="background-color: rgb(187, 187, 187);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#f06666" style="background-color: rgb(240, 102, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffc266" style="background-color: rgb(255, 194, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffff66" style="background-color: rgb(255, 255, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#66b966" style="background-color: rgb(102, 185, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#66a3e0" style="background-color: rgb(102, 163, 224);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#c285ff" style="background-color: rgb(194, 133, 255);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#888888" style="background-color: rgb(136, 136, 136);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#a10000" style="background-color: rgb(161, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#b26b00" style="background-color: rgb(178, 107, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#b2b200" style="background-color: rgb(178, 178, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#006100" style="background-color: rgb(0, 97, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#0047b2" style="background-color: rgb(0, 71, 178);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#6b24b2" style="background-color: rgb(107, 36, 178);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#444444" style="background-color: rgb(68, 68, 68);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#5c0000" style="background-color: rgb(92, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#663d00" style="background-color: rgb(102, 61, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#666600" style="background-color: rgb(102, 102, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#003700" style="background-color: rgb(0, 55, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#002966" style="background-color: rgb(0, 41, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#3d1466" style="background-color: rgb(61, 20, 102);"></span></span></span><select class="ql-color" style="display: none;">
                                                <option selected="selected"></option>
                                                <option value="#e60000"></option>
                                                <option value="#ff9900"></option>
                                                <option value="#ffff00"></option>
                                                <option value="#008a00"></option>
                                                <option value="#0066cc"></option>
                                                <option value="#9933ff"></option>
                                                <option value="#ffffff"></option>
                                                <option value="#facccc"></option>
                                                <option value="#ffebcc"></option>
                                                <option value="#ffffcc"></option>
                                                <option value="#cce8cc"></option>
                                                <option value="#cce0f5"></option>
                                                <option value="#ebd6ff"></option>
                                                <option value="#bbbbbb"></option>
                                                <option value="#f06666"></option>
                                                <option value="#ffc266"></option>
                                                <option value="#ffff66"></option>
                                                <option value="#66b966"></option>
                                                <option value="#66a3e0"></option>
                                                <option value="#c285ff"></option>
                                                <option value="#888888"></option>
                                                <option value="#a10000"></option>
                                                <option value="#b26b00"></option>
                                                <option value="#b2b200"></option>
                                                <option value="#006100"></option>
                                                <option value="#0047b2"></option>
                                                <option value="#6b24b2"></option>
                                                <option value="#444444"></option>
                                                <option value="#5c0000"></option>
                                                <option value="#663d00"></option>
                                                <option value="#666600"></option>
                                                <option value="#003700"></option>
                                                <option value="#002966"></option>
                                                <option value="#3d1466"></option>
                                            </select>
                                            <span class="ql-background ql-picker ql-color-picker"><span class="ql-picker-label" tabindex="0" role="button" aria-expanded="false" aria-controls="ql-picker-options-3"><svg viewBox="0 0 18 18">
                                                        <g class="ql-fill ql-color-label">
                                                            <polygon points="6 6.868 6 6 5 6 5 7 5.942 7 6 6.868"></polygon>
                                                            <rect height="1" width="1" x="4" y="4"></rect>
                                                            <polygon points="6.817 5 6 5 6 6 6.38 6 6.817 5"></polygon>
                                                            <rect height="1" width="1" x="2" y="6"></rect>
                                                            <rect height="1" width="1" x="3" y="5"></rect>
                                                            <rect height="1" width="1" x="4" y="7"></rect>
                                                            <polygon points="4 11.439 4 11 3 11 3 12 3.755 12 4 11.439"></polygon>
                                                            <rect height="1" width="1" x="2" y="12"></rect>
                                                            <rect height="1" width="1" x="2" y="9"></rect>
                                                            <rect height="1" width="1" x="2" y="15"></rect>
                                                            <polygon points="4.63 10 4 10 4 11 4.192 11 4.63 10"></polygon>
                                                            <rect height="1" width="1" x="3" y="8"></rect>
                                                            <path d="M10.832,4.2L11,4.582V4H10.708A1.948,1.948,0,0,1,10.832,4.2Z"></path>
                                                            <path d="M7,4.582L7.168,4.2A1.929,1.929,0,0,1,7.292,4H7V4.582Z"></path>
                                                            <path d="M8,13H7.683l-0.351.8a1.933,1.933,0,0,1-.124.2H8V13Z"></path>
                                                            <rect height="1" width="1" x="12" y="2"></rect>
                                                            <rect height="1" width="1" x="11" y="3"></rect>
                                                            <path d="M9,3H8V3.282A1.985,1.985,0,0,1,9,3Z"></path>
                                                            <rect height="1" width="1" x="2" y="3"></rect>
                                                            <rect height="1" width="1" x="6" y="2"></rect>
                                                            <rect height="1" width="1" x="3" y="2"></rect>
                                                            <rect height="1" width="1" x="5" y="3"></rect>
                                                            <rect height="1" width="1" x="9" y="2"></rect>
                                                            <rect height="1" width="1" x="15" y="14"></rect>
                                                            <polygon points="13.447 10.174 13.469 10.225 13.472 10.232 13.808 11 14 11 14 10 13.37 10 13.447 10.174"></polygon>
                                                            <rect height="1" width="1" x="13" y="7"></rect>
                                                            <rect height="1" width="1" x="15" y="5"></rect>
                                                            <rect height="1" width="1" x="14" y="6"></rect>
                                                            <rect height="1" width="1" x="15" y="8"></rect>
                                                            <rect height="1" width="1" x="14" y="9"></rect>
                                                            <path d="M3.775,14H3v1H4V14.314A1.97,1.97,0,0,1,3.775,14Z"></path>
                                                            <rect height="1" width="1" x="14" y="3"></rect>
                                                            <polygon points="12 6.868 12 6 11.62 6 12 6.868"></polygon>
                                                            <rect height="1" width="1" x="15" y="2"></rect>
                                                            <rect height="1" width="1" x="12" y="5"></rect>
                                                            <rect height="1" width="1" x="13" y="4"></rect>
                                                            <polygon points="12.933 9 13 9 13 8 12.495 8 12.933 9"></polygon>
                                                            <rect height="1" width="1" x="9" y="14"></rect>
                                                            <rect height="1" width="1" x="8" y="15"></rect>
                                                            <path d="M6,14.926V15H7V14.316A1.993,1.993,0,0,1,6,14.926Z"></path>
                                                            <rect height="1" width="1" x="5" y="15"></rect>
                                                            <path d="M10.668,13.8L10.317,13H10v1h0.792A1.947,1.947,0,0,1,10.668,13.8Z"></path>
                                                            <rect height="1" width="1" x="11" y="15"></rect>
                                                            <path d="M14.332,12.2a1.99,1.99,0,0,1,.166.8H15V12H14.245Z"></path>
                                                            <rect height="1" width="1" x="14" y="15"></rect>
                                                            <rect height="1" width="1" x="15" y="11"></rect>
                                                        </g>
                                                        <polyline class="ql-stroke" points="5.5 13 9 5 12.5 13"></polyline>
                                                        <line class="ql-stroke" x1="11.63" x2="6.38" y1="11" y2="11"></line>
                                                    </svg></span><span class="ql-picker-options" aria-hidden="true" tabindex="-1" id="ql-picker-options-3"><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#000000" style="background-color: rgb(0, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#e60000" style="background-color: rgb(230, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#ff9900" style="background-color: rgb(255, 153, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#ffff00" style="background-color: rgb(255, 255, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#008a00" style="background-color: rgb(0, 138, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#0066cc" style="background-color: rgb(0, 102, 204);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#9933ff" style="background-color: rgb(153, 51, 255);"></span><span tabindex="0" role="button" class="ql-picker-item"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#facccc" style="background-color: rgb(250, 204, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffebcc" style="background-color: rgb(255, 235, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffffcc" style="background-color: rgb(255, 255, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#cce8cc" style="background-color: rgb(204, 232, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#cce0f5" style="background-color: rgb(204, 224, 245);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ebd6ff" style="background-color: rgb(235, 214, 255);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#bbbbbb" style="background-color: rgb(187, 187, 187);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#f06666" style="background-color: rgb(240, 102, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffc266" style="background-color: rgb(255, 194, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffff66" style="background-color: rgb(255, 255, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#66b966" style="background-color: rgb(102, 185, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#66a3e0" style="background-color: rgb(102, 163, 224);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#c285ff" style="background-color: rgb(194, 133, 255);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#888888" style="background-color: rgb(136, 136, 136);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#a10000" style="background-color: rgb(161, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#b26b00" style="background-color: rgb(178, 107, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#b2b200" style="background-color: rgb(178, 178, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#006100" style="background-color: rgb(0, 97, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#0047b2" style="background-color: rgb(0, 71, 178);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#6b24b2" style="background-color: rgb(107, 36, 178);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#444444" style="background-color: rgb(68, 68, 68);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#5c0000" style="background-color: rgb(92, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#663d00" style="background-color: rgb(102, 61, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#666600" style="background-color: rgb(102, 102, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#003700" style="background-color: rgb(0, 55, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#002966" style="background-color: rgb(0, 41, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#3d1466" style="background-color: rgb(61, 20, 102);"></span></span></span><select class="ql-background" style="display: none;">
                                                <option value="#000000"></option>
                                                <option value="#e60000"></option>
                                                <option value="#ff9900"></option>
                                                <option value="#ffff00"></option>
                                                <option value="#008a00"></option>
                                                <option value="#0066cc"></option>
                                                <option value="#9933ff"></option>
                                                <option selected="selected"></option>
                                                <option value="#facccc"></option>
                                                <option value="#ffebcc"></option>
                                                <option value="#ffffcc"></option>
                                                <option value="#cce8cc"></option>
                                                <option value="#cce0f5"></option>
                                                <option value="#ebd6ff"></option>
                                                <option value="#bbbbbb"></option>
                                                <option value="#f06666"></option>
                                                <option value="#ffc266"></option>
                                                <option value="#ffff66"></option>
                                                <option value="#66b966"></option>
                                                <option value="#66a3e0"></option>
                                                <option value="#c285ff"></option>
                                                <option value="#888888"></option>
                                                <option value="#a10000"></option>
                                                <option value="#b26b00"></option>
                                                <option value="#b2b200"></option>
                                                <option value="#006100"></option>
                                                <option value="#0047b2"></option>
                                                <option value="#6b24b2"></option>
                                                <option value="#444444"></option>
                                                <option value="#5c0000"></option>
                                                <option value="#663d00"></option>
                                                <option value="#666600"></option>
                                                <option value="#003700"></option>
                                                <option value="#002966"></option>
                                                <option value="#3d1466"></option>
                                            </select></span><span class="ql-formats">
                                            <button class="ql-script" value="sub" type="button"><svg viewBox="0 0 18 18">
                                                    <path class="ql-fill" d="M15.5,15H13.861a3.858,3.858,0,0,0,1.914-2.975,1.8,1.8,0,0,0-1.6-1.751A1.921,1.921,0,0,0,12.021,11.7a0.50013,0.50013,0,1,0,.957.291h0a0.914,0.914,0,0,1,1.053-.725,0.81,0.81,0,0,1,.744.762c0,1.076-1.16971,1.86982-1.93971,2.43082A1.45639,1.45639,0,0,0,12,15.5a0.5,0.5,0,0,0,.5.5h3A0.5,0.5,0,0,0,15.5,15Z"></path>
                                                    <path class="ql-fill" d="M9.65,5.241a1,1,0,0,0-1.409.108L6,7.964,3.759,5.349A1,1,0,0,0,2.192,6.59178Q2.21541,6.6213,2.241,6.649L4.684,9.5,2.241,12.35A1,1,0,0,0,3.71,13.70722q0.02557-.02768.049-0.05722L6,11.036,8.241,13.65a1,1,0,1,0,1.567-1.24277Q9.78459,12.3777,9.759,12.35L7.316,9.5,9.759,6.651A1,1,0,0,0,9.65,5.241Z"></path>
                                                </svg></button>
                                            <button class="ql-script" value="super" type="button"><svg viewBox="0 0 18 18">
                                                    <path class="ql-fill" d="M15.5,7H13.861a4.015,4.015,0,0,0,1.914-2.975,1.8,1.8,0,0,0-1.6-1.751A1.922,1.922,0,0,0,12.021,3.7a0.5,0.5,0,1,0,.957.291,0.917,0.917,0,0,1,1.053-.725,0.81,0.81,0,0,1,.744.762c0,1.077-1.164,1.925-1.934,2.486A1.423,1.423,0,0,0,12,7.5a0.5,0.5,0,0,0,.5.5h3A0.5,0.5,0,0,0,15.5,7Z"></path>
                                                    <path class="ql-fill" d="M9.651,5.241a1,1,0,0,0-1.41.108L6,7.964,3.759,5.349a1,1,0,1,0-1.519,1.3L4.683,9.5,2.241,12.35a1,1,0,1,0,1.519,1.3L6,11.036,8.241,13.65a1,1,0,0,0,1.519-1.3L7.317,9.5,9.759,6.651A1,1,0,0,0,9.651,5.241Z"></path>
                                                </svg></button></span><span class="ql-formats">
                                            <button class="ql-header" value="1" type="button"><svg viewBox="0 0 18 18">
                                                    <path class="ql-fill" d="M10,4V14a1,1,0,0,1-2,0V10H3v4a1,1,0,0,1-2,0V4A1,1,0,0,1,3,4V8H8V4a1,1,0,0,1,2,0Zm6.06787,9.209H14.98975V7.59863a.54085.54085,0,0,0-.605-.60547h-.62744a1.01119,1.01119,0,0,0-.748.29688L11.645,8.56641a.5435.5435,0,0,0-.022.8584l.28613.30762a.53861.53861,0,0,0,.84717.0332l.09912-.08789a1.2137,1.2137,0,0,0,.2417-.35254h.02246s-.01123.30859-.01123.60547V13.209H12.041a.54085.54085,0,0,0-.605.60547v.43945a.54085.54085,0,0,0,.605.60547h4.02686a.54085.54085,0,0,0,.605-.60547v-.43945A.54085.54085,0,0,0,16.06787,13.209Z"></path>
                                                </svg></button>
                                            <button class="ql-header" value="2" type="button"><svg viewBox="0 0 18 18">
                                                    <path class="ql-fill" d="M16.73975,13.81445v.43945a.54085.54085,0,0,1-.605.60547H11.855a.58392.58392,0,0,1-.64893-.60547V14.0127c0-2.90527,3.39941-3.42187,3.39941-4.55469a.77675.77675,0,0,0-.84717-.78125,1.17684,1.17684,0,0,0-.83594.38477c-.2749.26367-.561.374-.85791.13184l-.4292-.34082c-.30811-.24219-.38525-.51758-.1543-.81445a2.97155,2.97155,0,0,1,2.45361-1.17676,2.45393,2.45393,0,0,1,2.68408,2.40918c0,2.45312-3.1792,2.92676-3.27832,3.93848h2.79443A.54085.54085,0,0,1,16.73975,13.81445ZM9,3A.99974.99974,0,0,0,8,4V8H3V4A1,1,0,0,0,1,4V14a1,1,0,0,0,2,0V10H8v4a1,1,0,0,0,2,0V4A.99974.99974,0,0,0,9,3Z"></path>
                                                </svg></button>
                                            <button class="ql-blockquote" type="button"><svg viewBox="0 0 18 18">
                                                    <rect class="ql-fill ql-stroke" height="3" width="3" x="4" y="5"></rect>
                                                    <rect class="ql-fill ql-stroke" height="3" width="3" x="11" y="5"></rect>
                                                    <path class="ql-even ql-fill ql-stroke" d="M7,8c0,4.031-3,5-3,5"></path>
                                                    <path class="ql-even ql-fill ql-stroke" d="M14,8c0,4.031-3,5-3,5"></path>
                                                </svg></button>
                                            <button class="ql-code-block" type="button"><svg viewBox="0 0 18 18">
                                                    <polyline class="ql-even ql-stroke" points="5 7 3 9 5 11"></polyline>
                                                    <polyline class="ql-even ql-stroke" points="13 7 15 9 13 11"></polyline>
                                                    <line class="ql-stroke" x1="10" x2="8" y1="5" y2="13"></line>
                                                </svg></button></span><span class="ql-formats">
                                            <button class="ql-list" value="ordered" type="button"><svg viewBox="0 0 18 18">
                                                    <line class="ql-stroke" x1="7" x2="15" y1="4" y2="4"></line>
                                                    <line class="ql-stroke" x1="7" x2="15" y1="9" y2="9"></line>
                                                    <line class="ql-stroke" x1="7" x2="15" y1="14" y2="14"></line>
                                                    <line class="ql-stroke ql-thin" x1="2.5" x2="4.5" y1="5.5" y2="5.5"></line>
                                                    <path class="ql-fill" d="M3.5,6A0.5,0.5,0,0,1,3,5.5V3.085l-0.276.138A0.5,0.5,0,0,1,2.053,3c-0.124-.247-0.023-0.324.224-0.447l1-.5A0.5,0.5,0,0,1,4,2.5v3A0.5,0.5,0,0,1,3.5,6Z"></path>
                                                    <path class="ql-stroke ql-thin" d="M4.5,10.5h-2c0-.234,1.85-1.076,1.85-2.234A0.959,0.959,0,0,0,2.5,8.156"></path>
                                                    <path class="ql-stroke ql-thin" d="M2.5,14.846a0.959,0.959,0,0,0,1.85-.109A0.7,0.7,0,0,0,3.75,14a0.688,0.688,0,0,0,.6-0.736,0.959,0.959,0,0,0-1.85-.109"></path>
                                                </svg></button>
                                            <button class="ql-list" value="bullet" type="button"><svg viewBox="0 0 18 18">
                                                    <line class="ql-stroke" x1="6" x2="15" y1="4" y2="4"></line>
                                                    <line class="ql-stroke" x1="6" x2="15" y1="9" y2="9"></line>
                                                    <line class="ql-stroke" x1="6" x2="15" y1="14" y2="14"></line>
                                                    <line class="ql-stroke" x1="3" x2="3" y1="4" y2="4"></line>
                                                    <line class="ql-stroke" x1="3" x2="3" y1="9" y2="9"></line>
                                                    <line class="ql-stroke" x1="3" x2="3" y1="14" y2="14"></line>
                                                </svg></button>
                                            <button class="ql-indent" value="-1" type="button"><svg viewBox="0 0 18 18">
                                                    <line class="ql-stroke" x1="3" x2="15" y1="14" y2="14"></line>
                                                    <line class="ql-stroke" x1="3" x2="15" y1="4" y2="4"></line>
                                                    <line class="ql-stroke" x1="9" x2="15" y1="9" y2="9"></line>
                                                    <polyline class="ql-stroke" points="5 7 5 11 3 9 5 7"></polyline>
                                                </svg></button>
                                            <button class="ql-indent" value="+1" type="button"><svg viewBox="0 0 18 18">
                                                    <line class="ql-stroke" x1="3" x2="15" y1="14" y2="14"></line>
                                                    <line class="ql-stroke" x1="3" x2="15" y1="4" y2="4"></line>
                                                    <line class="ql-stroke" x1="9" x2="15" y1="9" y2="9"></line>
                                                    <polyline class="ql-fill ql-stroke" points="3 7 3 11 5 9 3 7"></polyline>
                                                </svg></button></span><span class="ql-formats">
                                            <button class="ql-direction" value="rtl" type="button"><svg viewBox="0 0 18 18">
                                                    <polygon class="ql-stroke ql-fill" points="3 11 5 9 3 7 3 11"></polygon>
                                                    <line class="ql-stroke ql-fill" x1="15" x2="11" y1="4" y2="4"></line>
                                                    <path class="ql-fill" d="M11,3a3,3,0,0,0,0,6h1V3H11Z"></path>
                                                    <rect class="ql-fill" height="11" width="1" x="11" y="4"></rect>
                                                    <rect class="ql-fill" height="11" width="1" x="13" y="4"></rect>
                                                </svg><svg viewBox="0 0 18 18">
                                                    <polygon class="ql-stroke ql-fill" points="15 12 13 10 15 8 15 12"></polygon>
                                                    <line class="ql-stroke ql-fill" x1="9" x2="5" y1="4" y2="4"></line>
                                                    <path class="ql-fill" d="M5,3A3,3,0,0,0,5,9H6V3H5Z"></path>
                                                    <rect class="ql-fill" height="11" width="1" x="5" y="4"></rect>
                                                    <rect class="ql-fill" height="11" width="1" x="7" y="4"></rect>
                                                </svg></button>
                                            <span class="ql-align ql-picker ql-icon-picker"><span class="ql-picker-label" tabindex="0" role="button" aria-expanded="false" aria-controls="ql-picker-options-4"><svg viewBox="0 0 18 18">
                                                        <line class="ql-stroke" x1="3" x2="15" y1="9" y2="9"></line>
                                                        <line class="ql-stroke" x1="3" x2="13" y1="14" y2="14"></line>
                                                        <line class="ql-stroke" x1="3" x2="9" y1="4" y2="4"></line>
                                                    </svg></span><span class="ql-picker-options" aria-hidden="true" tabindex="-1" id="ql-picker-options-4"><span tabindex="0" role="button" class="ql-picker-item"><svg viewBox="0 0 18 18">
                                                            <line class="ql-stroke" x1="3" x2="15" y1="9" y2="9"></line>
                                                            <line class="ql-stroke" x1="3" x2="13" y1="14" y2="14"></line>
                                                            <line class="ql-stroke" x1="3" x2="9" y1="4" y2="4"></line>
                                                        </svg></span><span tabindex="0" role="button" class="ql-picker-item" data-value="center"><svg viewBox="0 0 18 18">
                                                            <line class="ql-stroke" x1="15" x2="3" y1="9" y2="9"></line>
                                                            <line class="ql-stroke" x1="14" x2="4" y1="14" y2="14"></line>
                                                            <line class="ql-stroke" x1="12" x2="6" y1="4" y2="4"></line>
                                                        </svg></span><span tabindex="0" role="button" class="ql-picker-item" data-value="right"><svg viewBox="0 0 18 18">
                                                            <line class="ql-stroke" x1="15" x2="3" y1="9" y2="9"></line>
                                                            <line class="ql-stroke" x1="15" x2="5" y1="14" y2="14"></line>
                                                            <line class="ql-stroke" x1="15" x2="9" y1="4" y2="4"></line>
                                                        </svg></span><span tabindex="0" role="button" class="ql-picker-item" data-value="justify"><svg viewBox="0 0 18 18">
                                                            <line class="ql-stroke" x1="15" x2="3" y1="9" y2="9"></line>
                                                            <line class="ql-stroke" x1="15" x2="3" y1="14" y2="14"></line>
                                                            <line class="ql-stroke" x1="15" x2="3" y1="4" y2="4"></line>
                                                        </svg></span></span></span><select class="ql-align" style="display: none;">
                                                <option selected="selected"></option>
                                                <option value="center"></option>
                                                <option value="right"></option>
                                                <option value="justify"></option>
                                            </select></span><span class="ql-formats">
                                            <button class="ql-link" type="button"><svg viewBox="0 0 18 18">
                                                    <line class="ql-stroke" x1="7" x2="11" y1="7" y2="11"></line>
                                                    <path class="ql-even ql-stroke" d="M8.9,4.577a3.476,3.476,0,0,1,.36,4.679A3.476,3.476,0,0,1,4.577,8.9C3.185,7.5,2.035,6.4,4.217,4.217S7.5,3.185,8.9,4.577Z"></path>
                                                    <path class="ql-even ql-stroke" d="M13.423,9.1a3.476,3.476,0,0,0-4.679-.36,3.476,3.476,0,0,0,.36,4.679c1.392,1.392,2.5,2.542,4.679.36S14.815,10.5,13.423,9.1Z"></path>
                                                </svg></button></span><span class="ql-formats">
                                            <button class="ql-clean" type="button"><svg class="" viewBox="0 0 18 18">
                                                    <line class="ql-stroke" x1="5" x2="13" y1="3" y2="3"></line>
                                                    <line class="ql-stroke" x1="6" x2="9.35" y1="12" y2="3"></line>
                                                    <line class="ql-stroke" x1="11" x2="15" y1="11" y2="15"></line>
                                                    <line class="ql-stroke" x1="15" x2="11" y1="11" y2="15"></line>
                                                    <rect class="ql-fill" height="1" rx="0.5" ry="0.5" width="7" x="2" y="14"></rect>
                                                </svg></button></span></div>
                                    <div class="bg-white ql-container ql-snow" id="editor-container" style="min-height: 300px; max-height: 1000px;overflow-y: auto;">
                                        <div class="ql-editor" data-gramm="false" contenteditable="true" data-placeholder="Compose an epic...">
                                            <h2></h2>
                                            <p><br></p>
                                            <p></p>
                                        </div>
                                        <div class="ql-clipboard" contenteditable="true" tabindex="-1"></div>
                                        <div class="ql-tooltip ql-hidden"><a class="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></a><input type="text" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>
                                    </div>
                                </div>
                                <label class="form-label" for="">By</label>
                                <input class="form-control mb-4" id="total_chq" type="text">
                                <!-- <button class="btn btn-outline-primary mb-4">Add</button> -->

                                <button class="btn btn-outline-primary mb-4 add">Add</button>
                                <button class="btn btn-outline-primary mb-4 remove">remove</button>
                                <div id="new_chq"></div>



                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <a class="collapsed card-link" data-toggle="collapse" href="#collapseSix">Processes Followed By Acclivis</a>
                        </div>
                        <div id="collapseSix" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                <label class="form-label" for="postTitle">Title</label>
                                <input class="form-control mb-4" id="postTitle" type="text">
                                <label class="form-label" for="postDesc">Description</label>
                                <div id="postDesc">
                                    <div id="toolbar-container" class="ql-toolbar ql-snow"><span class="ql-formats">
                                            <span class="ql-font ql-picker"><span class="ql-picker-label" tabindex="0" role="button" aria-expanded="false" aria-controls="ql-picker-options-0"><svg viewBox="0 0 18 18">
                                                        <polygon class="ql-stroke" points="7 11 9 13 11 11 7 11"></polygon>
                                                        <polygon class="ql-stroke" points="7 7 9 5 11 7 7 7"></polygon>
                                                    </svg></span><span class="ql-picker-options" aria-hidden="true" tabindex="-1" id="ql-picker-options-0"><span tabindex="0" role="button" class="ql-picker-item"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="serif"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="monospace"></span></span></span><select class="ql-font" style="display: none;">
                                                <option selected="selected"></option>
                                                <option value="serif"></option>
                                                <option value="monospace"></option>
                                            </select>
                                            <span class="ql-size ql-picker"><span class="ql-picker-label" tabindex="0" role="button" aria-expanded="false" aria-controls="ql-picker-options-1"><svg viewBox="0 0 18 18">
                                                        <polygon class="ql-stroke" points="7 11 9 13 11 11 7 11"></polygon>
                                                        <polygon class="ql-stroke" points="7 7 9 5 11 7 7 7"></polygon>
                                                    </svg></span><span class="ql-picker-options" aria-hidden="true" tabindex="-1" id="ql-picker-options-1"><span tabindex="0" role="button" class="ql-picker-item" data-value="small"></span><span tabindex="0" role="button" class="ql-picker-item"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="large"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="huge"></span></span></span><select class="ql-size" style="display: none;">
                                                <option value="small"></option>
                                                <option selected="selected"></option>
                                                <option value="large"></option>
                                                <option value="huge"></option>
                                            </select></span><span class="ql-formats">
                                            <button class="ql-bold" type="button"><svg viewBox="0 0 18 18">
                                                    <path class="ql-stroke" d="M5,4H9.5A2.5,2.5,0,0,1,12,6.5v0A2.5,2.5,0,0,1,9.5,9H5A0,0,0,0,1,5,9V4A0,0,0,0,1,5,4Z"></path>
                                                    <path class="ql-stroke" d="M5,9h5.5A2.5,2.5,0,0,1,13,11.5v0A2.5,2.5,0,0,1,10.5,14H5a0,0,0,0,1,0,0V9A0,0,0,0,1,5,9Z"></path>
                                                </svg></button>
                                            <button class="ql-italic" type="button"><svg viewBox="0 0 18 18">
                                                    <line class="ql-stroke" x1="7" x2="13" y1="4" y2="4"></line>
                                                    <line class="ql-stroke" x1="5" x2="11" y1="14" y2="14"></line>
                                                    <line class="ql-stroke" x1="8" x2="10" y1="14" y2="4"></line>
                                                </svg></button>
                                            <button class="ql-underline" type="button"><svg viewBox="0 0 18 18">
                                                    <path class="ql-stroke" d="M5,3V9a4.012,4.012,0,0,0,4,4H9a4.012,4.012,0,0,0,4-4V3"></path>
                                                    <rect class="ql-fill" height="1" rx="0.5" ry="0.5" width="12" x="3" y="15"></rect>
                                                </svg></button>
                                            <button class="ql-strike" type="button"><svg viewBox="0 0 18 18">
                                                    <line class="ql-stroke ql-thin" x1="15.5" x2="2.5" y1="8.5" y2="9.5"></line>
                                                    <path class="ql-fill" d="M9.007,8C6.542,7.791,6,7.519,6,6.5,6,5.792,7.283,5,9,5c1.571,0,2.765.679,2.969,1.309a1,1,0,0,0,1.9-.617C13.356,4.106,11.354,3,9,3,6.2,3,4,4.538,4,6.5a3.2,3.2,0,0,0,.5,1.843Z"></path>
                                                    <path class="ql-fill" d="M8.984,10C11.457,10.208,12,10.479,12,11.5c0,0.708-1.283,1.5-3,1.5-1.571,0-2.765-.679-2.969-1.309a1,1,0,1,0-1.9.617C4.644,13.894,6.646,15,9,15c2.8,0,5-1.538,5-3.5a3.2,3.2,0,0,0-.5-1.843Z"></path>
                                                </svg></button></span><span class="ql-formats">
                                            <span class="ql-color ql-picker ql-color-picker"><span class="ql-picker-label" tabindex="0" role="button" aria-expanded="false" aria-controls="ql-picker-options-2"><svg viewBox="0 0 18 18">
                                                        <line class="ql-color-label ql-stroke ql-transparent" x1="3" x2="15" y1="15" y2="15"></line>
                                                        <polyline class="ql-stroke" points="5.5 11 9 3 12.5 11"></polyline>
                                                        <line class="ql-stroke" x1="11.63" x2="6.38" y1="9" y2="9"></line>
                                                    </svg></span><span class="ql-picker-options" aria-hidden="true" tabindex="-1" id="ql-picker-options-2"><span tabindex="0" role="button" class="ql-picker-item ql-primary"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#e60000" style="background-color: rgb(230, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#ff9900" style="background-color: rgb(255, 153, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#ffff00" style="background-color: rgb(255, 255, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#008a00" style="background-color: rgb(0, 138, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#0066cc" style="background-color: rgb(0, 102, 204);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#9933ff" style="background-color: rgb(153, 51, 255);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffffff" style="background-color: rgb(255, 255, 255);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#facccc" style="background-color: rgb(250, 204, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffebcc" style="background-color: rgb(255, 235, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffffcc" style="background-color: rgb(255, 255, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#cce8cc" style="background-color: rgb(204, 232, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#cce0f5" style="background-color: rgb(204, 224, 245);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ebd6ff" style="background-color: rgb(235, 214, 255);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#bbbbbb" style="background-color: rgb(187, 187, 187);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#f06666" style="background-color: rgb(240, 102, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffc266" style="background-color: rgb(255, 194, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffff66" style="background-color: rgb(255, 255, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#66b966" style="background-color: rgb(102, 185, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#66a3e0" style="background-color: rgb(102, 163, 224);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#c285ff" style="background-color: rgb(194, 133, 255);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#888888" style="background-color: rgb(136, 136, 136);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#a10000" style="background-color: rgb(161, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#b26b00" style="background-color: rgb(178, 107, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#b2b200" style="background-color: rgb(178, 178, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#006100" style="background-color: rgb(0, 97, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#0047b2" style="background-color: rgb(0, 71, 178);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#6b24b2" style="background-color: rgb(107, 36, 178);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#444444" style="background-color: rgb(68, 68, 68);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#5c0000" style="background-color: rgb(92, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#663d00" style="background-color: rgb(102, 61, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#666600" style="background-color: rgb(102, 102, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#003700" style="background-color: rgb(0, 55, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#002966" style="background-color: rgb(0, 41, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#3d1466" style="background-color: rgb(61, 20, 102);"></span></span></span><select class="ql-color" style="display: none;">
                                                <option selected="selected"></option>
                                                <option value="#e60000"></option>
                                                <option value="#ff9900"></option>
                                                <option value="#ffff00"></option>
                                                <option value="#008a00"></option>
                                                <option value="#0066cc"></option>
                                                <option value="#9933ff"></option>
                                                <option value="#ffffff"></option>
                                                <option value="#facccc"></option>
                                                <option value="#ffebcc"></option>
                                                <option value="#ffffcc"></option>
                                                <option value="#cce8cc"></option>
                                                <option value="#cce0f5"></option>
                                                <option value="#ebd6ff"></option>
                                                <option value="#bbbbbb"></option>
                                                <option value="#f06666"></option>
                                                <option value="#ffc266"></option>
                                                <option value="#ffff66"></option>
                                                <option value="#66b966"></option>
                                                <option value="#66a3e0"></option>
                                                <option value="#c285ff"></option>
                                                <option value="#888888"></option>
                                                <option value="#a10000"></option>
                                                <option value="#b26b00"></option>
                                                <option value="#b2b200"></option>
                                                <option value="#006100"></option>
                                                <option value="#0047b2"></option>
                                                <option value="#6b24b2"></option>
                                                <option value="#444444"></option>
                                                <option value="#5c0000"></option>
                                                <option value="#663d00"></option>
                                                <option value="#666600"></option>
                                                <option value="#003700"></option>
                                                <option value="#002966"></option>
                                                <option value="#3d1466"></option>
                                            </select>
                                            <span class="ql-background ql-picker ql-color-picker"><span class="ql-picker-label" tabindex="0" role="button" aria-expanded="false" aria-controls="ql-picker-options-3"><svg viewBox="0 0 18 18">
                                                        <g class="ql-fill ql-color-label">
                                                            <polygon points="6 6.868 6 6 5 6 5 7 5.942 7 6 6.868"></polygon>
                                                            <rect height="1" width="1" x="4" y="4"></rect>
                                                            <polygon points="6.817 5 6 5 6 6 6.38 6 6.817 5"></polygon>
                                                            <rect height="1" width="1" x="2" y="6"></rect>
                                                            <rect height="1" width="1" x="3" y="5"></rect>
                                                            <rect height="1" width="1" x="4" y="7"></rect>
                                                            <polygon points="4 11.439 4 11 3 11 3 12 3.755 12 4 11.439"></polygon>
                                                            <rect height="1" width="1" x="2" y="12"></rect>
                                                            <rect height="1" width="1" x="2" y="9"></rect>
                                                            <rect height="1" width="1" x="2" y="15"></rect>
                                                            <polygon points="4.63 10 4 10 4 11 4.192 11 4.63 10"></polygon>
                                                            <rect height="1" width="1" x="3" y="8"></rect>
                                                            <path d="M10.832,4.2L11,4.582V4H10.708A1.948,1.948,0,0,1,10.832,4.2Z"></path>
                                                            <path d="M7,4.582L7.168,4.2A1.929,1.929,0,0,1,7.292,4H7V4.582Z"></path>
                                                            <path d="M8,13H7.683l-0.351.8a1.933,1.933,0,0,1-.124.2H8V13Z"></path>
                                                            <rect height="1" width="1" x="12" y="2"></rect>
                                                            <rect height="1" width="1" x="11" y="3"></rect>
                                                            <path d="M9,3H8V3.282A1.985,1.985,0,0,1,9,3Z"></path>
                                                            <rect height="1" width="1" x="2" y="3"></rect>
                                                            <rect height="1" width="1" x="6" y="2"></rect>
                                                            <rect height="1" width="1" x="3" y="2"></rect>
                                                            <rect height="1" width="1" x="5" y="3"></rect>
                                                            <rect height="1" width="1" x="9" y="2"></rect>
                                                            <rect height="1" width="1" x="15" y="14"></rect>
                                                            <polygon points="13.447 10.174 13.469 10.225 13.472 10.232 13.808 11 14 11 14 10 13.37 10 13.447 10.174"></polygon>
                                                            <rect height="1" width="1" x="13" y="7"></rect>
                                                            <rect height="1" width="1" x="15" y="5"></rect>
                                                            <rect height="1" width="1" x="14" y="6"></rect>
                                                            <rect height="1" width="1" x="15" y="8"></rect>
                                                            <rect height="1" width="1" x="14" y="9"></rect>
                                                            <path d="M3.775,14H3v1H4V14.314A1.97,1.97,0,0,1,3.775,14Z"></path>
                                                            <rect height="1" width="1" x="14" y="3"></rect>
                                                            <polygon points="12 6.868 12 6 11.62 6 12 6.868"></polygon>
                                                            <rect height="1" width="1" x="15" y="2"></rect>
                                                            <rect height="1" width="1" x="12" y="5"></rect>
                                                            <rect height="1" width="1" x="13" y="4"></rect>
                                                            <polygon points="12.933 9 13 9 13 8 12.495 8 12.933 9"></polygon>
                                                            <rect height="1" width="1" x="9" y="14"></rect>
                                                            <rect height="1" width="1" x="8" y="15"></rect>
                                                            <path d="M6,14.926V15H7V14.316A1.993,1.993,0,0,1,6,14.926Z"></path>
                                                            <rect height="1" width="1" x="5" y="15"></rect>
                                                            <path d="M10.668,13.8L10.317,13H10v1h0.792A1.947,1.947,0,0,1,10.668,13.8Z"></path>
                                                            <rect height="1" width="1" x="11" y="15"></rect>
                                                            <path d="M14.332,12.2a1.99,1.99,0,0,1,.166.8H15V12H14.245Z"></path>
                                                            <rect height="1" width="1" x="14" y="15"></rect>
                                                            <rect height="1" width="1" x="15" y="11"></rect>
                                                        </g>
                                                        <polyline class="ql-stroke" points="5.5 13 9 5 12.5 13"></polyline>
                                                        <line class="ql-stroke" x1="11.63" x2="6.38" y1="11" y2="11"></line>
                                                    </svg></span><span class="ql-picker-options" aria-hidden="true" tabindex="-1" id="ql-picker-options-3"><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#000000" style="background-color: rgb(0, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#e60000" style="background-color: rgb(230, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#ff9900" style="background-color: rgb(255, 153, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#ffff00" style="background-color: rgb(255, 255, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#008a00" style="background-color: rgb(0, 138, 0);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#0066cc" style="background-color: rgb(0, 102, 204);"></span><span tabindex="0" role="button" class="ql-picker-item ql-primary" data-value="#9933ff" style="background-color: rgb(153, 51, 255);"></span><span tabindex="0" role="button" class="ql-picker-item"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#facccc" style="background-color: rgb(250, 204, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffebcc" style="background-color: rgb(255, 235, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffffcc" style="background-color: rgb(255, 255, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#cce8cc" style="background-color: rgb(204, 232, 204);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#cce0f5" style="background-color: rgb(204, 224, 245);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ebd6ff" style="background-color: rgb(235, 214, 255);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#bbbbbb" style="background-color: rgb(187, 187, 187);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#f06666" style="background-color: rgb(240, 102, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffc266" style="background-color: rgb(255, 194, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#ffff66" style="background-color: rgb(255, 255, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#66b966" style="background-color: rgb(102, 185, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#66a3e0" style="background-color: rgb(102, 163, 224);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#c285ff" style="background-color: rgb(194, 133, 255);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#888888" style="background-color: rgb(136, 136, 136);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#a10000" style="background-color: rgb(161, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#b26b00" style="background-color: rgb(178, 107, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#b2b200" style="background-color: rgb(178, 178, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#006100" style="background-color: rgb(0, 97, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#0047b2" style="background-color: rgb(0, 71, 178);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#6b24b2" style="background-color: rgb(107, 36, 178);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#444444" style="background-color: rgb(68, 68, 68);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#5c0000" style="background-color: rgb(92, 0, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#663d00" style="background-color: rgb(102, 61, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#666600" style="background-color: rgb(102, 102, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#003700" style="background-color: rgb(0, 55, 0);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#002966" style="background-color: rgb(0, 41, 102);"></span><span tabindex="0" role="button" class="ql-picker-item" data-value="#3d1466" style="background-color: rgb(61, 20, 102);"></span></span></span><select class="ql-background" style="display: none;">
                                                <option value="#000000"></option>
                                                <option value="#e60000"></option>
                                                <option value="#ff9900"></option>
                                                <option value="#ffff00"></option>
                                                <option value="#008a00"></option>
                                                <option value="#0066cc"></option>
                                                <option value="#9933ff"></option>
                                                <option selected="selected"></option>
                                                <option value="#facccc"></option>
                                                <option value="#ffebcc"></option>
                                                <option value="#ffffcc"></option>
                                                <option value="#cce8cc"></option>
                                                <option value="#cce0f5"></option>
                                                <option value="#ebd6ff"></option>
                                                <option value="#bbbbbb"></option>
                                                <option value="#f06666"></option>
                                                <option value="#ffc266"></option>
                                                <option value="#ffff66"></option>
                                                <option value="#66b966"></option>
                                                <option value="#66a3e0"></option>
                                                <option value="#c285ff"></option>
                                                <option value="#888888"></option>
                                                <option value="#a10000"></option>
                                                <option value="#b26b00"></option>
                                                <option value="#b2b200"></option>
                                                <option value="#006100"></option>
                                                <option value="#0047b2"></option>
                                                <option value="#6b24b2"></option>
                                                <option value="#444444"></option>
                                                <option value="#5c0000"></option>
                                                <option value="#663d00"></option>
                                                <option value="#666600"></option>
                                                <option value="#003700"></option>
                                                <option value="#002966"></option>
                                                <option value="#3d1466"></option>
                                            </select></span><span class="ql-formats">
                                            <button class="ql-script" value="sub" type="button"><svg viewBox="0 0 18 18">
                                                    <path class="ql-fill" d="M15.5,15H13.861a3.858,3.858,0,0,0,1.914-2.975,1.8,1.8,0,0,0-1.6-1.751A1.921,1.921,0,0,0,12.021,11.7a0.50013,0.50013,0,1,0,.957.291h0a0.914,0.914,0,0,1,1.053-.725,0.81,0.81,0,0,1,.744.762c0,1.076-1.16971,1.86982-1.93971,2.43082A1.45639,1.45639,0,0,0,12,15.5a0.5,0.5,0,0,0,.5.5h3A0.5,0.5,0,0,0,15.5,15Z"></path>
                                                    <path class="ql-fill" d="M9.65,5.241a1,1,0,0,0-1.409.108L6,7.964,3.759,5.349A1,1,0,0,0,2.192,6.59178Q2.21541,6.6213,2.241,6.649L4.684,9.5,2.241,12.35A1,1,0,0,0,3.71,13.70722q0.02557-.02768.049-0.05722L6,11.036,8.241,13.65a1,1,0,1,0,1.567-1.24277Q9.78459,12.3777,9.759,12.35L7.316,9.5,9.759,6.651A1,1,0,0,0,9.65,5.241Z"></path>
                                                </svg></button>
                                            <button class="ql-script" value="super" type="button"><svg viewBox="0 0 18 18">
                                                    <path class="ql-fill" d="M15.5,7H13.861a4.015,4.015,0,0,0,1.914-2.975,1.8,1.8,0,0,0-1.6-1.751A1.922,1.922,0,0,0,12.021,3.7a0.5,0.5,0,1,0,.957.291,0.917,0.917,0,0,1,1.053-.725,0.81,0.81,0,0,1,.744.762c0,1.077-1.164,1.925-1.934,2.486A1.423,1.423,0,0,0,12,7.5a0.5,0.5,0,0,0,.5.5h3A0.5,0.5,0,0,0,15.5,7Z"></path>
                                                    <path class="ql-fill" d="M9.651,5.241a1,1,0,0,0-1.41.108L6,7.964,3.759,5.349a1,1,0,1,0-1.519,1.3L4.683,9.5,2.241,12.35a1,1,0,1,0,1.519,1.3L6,11.036,8.241,13.65a1,1,0,0,0,1.519-1.3L7.317,9.5,9.759,6.651A1,1,0,0,0,9.651,5.241Z"></path>
                                                </svg></button></span><span class="ql-formats">
                                            <button class="ql-header" value="1" type="button"><svg viewBox="0 0 18 18">
                                                    <path class="ql-fill" d="M10,4V14a1,1,0,0,1-2,0V10H3v4a1,1,0,0,1-2,0V4A1,1,0,0,1,3,4V8H8V4a1,1,0,0,1,2,0Zm6.06787,9.209H14.98975V7.59863a.54085.54085,0,0,0-.605-.60547h-.62744a1.01119,1.01119,0,0,0-.748.29688L11.645,8.56641a.5435.5435,0,0,0-.022.8584l.28613.30762a.53861.53861,0,0,0,.84717.0332l.09912-.08789a1.2137,1.2137,0,0,0,.2417-.35254h.02246s-.01123.30859-.01123.60547V13.209H12.041a.54085.54085,0,0,0-.605.60547v.43945a.54085.54085,0,0,0,.605.60547h4.02686a.54085.54085,0,0,0,.605-.60547v-.43945A.54085.54085,0,0,0,16.06787,13.209Z"></path>
                                                </svg></button>
                                            <button class="ql-header" value="2" type="button"><svg viewBox="0 0 18 18">
                                                    <path class="ql-fill" d="M16.73975,13.81445v.43945a.54085.54085,0,0,1-.605.60547H11.855a.58392.58392,0,0,1-.64893-.60547V14.0127c0-2.90527,3.39941-3.42187,3.39941-4.55469a.77675.77675,0,0,0-.84717-.78125,1.17684,1.17684,0,0,0-.83594.38477c-.2749.26367-.561.374-.85791.13184l-.4292-.34082c-.30811-.24219-.38525-.51758-.1543-.81445a2.97155,2.97155,0,0,1,2.45361-1.17676,2.45393,2.45393,0,0,1,2.68408,2.40918c0,2.45312-3.1792,2.92676-3.27832,3.93848h2.79443A.54085.54085,0,0,1,16.73975,13.81445ZM9,3A.99974.99974,0,0,0,8,4V8H3V4A1,1,0,0,0,1,4V14a1,1,0,0,0,2,0V10H8v4a1,1,0,0,0,2,0V4A.99974.99974,0,0,0,9,3Z"></path>
                                                </svg></button>
                                            <button class="ql-blockquote" type="button"><svg viewBox="0 0 18 18">
                                                    <rect class="ql-fill ql-stroke" height="3" width="3" x="4" y="5"></rect>
                                                    <rect class="ql-fill ql-stroke" height="3" width="3" x="11" y="5"></rect>
                                                    <path class="ql-even ql-fill ql-stroke" d="M7,8c0,4.031-3,5-3,5"></path>
                                                    <path class="ql-even ql-fill ql-stroke" d="M14,8c0,4.031-3,5-3,5"></path>
                                                </svg></button>
                                            <button class="ql-code-block" type="button"><svg viewBox="0 0 18 18">
                                                    <polyline class="ql-even ql-stroke" points="5 7 3 9 5 11"></polyline>
                                                    <polyline class="ql-even ql-stroke" points="13 7 15 9 13 11"></polyline>
                                                    <line class="ql-stroke" x1="10" x2="8" y1="5" y2="13"></line>
                                                </svg></button></span><span class="ql-formats">
                                            <button class="ql-list" value="ordered" type="button"><svg viewBox="0 0 18 18">
                                                    <line class="ql-stroke" x1="7" x2="15" y1="4" y2="4"></line>
                                                    <line class="ql-stroke" x1="7" x2="15" y1="9" y2="9"></line>
                                                    <line class="ql-stroke" x1="7" x2="15" y1="14" y2="14"></line>
                                                    <line class="ql-stroke ql-thin" x1="2.5" x2="4.5" y1="5.5" y2="5.5"></line>
                                                    <path class="ql-fill" d="M3.5,6A0.5,0.5,0,0,1,3,5.5V3.085l-0.276.138A0.5,0.5,0,0,1,2.053,3c-0.124-.247-0.023-0.324.224-0.447l1-.5A0.5,0.5,0,0,1,4,2.5v3A0.5,0.5,0,0,1,3.5,6Z"></path>
                                                    <path class="ql-stroke ql-thin" d="M4.5,10.5h-2c0-.234,1.85-1.076,1.85-2.234A0.959,0.959,0,0,0,2.5,8.156"></path>
                                                    <path class="ql-stroke ql-thin" d="M2.5,14.846a0.959,0.959,0,0,0,1.85-.109A0.7,0.7,0,0,0,3.75,14a0.688,0.688,0,0,0,.6-0.736,0.959,0.959,0,0,0-1.85-.109"></path>
                                                </svg></button>
                                            <button class="ql-list" value="bullet" type="button"><svg viewBox="0 0 18 18">
                                                    <line class="ql-stroke" x1="6" x2="15" y1="4" y2="4"></line>
                                                    <line class="ql-stroke" x1="6" x2="15" y1="9" y2="9"></line>
                                                    <line class="ql-stroke" x1="6" x2="15" y1="14" y2="14"></line>
                                                    <line class="ql-stroke" x1="3" x2="3" y1="4" y2="4"></line>
                                                    <line class="ql-stroke" x1="3" x2="3" y1="9" y2="9"></line>
                                                    <line class="ql-stroke" x1="3" x2="3" y1="14" y2="14"></line>
                                                </svg></button>
                                            <button class="ql-indent" value="-1" type="button"><svg viewBox="0 0 18 18">
                                                    <line class="ql-stroke" x1="3" x2="15" y1="14" y2="14"></line>
                                                    <line class="ql-stroke" x1="3" x2="15" y1="4" y2="4"></line>
                                                    <line class="ql-stroke" x1="9" x2="15" y1="9" y2="9"></line>
                                                    <polyline class="ql-stroke" points="5 7 5 11 3 9 5 7"></polyline>
                                                </svg></button>
                                            <button class="ql-indent" value="+1" type="button"><svg viewBox="0 0 18 18">
                                                    <line class="ql-stroke" x1="3" x2="15" y1="14" y2="14"></line>
                                                    <line class="ql-stroke" x1="3" x2="15" y1="4" y2="4"></line>
                                                    <line class="ql-stroke" x1="9" x2="15" y1="9" y2="9"></line>
                                                    <polyline class="ql-fill ql-stroke" points="3 7 3 11 5 9 3 7"></polyline>
                                                </svg></button></span><span class="ql-formats">
                                            <button class="ql-direction" value="rtl" type="button"><svg viewBox="0 0 18 18">
                                                    <polygon class="ql-stroke ql-fill" points="3 11 5 9 3 7 3 11"></polygon>
                                                    <line class="ql-stroke ql-fill" x1="15" x2="11" y1="4" y2="4"></line>
                                                    <path class="ql-fill" d="M11,3a3,3,0,0,0,0,6h1V3H11Z"></path>
                                                    <rect class="ql-fill" height="11" width="1" x="11" y="4"></rect>
                                                    <rect class="ql-fill" height="11" width="1" x="13" y="4"></rect>
                                                </svg><svg viewBox="0 0 18 18">
                                                    <polygon class="ql-stroke ql-fill" points="15 12 13 10 15 8 15 12"></polygon>
                                                    <line class="ql-stroke ql-fill" x1="9" x2="5" y1="4" y2="4"></line>
                                                    <path class="ql-fill" d="M5,3A3,3,0,0,0,5,9H6V3H5Z"></path>
                                                    <rect class="ql-fill" height="11" width="1" x="5" y="4"></rect>
                                                    <rect class="ql-fill" height="11" width="1" x="7" y="4"></rect>
                                                </svg></button>
                                            <span class="ql-align ql-picker ql-icon-picker"><span class="ql-picker-label" tabindex="0" role="button" aria-expanded="false" aria-controls="ql-picker-options-4"><svg viewBox="0 0 18 18">
                                                        <line class="ql-stroke" x1="3" x2="15" y1="9" y2="9"></line>
                                                        <line class="ql-stroke" x1="3" x2="13" y1="14" y2="14"></line>
                                                        <line class="ql-stroke" x1="3" x2="9" y1="4" y2="4"></line>
                                                    </svg></span><span class="ql-picker-options" aria-hidden="true" tabindex="-1" id="ql-picker-options-4"><span tabindex="0" role="button" class="ql-picker-item"><svg viewBox="0 0 18 18">
                                                            <line class="ql-stroke" x1="3" x2="15" y1="9" y2="9"></line>
                                                            <line class="ql-stroke" x1="3" x2="13" y1="14" y2="14"></line>
                                                            <line class="ql-stroke" x1="3" x2="9" y1="4" y2="4"></line>
                                                        </svg></span><span tabindex="0" role="button" class="ql-picker-item" data-value="center"><svg viewBox="0 0 18 18">
                                                            <line class="ql-stroke" x1="15" x2="3" y1="9" y2="9"></line>
                                                            <line class="ql-stroke" x1="14" x2="4" y1="14" y2="14"></line>
                                                            <line class="ql-stroke" x1="12" x2="6" y1="4" y2="4"></line>
                                                        </svg></span><span tabindex="0" role="button" class="ql-picker-item" data-value="right"><svg viewBox="0 0 18 18">
                                                            <line class="ql-stroke" x1="15" x2="3" y1="9" y2="9"></line>
                                                            <line class="ql-stroke" x1="15" x2="5" y1="14" y2="14"></line>
                                                            <line class="ql-stroke" x1="15" x2="9" y1="4" y2="4"></line>
                                                        </svg></span><span tabindex="0" role="button" class="ql-picker-item" data-value="justify"><svg viewBox="0 0 18 18">
                                                            <line class="ql-stroke" x1="15" x2="3" y1="9" y2="9"></line>
                                                            <line class="ql-stroke" x1="15" x2="3" y1="14" y2="14"></line>
                                                            <line class="ql-stroke" x1="15" x2="3" y1="4" y2="4"></line>
                                                        </svg></span></span></span><select class="ql-align" style="display: none;">
                                                <option selected="selected"></option>
                                                <option value="center"></option>
                                                <option value="right"></option>
                                                <option value="justify"></option>
                                            </select></span><span class="ql-formats">
                                            <button class="ql-link" type="button"><svg viewBox="0 0 18 18">
                                                    <line class="ql-stroke" x1="7" x2="11" y1="7" y2="11"></line>
                                                    <path class="ql-even ql-stroke" d="M8.9,4.577a3.476,3.476,0,0,1,.36,4.679A3.476,3.476,0,0,1,4.577,8.9C3.185,7.5,2.035,6.4,4.217,4.217S7.5,3.185,8.9,4.577Z"></path>
                                                    <path class="ql-even ql-stroke" d="M13.423,9.1a3.476,3.476,0,0,0-4.679-.36,3.476,3.476,0,0,0,.36,4.679c1.392,1.392,2.5,2.542,4.679.36S14.815,10.5,13.423,9.1Z"></path>
                                                </svg></button></span><span class="ql-formats">
                                            <button class="ql-clean" type="button"><svg class="" viewBox="0 0 18 18">
                                                    <line class="ql-stroke" x1="5" x2="13" y1="3" y2="3"></line>
                                                    <line class="ql-stroke" x1="6" x2="9.35" y1="12" y2="3"></line>
                                                    <line class="ql-stroke" x1="11" x2="15" y1="11" y2="15"></line>
                                                    <line class="ql-stroke" x1="15" x2="11" y1="11" y2="15"></line>
                                                    <rect class="ql-fill" height="1" rx="0.5" ry="0.5" width="7" x="2" y="14"></rect>
                                                </svg></button></span></div>
                                    <div class="bg-white ql-container ql-snow" id="editor-container" style="min-height: 300px; max-height: 1000px;overflow-y: auto;">
                                        <div class="ql-editor" data-gramm="false" contenteditable="true" data-placeholder="Compose an epic...">
                                            <h2></h2>
                                            <p><br></p>
                                            <p></p>
                                        </div>
                                        <div class="ql-clipboard" contenteditable="true" tabindex="-1"></div>
                                        <div class="ql-tooltip ql-hidden"><a class="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></a><input type="text" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>
                                    </div>
                                </div>



                                <div id="wp-m52module0copy-wrap" class="wp-core-ui wp-editor-wrap tmce-active">
                                    <div id="wp-m52module0copy-editor-tools" class="wp-editor-tools hide-if-no-js">
                                        <div id="wp-m52module0copy-media-buttons" class="wp-media-buttons"><a href="#" id="insert-media-button" class="button insert-media add_media" data-editor="m52module0copy" title="Add Media"><span class="wp-media-buttons-icon"></span> Add Media</a></div>
                                        <div class="wp-editor-tabs"><a id="m52module0copy-html" class="wp-switch-editor switch-html" data-wp-editor-id="m52module0copy">Text</a><a id="m52module0copy-tmce" class="wp-switch-editor switch-tmce" data-wp-editor-id="m52module0copy">Visual</a></div>
                                    </div>
                                    <div id="wp-m52module0copy-editor-container" class="wp-editor-container">
                                        <div id="qt_m52module0copy_toolbar" class="quicktags-toolbar"><input type="button" id="qt_m52module0copy_strong" class="ed_button button button-small" aria-label="Bold" value="b"><input type="button" id="qt_m52module0copy_em" class="ed_button button button-small" aria-label="Italic" value="i"><input type="button" id="qt_m52module0copy_link" class="ed_button button button-small" aria-label="Insert link" value="link"><input type="button" id="qt_m52module0copy_block" class="ed_button button button-small" aria-label="Blockquote" value="b-quote"><input type="button" id="qt_m52module0copy_del" class="ed_button button button-small" aria-label="Deleted text (strikethrough)" value="del"><input type="button" id="qt_m52module0copy_ins" class="ed_button button button-small" aria-label="Inserted text" value="ins"><input type="button" id="qt_m52module0copy_img" class="ed_button button button-small" aria-label="Insert image" value="img"><input type="button" id="qt_m52module0copy_ul" class="ed_button button button-small" aria-label="Bulleted list" value="ul"><input type="button" id="qt_m52module0copy_ol" class="ed_button button button-small" aria-label="Numbered list" value="ol"><input type="button" id="qt_m52module0copy_li" class="ed_button button button-small" aria-label="List item" value="li"><input type="button" id="qt_m52module0copy_code" class="ed_button button button-small" aria-label="Code" value="code"><input type="button" id="qt_m52module0copy_more" class="ed_button button button-small" aria-label="Insert Read More tag" value="more"><input type="button" id="qt_m52module0copy_button" class="ed_button button button-small" aria-label="Button" value="button"><input type="button" id="qt_m52module0copy_close" class="ed_button button button-small" title="Close all open tags" value="close tags"></div>
                                        <div id="mceu_13" class="mce-tinymce mce-container mce-panel" hidefocus="1" tabindex="-1" role="application" style="visibility: hidden; border-width: 1px; width: 100%;">
                                            <div id="mceu_13-body" class="mce-container-body mce-stack-layout">
                                                <div id="mceu_14" class="mce-top-part mce-container mce-stack-layout-item mce-first">
                                                    <div id="mceu_14-body" class="mce-container-body">
                                                        <div id="mceu_15" class="mce-toolbar-grp mce-container mce-panel mce-first mce-last" hidefocus="1" tabindex="-1" role="group">
                                                            <div id="mceu_15-body" class="mce-container-body mce-stack-layout">
                                                                <div id="mceu_16" class="mce-container mce-toolbar mce-stack-layout-item mce-first mce-last" role="toolbar">
                                                                    <div id="mceu_16-body" class="mce-container-body mce-flow-layout">
                                                                        <div id="mceu_17" class="mce-container mce-flow-layout-item mce-first mce-last mce-btn-group" role="group">
                                                                            <div id="mceu_17-body">
                                                                                <div id="mceu_0" class="mce-widget mce-btn mce-menubtn mce-fixed-width mce-listbox mce-first mce-btn-has-text" tabindex="-1" aria-labelledby="mceu_0" role="button" aria-haspopup="true"><button id="mceu_0-open" role="presentation" type="button" tabindex="-1"><span class="mce-txt">Paragraph</span> <i class="mce-caret"></i></button></div>
                                                                                <div id="mceu_1" class="mce-widget mce-btn" tabindex="-1" aria-pressed="false" role="button" aria-label="Bold"><button id="mceu_1-button" role="presentation" type="button" tabindex="-1"><i class="mce-ico mce-i-bold"></i></button></div>
                                                                                <div id="mceu_2" class="mce-widget mce-btn" tabindex="-1" aria-pressed="false" role="button" aria-label="Italic"><button id="mceu_2-button" role="presentation" type="button" tabindex="-1"><i class="mce-ico mce-i-italic"></i></button></div>
                                                                                <div id="mceu_3" class="mce-widget mce-btn" tabindex="-1" aria-pressed="false" role="button" aria-label="Underline"><button id="mceu_3-button" role="presentation" type="button" tabindex="-1"><i class="mce-ico mce-i-underline"></i></button></div>
                                                                                <div id="mceu_4" class="mce-widget mce-btn" tabindex="-1" aria-pressed="false" role="button" aria-label="Blockquote"><button id="mceu_4-button" role="presentation" type="button" tabindex="-1"><i class="mce-ico mce-i-blockquote"></i></button></div>
                                                                                <div id="mceu_5" class="mce-widget mce-btn mce-disabled" tabindex="-1" role="button" aria-label="Undo" aria-disabled="true"><button id="mceu_5-button" role="presentation" type="button" tabindex="-1"><i class="mce-ico mce-i-undo"></i></button></div>
                                                                                <div id="mceu_6" class="mce-widget mce-btn mce-disabled" tabindex="-1" role="button" aria-label="Redo" aria-disabled="true"><button id="mceu_6-button" role="presentation" type="button" tabindex="-1"><i class="mce-ico mce-i-redo"></i></button></div>
                                                                                <div id="mceu_7" class="mce-widget mce-btn" tabindex="-1" role="button" aria-label="Clear formatting"><button id="mceu_7-button" role="presentation" type="button" tabindex="-1"><i class="mce-ico mce-i-removeformat"></i></button></div>
                                                                                <div id="mceu_8" class="mce-widget mce-btn" tabindex="-1" aria-pressed="false" role="button" aria-label="Subscript"><button id="mceu_8-button" role="presentation" type="button" tabindex="-1"><i class="mce-ico mce-i-subscript"></i></button></div>
                                                                                <div id="mceu_9" class="mce-widget mce-btn" tabindex="-1" aria-pressed="false" role="button" aria-label="Superscript"><button id="mceu_9-button" role="presentation" type="button" tabindex="-1"><i class="mce-ico mce-i-superscript"></i></button></div>
                                                                                <div id="mceu_10" class="mce-widget mce-btn" tabindex="-1" aria-pressed="false" role="button" aria-label="Insert/edit link"><button id="mceu_10-button" role="presentation" type="button" tabindex="-1"><i class="mce-ico mce-i-link"></i></button></div>
                                                                                <div id="mceu_11" class="mce-widget mce-btn" tabindex="-1" role="button" aria-label="Remove link"><button id="mceu_11-button" role="presentation" type="button" tabindex="-1"><i class="mce-ico mce-i-unlink"></i></button></div>
                                                                                <div id="mceu_12" class="mce-widget mce-btn mce-ico mce-i-custom-button mce-last mce-btn-has-text" tabindex="-1" role="button"><button id="mceu_12-button" role="presentation" type="button" tabindex="-1"><span class="mce-txt">Button</span></button></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="mceu_18" class="mce-edit-area mce-container mce-panel mce-stack-layout-item" hidefocus="1" tabindex="-1" role="group" style="border-width: 1px 0px 0px;"><iframe id="m52module0copy_ifr" frameborder="0" allowtransparency="true" title="Rich Text Area. Press Alt-Shift-H for help." style="width: 100%; height: 100px; display: block;"></iframe></div>
                                                <div id="mceu_19" class="mce-statusbar mce-container mce-panel mce-stack-layout-item mce-last" hidefocus="1" tabindex="-1" role="group" style="border-width: 1px 0px 0px;">
                                                    <div id="mceu_19-body" class="mce-container-body mce-flow-layout">
                                                        <div id="mceu_20" class="mce-path mce-flow-layout-item mce-first">
                                                            <div class="mce-path-item">&nbsp;</div>
                                                        </div>
                                                        <div id="mceu_21" class="mce-flow-layout-item mce-resizehandle"><i class="mce-ico mce-i-resize"></i></div><span id="mceu_22" class="mce-branding mce-widget mce-label mce-flow-layout-item mce-last"> Powered by <a href="https://www.tiny.cloud/?utm_campaign=editor_referral&amp;utm_medium=poweredby&amp;utm_source=tinymce" rel="noopener" target="_blank" role="presentation" tabindex="-1">Tiny</a></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><textarea id="m52module0copy" class="wp-tmce attribute wp-editor-area" name="m52_module[0][copy]" data-settings="{&quot;media_buttons&quot;:true,&quot;quicktags&quot;:true}" style="display: none;" aria-hidden="true"></textarea>
                                    </div>
                                </div>





















                            </div>
                        </div>
                    </div>


                </div>
                <!-- Accordion End -->


                <div id="sample">
                    <script type="text/javascript" src="js/nicEdit-latest.js"></script>
                    <script type="text/javascript">
                        //<![CDATA[
                        bkLib.onDomLoaded(function() {
                            new nicEditor().panelInstance('area1');
                            new nicEditor({
                                fullPanel: true
                            }).panelInstance('area2');
                            new nicEditor({
                                iconsPath: '../nicEditorIcons.gif'
                            }).panelInstance('area3');
                            new nicEditor({
                                buttonList: ['fontSize', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'html', 'image']
                            }).panelInstance('area4');
                            new nicEditor({
                                maxHeight: 100
                            }).panelInstance('area5');
                        });
                        //]]>
                    </script>

                </div>

            </div>
        </div>
    </div>
    <div class="col-lg-4 col-xxl-3">
        <div class="card card-sm shadow-sm mb-4">
            <div class="card-header py-4">
                <h4 class="card-heading">Publish</h4>
            </div>
            <div class="card-body text-gray-700">
                <div class="d-flex mb-4 justify-content-between">
                    <button class="btn btn-sm btn-outline-secondary">Save Draft</button>
                    <button class="btn btn-sm btn-outline-secondary">Preview</button>
                </div>
                <hr class="bg-gray-500">
                <div class="mb-3">
                    Status: <strong>Draft </strong><a class="ms-2 text-sm" data-bs-toggle="collapse" href="#collapseStatus" role="button" aria-expanded="false" aria-controls="collapseStatus">Edit</a>
                    <div class="collapse" id="collapseStatus">
                        <div class="py-2">
                            <select class="form-select form-select-sm" aria-label="Default select example">
                                <option>Draft</option>
                                <option>Pending Review</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="mb-3">
                    Visibility: <strong>Public </strong><a class="ms-2 text-sm" data-bs-toggle="collapse" href="#collapseVisibility" role="button" aria-expanded="false" aria-controls="collapseVisibility">Edit</a>
                    <div class="collapse" id="collapseVisibility">
                        <div class="py-2">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="visibility" id="visibility1">
                                <label class="form-check-label" for="visibility1">Public</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="visibility" id="visibility2">
                                <label class="form-check-label" for="visibility2">Password protected</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="visibility" id="visibility3">
                                <label class="form-check-label" for="visibility3">Private</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    Publish <strong>immediately </strong><a class="ms-2 text-sm" data-bs-toggle="collapse" href="#collapsePublish" role="button" aria-expanded="false" aria-controls="collapsePublish">Edit</a>
                    <div class="collapse" id="collapsePublish">
                        <div class="py-3">
                            <div class="row g-2">
                                <div class="col-lg-6">
                                    <input class="form-control form-control-sm" id="datePublished" value="10/20/2017">
                                </div>
                                <div class="col-lg-6">
                                    <div class="d-flex align-items-center text-sm"><span class="me-1">at</span>
                                        <input class="form-control form-control-sm text-center" id="hoursPublished" type="text" value="8"><span class="mx-1">:</span>
                                        <input class="form-control form-control-sm text-center" id="minutesPublished" type="text" value="00">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-end">
                <button class="btn btn-primary">Publish</button>
            </div>
        </div>
        <div class="card shadow-sm mb-4">
            <div class="card-header py-4">
                <h4 class="card-heading">Format</h4>
            </div>
            <div class="card-body">
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="format" id="format0" checked>
                    <label class="form-check-label" for="format0">Standard</label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="format" id="format1">
                    <label class="form-check-label" for="format1">Aside</label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="format" id="format2">
                    <label class="form-check-label" for="format2">Image</label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="format" id="format3">
                    <label class="form-check-label" for="format3">Video</label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="format" id="format4">
                    <label class="form-check-label" for="format4">Quote</label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="format" id="format5">
                    <label class="form-check-label" for="format5">Link</label>
                </div>
            </div>
        </div>
        <div class="card shadow-sm mb-4">
            <div class="card-header py-4">
                <h4 class="card-heading">Categories</h4>
            </div>
            <div class="card-body">
                <div class="mb-4">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="category" id="category0">
                        <label class="form-check-label" for="category0">Gear</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="category" id="category1">
                        <label class="form-check-label" for="category1">Stories</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="category" id="category2">
                        <label class="form-check-label" for="category2">Tips &amp; Tricks</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="category" id="category3">
                        <label class="form-check-label" for="category3">Trips</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="category" id="category4">
                        <label class="form-check-label" for="category4">Uncategorized</label>
                    </div>
                </div><a class="btn-link" href="#">+ Add New Category</a>
            </div>
        </div>
        <div class="card shadow-sm mb-4">
            <div class="card-header py-4">
                <h4 class="card-heading">Tags</h4>
            </div>
            <div class="card-body">
                <input class="form-control" id="tags" type="text" value="Tag 1, Tag 2" placeholder="Enter something">
            </div>
        </div>
    </div>
</div>

<!-- Script for Add Button Start -->
<script>
    $('.add').on('click', add);
    $('.remove').on('click', remove);

    function add() {
        var new_chq_no = parseInt($('#total_chq').val()) + 1;
        var new_input = "<input type='text' id='new_" + new_chq_no + "'>";
        var new_desc = "<input type='textarea' id='new_" + new_chq_no + "'>";

        $('#new_chq').append(new_input);
        $('#new_chq').append(new_desc);

        $('#total_chq').val(new_chq_no);
        $('#total_chq').val(new_chq_no);
    }

    function remove() {
        var last_chq_no = $('#total_chq').val();

        if (last_chq_no > 0) {
            $('#new_' + last_chq_no).remove();
            $('#total_chq').val(last_chq_no - 1);
        }
    }
</script>



<!-- Script for Add Button Start -->