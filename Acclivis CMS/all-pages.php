<?php include 'toolbar.php';
?>
<div class="row mb-5">
  <div class="col-lg-9">
    <div class="card">
      <div class="card-body card-table">
        <div class="card-header">
          <h5 class="card-heading">Latest orders</h5>
          <div class="card-header-more">
            <button class="btn-header-more" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></button>
            <div class="dropdown-menu dropdown-menu-end text-sm"><a class="dropdown-item" href="#!"><i class="fas fa-expand-arrows-alt opacity-5 me-2"></i>Expand</a><a class="dropdown-item" href="#!"><i class="far fa-window-minimize opacity-5 me-2"></i>Minimize</a><a class="dropdown-item" href="#!"><i class="fas fa-redo opacity-5 me-2"></i> Reload</a><a class="dropdown-item" href="#!"><i class="far fa-trash-alt opacity-5 me-2"></i> Remove </a></div>
          </div>
        </div>
        <div class="card-body">
          <div class="preload-wrapper opacity-10">
            <div class="table-responsive">
              <div class="dataTable-wrapper dataTable-loading no-footer sortable searchable fixed-columns">
                <div class="dataTable-top">
                  <div class="dataTable-dropdown"><span class="me-2" id="categoryBulkActionOrders">
                      <select class="form-select form-select-sm d-inline w-auto" name="categoryBulkAction">
                        <option>Bulk Actions</option>
                        <option>Delete</option>
                      </select>
                      <button class="btn btn-sm btn-outline-primary align-top">Apply</button></span><label><select class="dataTable-selector form-select form-select-sm">
                        <option value="5">5</option>
                        <option value="10" selected="">10</option>
                        <option value="15">15</option>
                        <option value="20">20</option>
                        <option value="25">25</option>
                      </select> entries per page</label></div>
                  <div class="dataTable-search"><input class="dataTable-input form-control form-control-sm" placeholder="Search..." type="text"></div>
                </div>
                <div class="dataTable-container">
                  <table class="table table-hover mb-0 dataTable-table" id="ordersDatatable">
                    <thead>
                      <tr>
                        <th data-sortable=""><a href="#" class="dataTable-sorter">Id</a></th>
                        <th data-sortable=""><a href="#" class="dataTable-sorter">Title</a></th>
                        <th data-sortable=""><a href="#" class="dataTable-sorter">Date</a></th>
                        <th data-sortable=""><a href="#" class="dataTable-sorter">Author</a></th>
                        <th data-sortable=""><a href="#" class="dataTable-sorter">Template</a></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr class="align-middle">
                        <td> <span class="form-check">
                            <input class="form-check-input" type="checkbox" id="check0"></span></td>
                        <td class="title column-title has-row-actions column-primary page-title" data-colname="Title">
                          <div class="locked-info"><span class="locked-avatar"></span> <span class="locked-text"></span></div>
                          <strong><a class="row-title" href="action=edit" aria-label="“About Us” (Edit)"> <strong>Home</strong></a></strong>
                          <div class="row-actions"><span class="edit text-muted"><a href="index.php?action=home-page" aria-label="Edit “About Us”">Edit</a> | </span>
                          <span class="trash"><a href="" class="submitdelete" aria-label="Move “About Us” to the Trash">Trash</a> | </span>
                          <span class="trash"><a href="" class="editinline" aria-label="Quick edit “About Us” inline">Quick Edit</a> | </span>
                          <span class="view"><a href="/about-us/" rel="bookmark" aria-label="View “About Us”">View</a></span></div>
                        </td>
                        <td>2021/01/05</td>
                        <td> <strong>Nielsen Cobb</strong><br></td>
                        <td><span class="badge badge-success-light"> <span class="indicator"></span>Open</span></td>
                      </tr>
                      <tr class="align-middle">
                        <td> <span class="form-check">
                            <input class="form-check-input" type="checkbox" id="check0"></span></td>
                        <td class="title column-title has-row-actions column-primary page-title" data-colname="Title">
                          <div class="locked-info"><span class="locked-avatar"></span> <span class="locked-text"></span></div>
                          <strong><a class="row-title" href="action=edit" aria-label="“About Us” (Edit)"> <strong>About Us</strong></a></strong>
                          <div class="row-actions"><span class="edit text-muted"><a href="action=edit" aria-label="Edit “About Us”">Edit</a> | </span>
                          <span class="trash"><a href="" class="submitdelete" aria-label="Move “About Us” to the Trash">Trash</a> | </span>
                          <span class="trash"><a href="" class="editinline" aria-label="Quick edit “About Us” inline">Quick Edit</a> | </span>
                          <span class="view"><a href="/about-us/" rel="bookmark" aria-label="View “About Us”">View</a></span></div>
                        </td>
                        <td>2021/01/05</td>
                        <td> <strong>Nielsen Cobb</strong><br></td>
                        <td><span class="badge badge-success-light"> <span class="indicator"></span>Open</span></td>
                      </tr>
                      
                      <tr class="align-middle">
                        <td> <span class="form-check">
                            <input class="form-check-input" type="checkbox" id="check0"></span></td>
                        <td class="title column-title has-row-actions column-primary page-title" data-colname="Title">
                          <div class="locked-info"><span class="locked-avatar"></span> <span class="locked-text"></span></div>
                          <strong><a class="row-title" href="action=edit" aria-label="“About Us” (Edit)"> <strong>Career</strong></a></strong>
                          <div class="row-actions"><span class="edit text-muted"><a href="action=edit" aria-label="Edit “About Us”">Edit</a> | </span>
                          <span class="trash"><a href="" class="submitdelete" aria-label="Move “About Us” to the Trash">Trash</a> | </span>
                          <span class="trash"><a href="" class="editinline" aria-label="Quick edit “About Us” inline">Quick Edit</a> | </span>
                          <span class="view"><a href="/about-us/" rel="bookmark" aria-label="View “About Us”">View</a></span></div>
                        </td>
                        <td>2021/01/05</td>
                        <td> <strong>Nielsen Cobb</strong><br></td>
                        <td><span class="badge badge-success-light"> <span class="indicator"></span>Open</span></td>
                      </tr>
                      
                      <tr class="align-middle">
                        <td> <span class="form-check">
                            <input class="form-check-input" type="checkbox" id="check0"></span></td>
                        <td class="title column-title has-row-actions column-primary page-title" data-colname="Title">
                          <div class="locked-info"><span class="locked-avatar"></span> <span class="locked-text"></span></div>
                          <strong><a class="row-title" href="action=edit" aria-label="“About Us” (Edit)"> <strong>Contact Us</strong></a></strong>
                          <div class="row-actions"><span class="edit text-muted"><a href="action=edit" aria-label="Edit “About Us”">Edit</a> | </span>
                          <span class="trash"><a href="" class="submitdelete" aria-label="Move “About Us” to the Trash">Trash</a> | </span>
                          <span class="trash"><a href="" class="editinline" aria-label="Quick edit “About Us” inline">Quick Edit</a> | </span>
                          <span class="view"><a href="/about-us/" rel="bookmark" aria-label="View “About Us”">View</a></span></div>
                        </td>
                        <td>2021/01/05</td>
                        <td> <strong>Nielsen Cobb</strong><br></td>
                        <td><span class="badge badge-success-light"> <span class="indicator"></span>Open</span></td>
                      </tr>
                      
                      <tr class="align-middle">
                        <td> <span class="form-check">
                            <input class="form-check-input" type="checkbox" id="check0"></span></td>
                        <td class="title column-title has-row-actions column-primary page-title" data-colname="Title">
                          <div class="locked-info"><span class="locked-avatar"></span> <span class="locked-text"></span></div>
                          <strong><a class="row-title" href="action=edit" aria-label="“About Us” (Edit)"> <strong>Offering</strong></a></strong>
                          <div class="row-actions"><span class="edit text-muted"><a href="action=edit" aria-label="Edit “About Us”">Edit</a> | </span>
                          <span class="trash"><a href="" class="submitdelete" aria-label="Move “About Us” to the Trash">Trash</a> | </span>
                          <span class="trash"><a href="" class="editinline" aria-label="Quick edit “About Us” inline">Quick Edit</a> | </span>
                          <span class="view"><a href="/about-us/" rel="bookmark" aria-label="View “About Us”">View</a></span></div>
                        </td>
                        <td>2021/01/05</td>
                        <td> <strong>Nielsen Cobb</strong><br></td>
                        <td><span class="badge badge-success-light"> <span class="indicator"></span>Open</span></td>
                      </tr>
                      
                      <tr class="align-middle">
                        <td> <span class="form-check">
                            <input class="form-check-input" type="checkbox" id="check0"></span></td>
                        <td class="title column-title has-row-actions column-primary page-title" data-colname="Title">
                          <div class="locked-info"><span class="locked-avatar"></span> <span class="locked-text"></span></div>
                          <strong><a class="row-title" href="action=edit" aria-label="“About Us” (Edit)"> <strong>Solutions</strong></a></strong>
                          <div class="row-actions"><span class="edit text-muted"><a href="action=edit" aria-label="Edit “About Us”">Edit</a> | </span>
                          <span class="trash"><a href="" class="submitdelete" aria-label="Move “About Us” to the Trash">Trash</a> | </span>
                          <span class="trash"><a href="" class="editinline" aria-label="Quick edit “About Us” inline">Quick Edit</a> | </span>
                          <span class="view"><a href="/about-us/" rel="bookmark" aria-label="View “About Us”">View</a></span></div>
                        </td>
                        <td>2021/01/05</td>
                        <td> <strong>Nielsen Cobb</strong><br></td>
                        <td><span class="badge badge-success-light"> <span class="indicator"></span>Open</span></td>
                      </tr>
                      
                      <tr class="align-middle">
                        <td> <span class="form-check">
                            <input class="form-check-input" type="checkbox" id="check0"></span></td>
                        <td class="title column-title has-row-actions column-primary page-title" data-colname="Title">
                          <div class="locked-info"><span class="locked-avatar"></span> <span class="locked-text"></span></div>
                          <strong><a class="row-title" href="action=edit" aria-label="“About Us” (Edit)"> <strong>About Us</strong></a></strong>
                          <div class="row-actions"><span class="edit text-muted"><a href="action=home-page" aria-label="Edit “About Us”">Edit</a> | </span>
                          <span class="trash"><a href="" class="submitdelete" aria-label="Move “About Us” to the Trash">Trash</a> | </span>
                          <span class="trash"><a href="" class="editinline" aria-label="Quick edit “About Us” inline">Quick Edit</a> | </span>
                          <span class="view"><a href="/about-us/" rel="bookmark" aria-label="View “About Us”">View</a></span></div>
                        </td>
                        <td>2021/01/05</td>
                        <td> <strong>Nielsen Cobb</strong><br></td>
                        <td><span class="badge badge-success-light"> <span class="indicator"></span>Open</span></td>
                      </tr>
                      
                    </tbody>
                  </table>
                </div>
                <div class="dataTable-bottom">
                  <div class="dataTable-info">Showing 1 to 10 of 20 entries</div>
                  <nav class="dataTable-pagination">
                    <ul class="dataTable-pagination-list">
                      <li class="active"><a href="#" data-page="1">1</a></li>
                      <li class=""><a href="#" data-page="2">2</a></li>
                      <li class="pager"><a href="#" data-page="2">›</a></li>
                    </ul>
                  </nav>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  